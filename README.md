# ** Final Year Project ** #
Today does not walk, will have to run tomorrow.

## **Initialize** ##
1. Android Studio, select 'Check out project from Version Control', then choose 'Git'.
2. URL can be found in 'Overview' in Bitbucket, Directory Name = FYP
3. Test connection, if OKAY then Clone
4. If nothing show on the left, close the project.
5. Open the project using 'Open an existing Android Studio Project'.
6. Done.

## ** Normal workflow ** ##
1. **git pull** for latest commit.
2. Start coding.
3. Finish coding, ready to upload.
4. **git commit -a -m 'Message here'** to record changes to the repository.
5. **git push** upload to repository.
6. Done.

## ** Please, commit your changes or stash them before you can merge. ** ##
* git stash
* git pull
* git stash pop


## ** Useful Command ** ##
* git init
* git status
* git pull
* git commit -a -m 'Message here'
* git push
* git revert <commit id>
* git reset --hard <commit id>

## ** Tony ** ##
* Design Principle (Material Design)
* Draw UI and make it real
* Use Case

## ** Gavin ** ##
* Database design for server and client(SQLite)
* XAMPP > phpMyAdmin, php
* Android Studio > JSONParser, SQLite

## ** Silver ** ##
* Google Map
* Google Calendar
* Facebook

## ** Raymond ** ##
:)

## ** WEB Server ** ##
* Host: 1999fyp.comli.com 
* username: a3203939 
* password: 1999fyp

## ** PHP ** ##
* email: ron.lch@hotmail.com 
* password:123456

## ** Icon ** ##
http://www.flaticon.com/

## ** PHP remote control ** ##
202.125.255.3

## ** Useful UI ** ##
https://github.com/wasabeef/awesome-android-ui -- useful ui lib -- github

## ** Get SHA-1 for API key ** ##
http://www.truiton.com/2015/04/obtaining-sha1-fingerprint-android-keystore

## ** Un-track files from Git ** ##
git update-index --assume-unchanged path/to/file

## ** Re-track files from Git ** ##
git update-index --no-assume-unchanged path/to/file