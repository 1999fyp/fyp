package com.example.ron.fyp;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.ron.fyp.Tab.Tab2;
import com.example.ron.fyp.Tab.Tab3;
import com.example.ron.fyp.Tab.Tab4;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created
    Context _context;
    String username;

    public void setUsername(String username) {
        this.username = username;
    }
    protected static int[] ICONS = new int[]{
            R.drawable.home,
            R.drawable.gallery,
            R.drawable.user
    };

    // Build a Constructor and assign the passed Values to appropriate values in the class
    public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb, Context c) {
        super(fm);
        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;
        _context = c;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0: {
                Tab2 tab2 = new Tab2();
                return tab2;
            }
            case 1: {
                Tab4 tab4 = new Tab4();
                return tab4;
            }
/*            case 2: {
                Tab1 tab1 = new Tab1();
                return tab1;
            }*/
            case 2: {
                Tab3 tab3 = new Tab3();
                return tab3;
            }
        }
        return null;
    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {

        /*Drawable image = ContextCompat.getDrawable(_context, ICONS[position]);
        image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
        SpannableString sb = new SpannableString(" ");
        ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
        sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return sb;*/
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }

    public int getDrawableId(int position) {
        return ICONS[position];
    }


}