package com.example.ron.fyp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ron.fyp.Profile.ImageLoadTask;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by User on 24/4/2016.
 */
public class Event_Detail_adapter extends BaseAdapter {
    private ArrayList<String> user_id = new ArrayList<>();
    private ArrayList<String> full_name = new ArrayList<>();
    private ArrayList<String> icon = new ArrayList<>();
    private TextView friend_name1;

    private CircleImageView friend_icon1;
    Context c;

    Event_Detail_adapter(ArrayList<String> user_id, ArrayList<String> full_name, ArrayList<String> icon, Context c) {
        this.user_id = user_id;
        this.full_name = full_name;
        this.icon = icon;
        this.c = c;
        if(user_id.size() == 0){
            full_name.add("No participant");
            user_id.add("null");
            icon.add("null");
        }
    }

    @Override
    public int getCount() {
        return full_name.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = LayoutInflater.from(c).inflate(R.layout.event_detail_list,parent, false);
        friend_name1 = (TextView) convertView.findViewById(R.id.detail_name);
        friend_icon1 = (CircleImageView) convertView.findViewById(R.id.detail_icon);
        friend_name1.setText(full_name.get(position));
        new ImageLoadTask("http://raymondchan1179.dlinkddns.com/fyp/" + icon.get(position), friend_icon1).execute();
        return convertView;
    }
}