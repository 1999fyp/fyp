package com.example.ron.fyp.Weather;

import org.json.JSONObject;

/**
 * Created by User on 20/3/2016.
 */
public interface JSONPopulator {
    void populate(JSONObject data);
}
