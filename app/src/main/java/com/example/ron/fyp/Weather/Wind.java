package com.example.ron.fyp.Weather;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by User on 5/4/2016.
 */
public class Wind implements JSONPopulator {
    private String speed;

    public String getSpeed() {
        return speed;
    }

    @Override
    public void populate(JSONObject data) {
        speed = data.optString("speed");
    }
}