package com.example.ron.fyp;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidviewhover.BlurLayout;
import com.example.ron.fyp.Database.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.impl.entity.StrictContentLengthStrategy;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import jp.wasabeef.blurry.Blurry;

public class Tab2_recyclerview extends RecyclerView.Adapter<Tab2_recyclerview.ViewHolder> {
    private static final String TAG_JOINED = "is_joined";
    OnItemClickListener mItemClickListener;
    private final FragmentActivity mActivity;
    ArrayList<String> myListEventname = new ArrayList<String>();
    ArrayList<String> myListHost = new ArrayList<String>();
    ArrayList<String> myListSTARTDATE = new ArrayList<String>();
    ArrayList<String> myListSTARTTIME = new ArrayList<String>();
    ArrayList<String> myListSTARTLOCATION = new ArrayList<String>();
    ArrayList<String> myListJoined = new ArrayList<String>();
    ArrayList<String> myListCAPACITY = new ArrayList<String>();
    ArrayList<String> myListEventID = new ArrayList<String>();
    ArrayList<Integer> myListTYPE;
    ImageView joinBtn;
    JSONObject json;
    private int user_ID;
    private SharedPreferences prefs = null;

    public Tab2_recyclerview(FragmentActivity mActivity, ArrayList<String> myListEventname,
                             ArrayList<String> myListHost, ArrayList<String> myListSTARTDATE,
                             ArrayList<String> myListSTARTTIME, ArrayList<String> myListSTARTLOCATION,
                             ArrayList<String> myListJoined, ArrayList<String> myListCAPACITY,
                             ArrayList<Integer> myListTYPE, ArrayList<String> myListEventID, ImageView joinBtn) {
        this.mActivity = mActivity;
        this.myListEventname = myListEventname;
        this.myListHost = myListHost;
        this.myListSTARTDATE = myListSTARTDATE;
        this.myListSTARTTIME = myListSTARTTIME;
        this.myListSTARTLOCATION = myListSTARTLOCATION;
        this.myListJoined = myListJoined;
        this.myListCAPACITY = myListCAPACITY;
        this.myListTYPE = myListTYPE;
        this.myListEventID = myListEventID;
        this.joinBtn = joinBtn;
    }

    public String geteventid(int position) {
        return myListEventID.get(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        final View sView = mInflater.inflate(R.layout.list_item, parent, false);
        return new ViewHolder(sView);
    }

    private boolean setfavourite(ViewHolder viewHolder, int position) {
        prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
        user_ID = prefs.getInt("userID", user_ID);
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        JSONParser jsonParser = new JSONParser();
        prefs.getInt("userID", user_ID);
        params.add(new BasicNameValuePair("user_id", String.valueOf(user_ID)));
        params.add(new BasicNameValuePair("event_id", String.valueOf(myListEventID.get((position)))));
        params.add(new BasicNameValuePair("method", "1"));
        if(viewHolder.favourite_icon.getVisibility() == View.VISIBLE){
            json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/unfavourite.php", "GET", params);
        }
        else{
            json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/favourite.php", "GET", params);
        }


        try {
            if (json.getString("success").equals("1")) {
                viewHolder.favourite_icon.setVisibility(View.VISIBLE);
                return true;
            } else {
                viewHolder.favourite_icon.setVisibility(View.INVISIBLE);
                return false;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
        //  if (del favourite = false ,else add favourite = true)
    }

    private void Checkfavourite_icon(ViewHolder viewHolder, int position) {
        prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
        user_ID = prefs.getInt("userID", user_ID);
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        JSONParser jsonParser = new JSONParser();
        prefs.getInt("userID", user_ID);
        params.add(new BasicNameValuePair("user_id", String.valueOf(user_ID)));
        params.add(new BasicNameValuePair("event_id", String.valueOf(myListEventID.get(position))));
        params.add(new BasicNameValuePair("method", "0"));
        json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/favourite.php", "GET", params);
        try {
            if (json.getString("success").equals("4")) {
                viewHolder.favourite_icon.setVisibility(View.VISIBLE);
            } else {
                viewHolder.favourite_icon.setVisibility(View.INVISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.event_name.setText(myListEventname.get(position));
        holder.event_host.setText(myListHost.get(position));
        holder.event_date.setText(myListSTARTDATE.get(position));
        holder.event_time.setText(myListSTARTTIME.get(position));
        holder.event_location.setText(myListSTARTLOCATION.get(position));
        holder.event_capacity.setText(myListJoined.get(position) + " / " + myListCAPACITY.get(position));

        //final ImageView joinBtn = (ImageView) mActivity.findViewById(R.id.joinBtn);

        JSONObject json;
        prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
        user_ID = prefs.getInt("user_id", user_ID);
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        JSONParser jsonParser = new JSONParser();
        Log.v ("userid", String.valueOf(user_ID));
        Log.v ("eventID",String.valueOf(myListEventID));
        params.add(new BasicNameValuePair("user_id", String.valueOf(user_ID)));
        params.add(new BasicNameValuePair("event_id", String.valueOf(myListEventID.get(position))));
        json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/joined_activity.php", "GET", params);
        try {
            int isJoined = json.getInt(TAG_JOINED);
            Log.v ("isjoined", String.valueOf(isJoined));
            if (isJoined == 0) {
                if (json.getString("success").equals("1")) {
                    holder.joinBtn.setImageResource(R.drawable.join);
                }
            }
            else if (isJoined == 1){
                JSONObject json2;
                List<NameValuePair> params2 = new ArrayList<NameValuePair>();
                JSONParser jsonParser2 = new JSONParser();
                Log.v ("userid", String.valueOf(user_ID));
                Log.v ("eventID",String.valueOf(myListEventID));
                params.add(new BasicNameValuePair("user_id", String.valueOf(user_ID)));
                params.add(new BasicNameValuePair("event_id", String.valueOf(myListEventID.get(position))));
                json2 = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/quit_activity.php", "GET", params);
                holder.joinBtn.setImageResource(R.drawable.unjoin);
            }
        }
        catch (JSONException e){
            e.printStackTrace();
        }

        switch (myListTYPE.get(position)) {
            case 1:
                holder.sport_icon.setImageResource(R.drawable.type_run);
                break;
            case 2:
                holder.sport_icon.setImageResource(R.drawable.type_hike);
                break;
            case 3:
                holder.sport_icon.setImageResource(R.drawable.type_cycle);
                break;
        }
        Checkfavourite_icon(holder, position);

    }

    @Override
    public int getItemCount() {
        return myListEventname.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        View hover;
        BlurLayout sampleLayout;
        Handler mHandler;
        TextView event_name, event_host, event_date, event_time, event_location, event_capacity, event_joined;
        ImageView sport_icon, joinBtn, favourite_icon;

        View v;


        public ViewHolder(View view) {
            super(view);
            this.v = view;
            mHandler = new Handler();
            joinBtn = (ImageView) view.findViewById(R.id.joinBtn);
            event_name = (TextView) view.findViewById(R.id.event_name);
            event_host = (TextView) view.findViewById(R.id.event_host);
            event_date = (TextView) view.findViewById(R.id.event_date);
            event_time = (TextView) view.findViewById(R.id.event_time);
            event_location = (TextView) view.findViewById(R.id.event_location);
            event_capacity = (TextView) view.findViewById(R.id.event_capacity);
            sport_icon = (ImageView) view.findViewById(R.id.sport_icon);
            favourite_icon = (ImageView) view.findViewById(R.id.favourite_icon);

            //checkIsJoined();

            joinBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    JSONObject json;
                    prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
                    user_ID = prefs.getInt("userID", user_ID);
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    JSONParser jsonParser = new JSONParser();
                    Log.v ("userid", String.valueOf(user_ID));
                    Log.v ("eventID",String.valueOf(myListEventID));
                    params.add(new BasicNameValuePair("user_id", String.valueOf(user_ID)));
                    params.add(new BasicNameValuePair("event_id", myListEventID.get(getPosition())));
                    json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/joined_activity.php", "GET", params);
                    try {
                        int isJoined = json.getInt(TAG_JOINED);
                        int counter;
                        Log.v ("isjoined", String.valueOf(isJoined));
                        if (isJoined == 0) {
                            if (json.getString("success").equals("1")) {
                                joinBtn.setImageResource(R.drawable.join);
                                //Log.v ("AAAA", myListJoined.get(getPosition()));
                                //event_capacity.setText(Integer.valueOf(myListJoined.get(getPosition())) + 1 + " / " + myListCAPACITY.get(getPosition()));
                                Toast.makeText(mActivity, "Joined event successfully", Toast.LENGTH_LONG).show();
                            }
                        }
                        else if (isJoined == 1){
                            JSONObject json2;
                            prefs = PreferenceManager.getDefaultSharedPreferences(mActivity);
                            user_ID = prefs.getInt("userID", user_ID);
                            List<NameValuePair> params2 = new ArrayList<NameValuePair>();
                            JSONParser jsonParser2 = new JSONParser();
                            Log.v ("userid", String.valueOf(user_ID));
                            Log.v ("eventID",String.valueOf(myListEventID));
                            params.add(new BasicNameValuePair("user_id", String.valueOf(user_ID)));
                            params.add(new BasicNameValuePair("event_id", myListEventID.get(getPosition())));
                            json2 = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/quit_activity.php", "GET", params);
                            joinBtn.setImageResource(R.drawable.unjoin);
                            //event_capacity.setText(Integer.valueOf(myListJoined.get(getPosition())) - 1 + " / " + myListCAPACITY.get(getPosition()));
                            Toast.makeText(mActivity, "Quited event successfully", Toast.LENGTH_LONG).show();
                        }
                    }
                    catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            });
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        @Override
        public boolean onLongClick(View v) {

            sampleLayout = (BlurLayout) v.findViewById(R.id.blur);
            sampleLayout.setVisibility(View.VISIBLE);
            View hover = LayoutInflater.from(mActivity).inflate(R.layout.hover_layout, null);
            sampleLayout.setHovered(true);
           if(setfavourite(this, getPosition())) {
               sampleLayout.addChildAppearAnimator(hover, R.id.favourite_icon_end, Techniques.FadeIn, 500, 0, true);
               //View (R.id.heart) appear animation.
               sampleLayout.addChildDisappearAnimator(hover, R.id.favourite_icon_end, Techniques.FadeOut, 500, 300);
           }else{
               sampleLayout.addChildAppearAnimator(hover, R.id.favourite_icon_start, Techniques.FadeIn, 500, 0, true);
               //View (R.id.heart) appear animation.
               sampleLayout.addChildDisappearAnimator(hover, R.id.favourite_icon_start, Techniques.FadeOut, 500, 300);
           }
            //View (R.id.heart) disappear animation.

            sampleLayout.setHoverView(hover);
            sampleLayout.showHover();
            //   sampleLayout.toggleHover();

            Blurry.with(mActivity).capture(v).into((ImageView) v.findViewById(R.id.Burry));
            Blurry.with(mActivity)
                    .radius(10)
                    .sampling(8)
                    .color(Color.WHITE)
                    .async()
                    .animate(0)
                    .onto((ViewGroup) v.findViewById(R.id.hover));
            hover.postDelayed(runnable, 1000);

            hover.postDelayed(runnable2, 1000);
            return true;
        }


        final Runnable runnable = new Runnable() {
            public void run() {
                sampleLayout.dismissHover();
                Log.d("show", sampleLayout.getHoverStatus() + "");

            }
        };
        final Runnable runnable2 = new Runnable() {
            public void run() {
                sampleLayout.setVisibility(View.GONE);
                Blurry.delete((ViewGroup) v.findViewById(R.id.hover));
            }
        };

        @Override
        public void onClick(View v) {

            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }
    }


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

}
