package com.example.ron.fyp.Activity_Log;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.ron.fyp.Database.JSONParser;
import com.example.ron.fyp.R;
import com.example.ron.fyp.ViewLog;
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableItemViewHolder;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Activity_Log_List extends AppCompatActivity   {
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private ProgressBar log_progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity__log__list);
        recyclerView = (RecyclerView) findViewById(R.id.log_recyclerview);
        toolbar = (Toolbar) findViewById(R.id.log_list_toolbar);
        log_progressBar = (ProgressBar) findViewById(R.id.log_progressBar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        new Activity_Log_Json_Task().execute();
    }

    public class Activity_Log_Json_Task extends AsyncTask<Void, Void, Void> {
        private SharedPreferences prefs = null;
        private int user_ID;

        private JSONObject json = null;
        private ArrayList<String> log_id = new ArrayList<String>();
        private ArrayList<String> event_id = new ArrayList<String>();
        private ArrayList<String> user_id = new ArrayList<String>();
        private ArrayList<String> total_distance = new ArrayList<String>();
        private ArrayList<String> avg_pace = new ArrayList<String>();
        private ArrayList<String> total_calories = new ArrayList<String>();
        private ArrayList<String> total_time = new ArrayList<String>();
        private ArrayList<String> type_of_sports = new ArrayList<String>();
        private ArrayList<String> start_date = new ArrayList<String>();
        private ArrayList<String> end_date = new ArrayList<String>();
        private ArrayList<String> start_time = new ArrayList<String>();
        private ArrayList<String> end_time = new ArrayList<String>();
        private ArrayList<String> coordinates = new ArrayList<String>();
        private ArrayList<String> event_name = new ArrayList<String>();

        public Activity_Log_Json_Task() {
        }

        @Override
        protected Void doInBackground(Void... mparams) {
            prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            user_ID = prefs.getInt("userID", user_ID);
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            JSONParser jsonParser = new JSONParser();
            params.add(new BasicNameValuePair("user_id", String.valueOf(user_ID)));
            json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/activity_log_list_date.php", "GET", params);
            try {
                JSONArray log = null;
                log = json.getJSONArray("log");
                for (int i = 0; i < log.length(); i++) {
                    JSONObject c = log.getJSONObject(i);
                    log_id.add(c.getString("log_id"));
                    event_id.add(c.getString("event_id"));
                    total_distance.add(c.getString("total_distance"));
                    avg_pace.add(c.getString("avg_pace"));
                    total_calories.add(c.getString("total_calories"));
                    total_time.add(c.getString("total_time"));
                    type_of_sports.add(c.getString("type_of_sports"));
                    start_date.add(c.getString("start_date"));
                    end_date.add(c.getString("end_date"));
                    start_time.add(c.getString("start_time"));
                    end_time.add(c.getString("end_time"));
                    coordinates.add(c.getString("coordinates"));
                    user_id.add(c.getString("user_id"));
                    event_name.add(c.getString("event_name"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            // Setup expandable feature and RecyclerView
            RecyclerViewExpandableItemManager expMgr = new RecyclerViewExpandableItemManager(null);

            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
           RecyclerView.Adapter ad = expMgr.createWrappedAdapter(new MyAdapter(log_id,
                    event_id,
                    user_id,
                    total_distance,
                    avg_pace,
                    total_calories,
                    total_time,
                    type_of_sports,
                    start_date,
                    end_date,
                    start_time,
                    end_time,
                    coordinates,
                    event_name,
                    String.valueOf(user_ID)));

            recyclerView.setAdapter(ad);

            expMgr.attachRecyclerView(recyclerView);
            expMgr.expandAll();
            log_progressBar.setVisibility(View.INVISIBLE);

            recyclerView.setVisibility(View.VISIBLE);

        }
    }

    static abstract class MyBaseItem {
        public final long id;
        public final String text;

        public MyBaseItem(long id, String text) {
            this.id = id;
            this.text = text;
        }
    }

    static class MyGroupItem extends MyBaseItem {
        public final List<MyChildItem> children;

        public MyGroupItem(long id, String text) {
            super(id, text);
            children = new ArrayList<>();
        }
    }

    static class MyChildItem extends MyBaseItem {
        public MyChildItem(long id, String text) {
            super(id, text);
        }
    }



    static abstract class MyBaseViewHolder extends AbstractExpandableItemViewHolder {
        public TextView log_date;
        TextView event_id_txt, event_name_txt, start_date_txt, end_date_txt, avg_pace_txt,
                distance_txt, calories_txt, time_txt, start_time_txt, end_time_txt;
        ImageView sport_icon_img;

        public MyBaseViewHolder(View itemView) {
            super(itemView);
            log_date = (TextView) itemView.findViewById(R.id.log_date);
        }
    }

    static class MyGroupViewHolder extends MyBaseViewHolder {
        public MyGroupViewHolder(View itemView) {
            super(itemView);
        }
    }

    static class MyChildViewHolder extends MyBaseViewHolder {
        public MyChildViewHolder(View itemView) {
            super(itemView);
            event_id_txt = (TextView) itemView.findViewById(R.id.event_id_txt);
            event_name_txt = (TextView) itemView.findViewById(R.id.event_name_txt);
            start_date_txt = (TextView) itemView.findViewById(R.id.start_date_txt);
            end_date_txt = (TextView) itemView.findViewById(R.id.end_date_txt);

          /**  avg_pace_txt = (TextView) itemView.findViewById(R.id.avg_pace_txt);
            distance_txt = (TextView) itemView.findViewById(R.id.distance_txt);
            calories_txt = (TextView) itemView.findViewById(R.id.calories_txt);
            time_txt = (TextView) itemView.findViewById(R.id.time_txt);
            start_time_txt = (TextView) itemView.findViewById(R.id.start_time_txt);
            end_time_txt = (TextView) itemView.findViewById(R.id.end_time_txt);**/
            sport_icon_img = (ImageView) itemView.findViewById(R.id.sport_icon_img);


        }
    }

    class MyAdapter extends AbstractExpandableItemAdapter<MyGroupViewHolder, MyChildViewHolder> implements View.OnClickListener {
        List<MyGroupItem> mItems;
        ArrayList<String> Log_date = new ArrayList<String>();
        ArrayList<String> start_date_norepeated = new ArrayList<String>();
        ArrayList<Integer> choose = new ArrayList<Integer>();
        String id;
        ArrayList<String> log_id;
        ArrayList<String> event_id;
        ArrayList<String> user_id;
        ArrayList<String> total_distance;
        ArrayList<String> avg_pace;
        ArrayList<String> total_calories;
        ArrayList<String> total_time;
        ArrayList<String> type_of_sports;
        ArrayList<String> end_date;
        ArrayList<String> start_time;
        ArrayList<String> end_time;
        ArrayList<String> coordinates;
        ArrayList<String> event_name;
        public MyAdapter(ArrayList<String> log_id,
                         ArrayList<String> event_id,
                         ArrayList<String> user_id,
                         ArrayList<String> total_distance,
                         ArrayList<String> avg_pace,
                         ArrayList<String> total_calories,
                         ArrayList<String> total_time,
                         ArrayList<String> type_of_sports,
                         ArrayList<String> start_date,
                         ArrayList<String> end_date,
                         ArrayList<String> start_time,
                         ArrayList<String> end_time,
                         ArrayList<String> coordinates,
                         ArrayList<String> event_name,
                         String id
        )

        {
            setHasStableIds(true); // this is required for expandable feature.


            this.id = id;
            this.log_id = log_id;
            this.event_id = event_id;
            this.user_id = user_id;
            this.avg_pace = avg_pace;
            this.total_distance = total_distance;
            this.Log_date = start_date;
            this.total_calories = total_calories;
            this.total_time = total_time;
            this.type_of_sports = type_of_sports;
            this.end_date = end_date;
            this.start_time = start_time;
            this.end_time = end_time;
            this.coordinates = coordinates;
            this.event_name = event_name;
            mItems = new ArrayList<>();
            start_date_norepeated = norepeated(Log_date);
            for (int i = 0; i < 20; i++) {
                MyGroupItem group = new MyGroupItem(i, "GROUP " + i);
                for (int j = 0; j < 5; j++) {
                    group.children.add(new MyChildItem(i, "child " + i));
                }
                mItems.add(group);
            }
        }

        public ArrayList<String> norepeated(ArrayList<String> event_date) {
            ArrayList<String> al = new ArrayList<>();
            Set<String> hs = new HashSet<>();
            hs.addAll(event_date);
            al.addAll(hs);
            for (int a = 0; a < al.size(); a++) {
                Log.d("logtest", al.get(a));
            }
            return al;
        }

        @Override
        public int getGroupCount() {

            return start_date_norepeated.size();
        }


        @Override
        public int getChildCount(int groupPosition) {
            //choose.clear();

            for (int b = 0; b < Log_date.size(); b++) {
                // Log.d("pptestGroup",start_date_norepeated.get(groupPosition)+b+"---"+Log_date.get(b));
                if (start_date_norepeated.get(groupPosition).equals(Log_date.get(b))) {
                    // Log.d("myhzhz",start_date_norepeated.get(groupPosition)+"---"+Log_date.get(b));
                    choose.add(b);
                }
            }
            Log.d("size", choose.size() + "");
            return choose.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            choose.clear();

            // This method need to return unique value within all group items.
            return mItems.get(groupPosition).id;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {

            // This method need to return unique value within the group.
            return mItems.get(groupPosition).children.get(childPosition).id;
        }

        @Override
        public MyGroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.log_group_item, parent, false);
            return new MyGroupViewHolder(v);

        }

        @Override
        public MyChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.log_child_item, parent, false);
            v.setOnClickListener(this);
            return new MyChildViewHolder(v);
        }

        @Override
        public void onBindGroupViewHolder(MyGroupViewHolder holder, int groupPosition, int viewType) {

            MyGroupItem group = mItems.get(groupPosition);
            holder.log_date.setText(start_date_norepeated.get(groupPosition));
            //      holder.textView.setText(group.text);
        }

        @Override
        public void onBindChildViewHolder(MyChildViewHolder holder, int groupPosition, int childPosition, int viewType) {
            MyChildItem group = mItems.get(groupPosition).children.get(childPosition);

            holder.event_id_txt.setText("#" + event_id.get((holder.getPosition()-groupPosition-1)));
            holder.event_name_txt.setText(event_name.get((holder.getPosition()-groupPosition-1)));
            holder.start_date_txt.setText("Start date: " + Log_date.get((holder.getPosition()-groupPosition-1)));
            holder.end_date_txt.setText("End date: " + end_date.get((holder.getPosition()-groupPosition-1)));
        //    Log.d("uuitem","item: "+  (holder.getPosition()-groupPosition-1));
     /**       holder.avg_pace_txt.setText(avg_pace.get((holder.getPosition()-groupPosition-1)));
            holder.distance_txt.setText(total_distance.get((holder.getPosition()-groupPosition-1)));
            holder.calories_txt.setText(total_calories.get((holder.getPosition()-groupPosition-1)));
            holder.time_txt.setText(total_time.get((holder.getPosition()-groupPosition-1)));
            holder.start_time_txt.setText(start_time.get((holder.getPosition()-groupPosition-1)));
            holder.end_time_txt.setText(end_time.get((holder.getPosition()-groupPosition-1)));**/
            switch (type_of_sports.get((holder.getPosition()-groupPosition-1))) {
                case "1":
                    holder.sport_icon_img.setImageResource(R.drawable.running);
                    break;
                case "2":
                    holder.sport_icon_img.setImageResource(R.drawable.hiking);
                    break;
                case "3":
                    holder.sport_icon_img.setImageResource(R.drawable.cycling);
                    break;
            }
            holder.itemView.setTag((holder.getPosition()-groupPosition-1));

        }

        @Override
        public boolean onCheckCanExpandOrCollapseGroup(MyGroupViewHolder holder, int groupPosition, int x, int y, boolean expand) {
            return true;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(Activity_Log_List.this, ViewLog.class);
            Bundle bundle = new Bundle();
            bundle.putString("log_id",log_id.get((int) v.getTag()));
            bundle.putString("event_id",event_id.get((int) v.getTag()));
            bundle.putString("user_id",user_id.get((int) v.getTag()));
            bundle.putString("total_distance",total_distance.get((int) v.getTag()));
            bundle.putString("avg_pace",avg_pace.get((int) v.getTag()));
            bundle.putString("total_calories",total_calories.get((int) v.getTag()));
            bundle.putString("total_time", total_time.get((int) v.getTag()));
            bundle.putString("type_of_sports",type_of_sports.get((int) v.getTag()));
            bundle.putString("start_date",Log_date.get((int) v.getTag()));
            bundle.putString("end_date",end_time.get((int) v.getTag()));
            bundle.putString("start_time",start_time.get((int) v.getTag()));
            bundle.putString("end_time",end_time.get((int) v.getTag()));
            bundle.putString("coordinates",coordinates.get((int) v.getTag()));
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }


}
