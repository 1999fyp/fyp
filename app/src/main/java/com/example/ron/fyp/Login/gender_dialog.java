package com.example.ron.fyp.Login;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;
import com.example.ron.fyp.R;

public class gender_dialog extends DialogFragment implements View.OnClickListener {
    NoticeDialogListener mListener;
    private ImageView man, woman;
    private ActionProcessButton submit_BTN;
    private String gender;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View view = inflater.inflate(R.layout.activity_gender_dialog, null);
        builder.setView(view);
        man = (ImageView) view.findViewById(R.id.man_image);
        woman = (ImageView) view.findViewById(R.id.woman_image);
        submit_BTN = (ActionProcessButton) view.findViewById(R.id.submit_BTN);
        man.setOnClickListener(this);
        woman.setOnClickListener(this);
        submit_BTN.setOnClickListener(this);


        setCancelable(true);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.man_image:
                man.setImageResource(R.drawable.male_blue);
                woman.setImageResource(R.drawable.woman_black);
                gender = "Male";
                break;
            case R.id.woman_image:
                man.setImageResource(R.drawable.male_black);
                woman.setImageResource(R.drawable.woman_red);
                gender = "Female";
                break;
            case R.id.submit_BTN:
                if (gender == null) {
                    Toast.makeText(getActivity(), "Please choose your gender", Toast.LENGTH_SHORT).show();
                } else {
                    mListener.Messagepass(gender);
                    dismiss();
                }
                break;
        }
    }


    public interface NoticeDialogListener {
        public void Messagepass(String gender);
    }

}
