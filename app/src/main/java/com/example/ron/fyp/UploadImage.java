package com.example.ron.fyp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.ron.fyp.Database.JSONParser;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UploadImage extends AppCompatActivity {

    int USER_ID;
    String EVENT_ID = "1";
    Button Btn_upload;
    String base64;
    ImageView img;

    private String url = "http://raymondchan1179.dlinkddns.com/fyp/upload_img.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_image);

        Toolbar toolbar = (Toolbar) findViewById(R.id.image_upload_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Upload Image");

        img = (ImageView)findViewById(R.id.upload_img);
        byte[] byteArray = getIntent().getByteArrayExtra("ByteArr");

        //Convert Byte Array to Bitmap
        Bitmap bm = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        img.setImageBitmap(bm);

        base64 = Base64.encodeToString(byteArray, Base64.DEFAULT);
        Log.v("base64", base64);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;
        int width = size.x;
        img.requestLayout();
        img.getLayoutParams().height = height;
        img.getLayoutParams().width = width;

        Btn_upload = (Button)findViewById(R.id.Btn_upload_img);
        Btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new uploadToServer().execute();
            }
        });
    }

    public void Save() {
/*        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    public class uploadToServer extends AsyncTask<Void, Void, String> {
        com.andreabaccega.widget.FormEditText imageDescription = (com.andreabaccega.widget.FormEditText) findViewById(R.id.image_description);
        String descriptionStr = imageDescription.getText().toString();
        private ProgressDialog pd = new ProgressDialog(UploadImage.this);
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Image uploading!");
            pd.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            int defValue = 0000;
            USER_ID = prefs.getInt("userID", defValue);
            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("base64", base64));
            nameValuePairs.add(new BasicNameValuePair("ImageName", System.currentTimeMillis() + ".jpg"));
            nameValuePairs.add(new BasicNameValuePair("user_id", String.valueOf(USER_ID)));
            nameValuePairs.add(new BasicNameValuePair("event_id", EVENT_ID));
            nameValuePairs.add(new BasicNameValuePair("description", descriptionStr));
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                String st = EntityUtils.toString(response.getEntity());
                Log.v("log_tag", "In the try Loop" + st);

            } catch (Exception e) {
                Log.v("log_tag", "Error in http connection " + e.toString());
            }
            return "Success";

        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            pd.hide();
            pd.dismiss();
            finish();
        }
    }

}
