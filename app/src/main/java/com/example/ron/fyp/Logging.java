package com.example.ron.fyp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Logging extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    protected static final String TAG = "location-updates-sample";

    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 100;

    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    protected final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";
    protected final static String LOCATION_KEY = "location-key";

    protected GoogleApiClient mGoogleApiClient;

    protected LocationRequest mLocationRequest;

    protected Location mCurrentLocation;

    protected info.hoang8f.widget.FButton mStartUpdatesButton, mStopUpdatesButton;
    protected TextView distance_tv, pace_tv, calories_tv;
    protected Chronometer totalTime;
    protected long calories;
    protected double pace;
    protected double totalDistance = 0;
    protected long lastPause;

    // For php upload
    protected int USER_ID;
    protected String START_DATE, END_DATE;
    protected String START_TIME, END_TIME;

    // For calculating calories
    protected boolean is_male;
    protected double age, height, weight, calories_base;
    protected String type_of_sports, EVENTID;
    double[] sport_calories_factor = {7, 5.3, 7.5}; // 1 for Running, 2 for Hiking, 3 for Cycling

    protected Boolean mRequestingLocationUpdates;
    protected Boolean GPS_enable;

    // Scheduled task for pace and calories
    ScheduledExecutorService scheduler;
    Runnable r;
    ScheduledFuture<?> scheduledFuture;

    // OMG this is so 9
    String temp1, temp2, temp3;
    long temp4_timeElapsed, temp5_totalSec;

    MapView mapView;
    GoogleMap map;

    int result;
    private ArrayList<LatLng> routeList = new ArrayList<LatLng>();
    private LatLng startLatLng;
    private Polyline line;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.logginggggg);
        Log.v("onCreate", "start");

        // Keep screen on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        // Initialize for php upload
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int defValue = 0000;
        USER_ID = prefs.getInt("userID", defValue);
        Bundle typeIdBundle = new Bundle();
        Bundle eventIdBundle = new Bundle();
        eventIdBundle = getIntent().getExtras();
        typeIdBundle = getIntent().getExtras();
        type_of_sports = typeIdBundle.getString("typeID");
        EVENTID = eventIdBundle.getString("eventID");
        Log.v ("typeID", type_of_sports);
        Log.v ("eventID", EVENTID);

        // Get start Time
        Calendar c = Calendar.getInstance();
        String year = String.valueOf(c.get(Calendar.YEAR));
        String month = AddZero(c.get(Calendar.MONTH)+1);
        String day = AddZero(c.get(Calendar.DATE));
        String hour = AddZero(c.get(Calendar.HOUR_OF_DAY));
        String minutes = AddZero(c.get(Calendar.MINUTE));
        String seconds = AddZero(c.get(Calendar.SECOND));
        START_TIME = hour + ":" + minutes + ":" + seconds;
        START_DATE = year + "-" + month + "-" + day;

        // Locate the UI widgets.
        mStartUpdatesButton = (info.hoang8f.widget.FButton) findViewById(R.id.logStartButton);
        mStopUpdatesButton = (info.hoang8f.widget.FButton) findViewById(R.id.logStopButton);
        distance_tv = (TextView) findViewById(R.id.kmNumber);
        pace_tv = (TextView) findViewById(R.id.speedNumber);
        calories_tv = (TextView) findViewById(R.id.fireNumber);

        // Setup elapsed time
        totalTime = (Chronometer) findViewById(R.id.timerNumber);
        totalTime.setBase(SystemClock.elapsedRealtime());
        totalTime.start();
        mRequestingLocationUpdates = true;

        //Check if GPS is enabled on phone
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            GPS_enable = false;
            buildAlertMessageNoGps();
        } else {
            GPS_enable = true;
        }

        MapSetup(savedInstanceState);
        updateValuesFromBundle(savedInstanceState);
        if (GPS_enable)
            buildGoogleApiClient();
        else {
            // build dialog again?
            // say: geolocation recording will be disabled
        }

        // Initialize user's information for calculating calories
        is_male = (prefs.getString("gender",null).equals("M")) ? true : false;
        age = getAge(prefs.getString("DOB",null));

        weight = Double.parseDouble(prefs.getString("weight",null));  // KG as unit
        height = Double.parseDouble(prefs.getString("height",null)); // CM as unit

        // Calculate base factor of calories
        if (is_male)
            calories_base = 66.5 + (13.75 * weight) + (5.003 * height) - (6.775 * age); // Men
        else
            calories_base = 655.1 + (9.563 * weight) + (1.850 * height) - (4.676 * age); // Women
        calories_base *= sport_calories_factor[Integer.parseInt(type_of_sports) - 1];
        ScheduleTask();

        Log.v("onCreate", "finish");
    }

    private String AddZero(int temp) {
        String temp_addZero;
        temp_addZero = (temp > 10) ? String.valueOf(temp) : "0" + String.valueOf(temp);
        return temp_addZero;
    }

    // Initialize Map
    private void MapSetup(Bundle savedInstanceState) {
        Log.v("MapSetup", "start");

        //Map setup
        mapView = (MapView) findViewById(R.id.logMapView);
        mapView.onCreate(savedInstanceState);

        // Gets to GoogleMap from the MapView and does initialization stuff
        map = mapView.getMap();
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.setIndoorEnabled(false);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        } else {
            // Show rationale and request permission.
        }

        MapsInitializer.initialize(this);

        // Updates the location and zoom of the MapView
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(22.3714181, 114.137587), 11);
        map.animateCamera(cameraUpdate);
        Log.v("MapSetup", "finish");
    }

    private void updateValuesFromBundle(Bundle savedInstanceState) {
        Log.v("updateValuesFromBundle", "start");
        if (savedInstanceState != null) {
            Log.v("savedInstanceState", "not null");

            if (savedInstanceState.keySet().contains(REQUESTING_LOCATION_UPDATES_KEY)) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean(
                        REQUESTING_LOCATION_UPDATES_KEY);
                //setButtonsEnabledState();
            }

            if (savedInstanceState.keySet().contains(LOCATION_KEY)) {
                mCurrentLocation = savedInstanceState.getParcelable(LOCATION_KEY);
                mapSetLocation();
            }
        } else
            Log.v("savedInstanceState", "is null");
        Log.v("updateValuesFromBundle", "finish");

    }

    protected synchronized void buildGoogleApiClient() {
        Log.v("buildGoogleApiClient", "start");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
        Log.v("buildGoogleApiClient", "finish");
    }

    protected void createLocationRequest() {
        Log.v("createLocationRequest", "start");
        mLocationRequest = new LocationRequest();

        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        Log.v("createLocationRequest", "finish");
    }

    public void startUpdatesButtonHandler(View view) {
        // Pause Logging
        if (mStartUpdatesButton.getText().equals("PAUSE")) {
            lastPause = SystemClock.elapsedRealtime();
            totalTime.stop();
            scheduledFuture.cancel(true);
            mRequestingLocationUpdates = false;
            stopLocationUpdates();
            Toast.makeText(this, "Pause Logging", Toast.LENGTH_SHORT).show();
            mStartUpdatesButton.setText("RESUME");
            setEndTime();
        }
        // Resume Logging
        else if (mStartUpdatesButton.getText().equals("RESUME")) {
            totalTime.setBase(totalTime.getBase() + SystemClock.elapsedRealtime() - lastPause);
            totalTime.start();
            scheduledFuture = scheduler.scheduleAtFixedRate(r, 1L, 1L, TimeUnit.SECONDS);
            mRequestingLocationUpdates = true;
            startLocationUpdates();
            Toast.makeText(this, "Resume Logging", Toast.LENGTH_SHORT).show();
            mStartUpdatesButton.setText("PAUSE");
        }
    }

    public void stopUpdatesButtonHandler(View view) {
        if (!mStartUpdatesButton.getText().equals("RESUME")) {
            lastPause = SystemClock.elapsedRealtime();
            totalTime.stop();
            scheduledFuture.cancel(true);
            mRequestingLocationUpdates = false;
            stopLocationUpdates();
            Toast.makeText(this, "Stop Logging", Toast.LENGTH_SHORT).show();
            mStartUpdatesButton.setText("RESUME");
            setEndTime();
        }
            // Dialog popup, Confirm Stop, then switch to activity log
        new AlertDialog.Builder(this)
                .setTitle("Activity completed?")
                //.setMessage("Finish Activity?")
                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // upload
                        new uploadToServer().execute();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                //.setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    protected void startLocationUpdates() {

        Log.v("startLocationUpdates", "start");

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.v("No Permission", "android.Manifest.permission.ACCESS_FINE_LOCATION");
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    result);
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    result);
            return;
        } else
            Log.v("Have Permission", "android.Manifest.permission.ACCESS_FINE_LOCATION");
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        Log.v("startLocationUpdates", "finish");
    }

    protected void stopLocationUpdates() {
        Log.v("stopLocationUpdates", "start");
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        Log.v("stopLocationUpdates", "finish");
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        Log.v("onConnected", "Connected to GoogleApiClient");
        if (mCurrentLocation == null) {
            Log.v("mCurrentLocation", "is null");
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.v("No Permission", "android.Manifest.permission.ACCESS_FINE_LOCATION");
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        result);
                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                        result);
                return;
            } else
                Log.v("Have Permission", "android.Manifest.permission.ACCESS_FINE_LOCATION");
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            Log.v("onConnected", "first if = true");
        } else {
            Log.v("onConnected", "first if = else");
        }
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.v("onLocationChanged", "onLocationChanged");
        mCurrentLocation = location;
        drawMap();
        Toast.makeText(this, getResources().getString(R.string.location_updated_message),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.v(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.v(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        Log.v("onSaveInstanceState", "start");
        savedInstanceState.putBoolean(REQUESTING_LOCATION_UPDATES_KEY, mRequestingLocationUpdates);
        savedInstanceState.putParcelable(LOCATION_KEY, mCurrentLocation);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onStart() {
        Log.v("onStart", "onStart");
        super.onStart();
        if (GPS_enable)
            mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        Log.v("onResume", "onResume");
        mapView.onResume();
        super.onResume();
        if (GPS_enable)
            if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
                startLocationUpdates();
            }
    }

    @Override
    protected void onPause() {
        Log.v("onPause", "onPause");
        mapView.onPause();
        super.onPause();
        if (GPS_enable)
            if (mGoogleApiClient.isConnected()) {
                stopLocationUpdates();
            }
    }

    @Override
    protected void onStop() {
        Log.v("onStop", "onStop");
        if (GPS_enable)
            mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void mapSetLocation() {

        mapView = (MapView) findViewById(R.id.logMapView);
        map = mapView.getMap();

        // Updates the location and zoom of the MapView
        if (mCurrentLocation != null) {
            CameraUpdate cam2 = CameraUpdateFactory.newLatLngZoom(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), 17);
            map.animateCamera(cam2);
            Log.v("mapSetLocation", "animateCamera");
        }
        Log.v("mapSetLocation", "end");
    }

    private void drawMap() {

        LatLng temp = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        routeList.add(temp);
        Log.v("add to ArrayList " + routeList.size(), String.valueOf(temp));
        Log.v("asdfasdf array size", String.valueOf(routeList.size()));
        // Calculate distance
        if (routeList.size() > 1) {

            Location TempA = new Location("Temp A");
            TempA.setLatitude(routeList.get(routeList.size() - 2).latitude);
            TempA.setLongitude(routeList.get(routeList.size() - 2).longitude);

            Location TempB = new Location("Temp B");
            TempB.setLatitude(routeList.get(routeList.size() - 1).latitude);
            TempB.setLongitude(routeList.get(routeList.size() - 1).longitude);
            totalDistance += TempA.distanceTo(TempB) / 1000;
            distance_tv.setText(String.format("%.3f", totalDistance));
        }

        PolylineOptions options = new PolylineOptions().width(10).color(Color.BLUE).geodesic(true);
        for (int i = 0; i < routeList.size(); i++) {
            LatLng point = routeList.get(i);
            options.add(point);
        }
        line = map.addPolyline(options);
    }

    private void ScheduleTask() {
        scheduler =
                Executors.newSingleThreadScheduledExecutor();

        r = new Runnable() {
            @Override
            public void run() {
                // Total seconds of activity
                long timeElapsed = SystemClock.elapsedRealtime() - totalTime.getBase();
                long totalSec = timeElapsed / 1000;

                if (totalDistance > 0) {
                    // Calculate pace
                    pace = totalSec / 60.0000000 / totalDistance;
                    String pace_minute = String.valueOf((long) pace);

                    double temp_second = (pace - (long) pace) * 60;
                    String pace_second = (temp_second > 10) ? String.valueOf((long) temp_second) : "0" + String.valueOf((long) temp_second);
                    Log.v("schedule detail", totalSec + " " + totalDistance);
                    Log.v("schedule pace", pace_minute + ":" + pace_second);
                    final String temptemp = pace_minute + ":" + pace_second;

                    // Calculate calories
                    calories = CaloriesCount(totalSec/60.0);

                    runOnUiThread(new Runnable() {
                        public void run() {
                            pace_tv.setText(temptemp);
                            calories_tv.setText(String.valueOf(calories));
                        }
                    });
                }
            }
        };
        scheduledFuture = scheduler.scheduleAtFixedRate(r, 1L, 1L, TimeUnit.SECONDS);
    }

    private long CaloriesCount(double mins) {
        double temp = mins / 1440.0;
        calories = (long) (calories_base * temp);
        return calories;
    }

    public class uploadToServer extends AsyncTask<Void, Void, String> {

        private ProgressDialog pd = new ProgressDialog(Logging.this);
        protected void onPreExecute() {
            super.onPreExecute();

            temp1 = (String) distance_tv.getText();
            temp2 = (String) pace_tv.getText();
            temp3 = (String) calories_tv.getText();

            // Total seconds of activity
            temp4_timeElapsed = SystemClock.elapsedRealtime() - totalTime.getBase();
            temp5_totalSec = temp4_timeElapsed / 1000;

            pd.setMessage("Uploading!");
            pd.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

            Log.v("PHP", EVENTID + " " + USER_ID + " " + temp1 + " " + temp2 + " " + temp3 + " " + temp5_totalSec + " " + type_of_sports + " " + START_TIME + " " + END_TIME);
            nameValuePairs.add(new BasicNameValuePair("event_id", String.valueOf(EVENTID)));
            nameValuePairs.add(new BasicNameValuePair("user_id", String.valueOf(USER_ID)));
            nameValuePairs.add(new BasicNameValuePair("total_distance", temp1));
            nameValuePairs.add(new BasicNameValuePair("avg_pace", temp2));
            nameValuePairs.add(new BasicNameValuePair("total_calories", temp3));
            nameValuePairs.add(new BasicNameValuePair("total_time", String.valueOf(temp5_totalSec))); // seconds
            nameValuePairs.add(new BasicNameValuePair("type_of_sports", String.valueOf(type_of_sports)));
            nameValuePairs.add(new BasicNameValuePair("start_date", START_DATE));
            nameValuePairs.add(new BasicNameValuePair("end_date", END_DATE));
            nameValuePairs.add(new BasicNameValuePair("start_time", START_TIME));
            nameValuePairs.add(new BasicNameValuePair("end_time", END_TIME));
            nameValuePairs.add(new BasicNameValuePair("coordinates", String.valueOf(routeList)));

            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://raymondchan1179.dlinkddns.com/fyp/logging.php");
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                String st = EntityUtils.toString(response.getEntity());
                Log.v("log_tag", "In the try Loop" + st);

            } catch (Exception e) {
                Log.v("log_tag", "Error in http connection " + e.toString());
            }
            return "Success";
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            pd.hide();
            pd.dismiss();
            finish();
        }
    }


    private void setEndTime() {
        // Set end Time
        Calendar c = Calendar.getInstance();
        String year = String.valueOf(c.get(Calendar.YEAR));
        String month = AddZero(c.get(Calendar.MONTH)+1);
        String day = AddZero(c.get(Calendar.DATE));
        String hour = AddZero(c.get(Calendar.HOUR_OF_DAY));
        String minutes = AddZero(c.get(Calendar.MINUTE));
        String seconds = AddZero(c.get(Calendar.SECOND));
        END_TIME = hour + ":" + minutes + ":" + seconds;
        END_DATE = year + "-" + month + "-" + day;
    }

    private int getAge(String prefs){
        int year, month, day;
        year = Integer.parseInt(prefs.substring(0,4));
        month = Integer.parseInt(prefs.substring(5,7));
        day = Integer.parseInt(prefs.substring(8,10));

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }

        Integer ageInt = new Integer(age);

        return ageInt;
    }

    @Override
    public void onBackPressed() {
        stopUpdatesButtonHandler(getWindow().getDecorView().getRootView());
    }
}