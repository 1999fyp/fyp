package com.example.ron.fyp.Weather;

import org.json.JSONObject;

public class Myatmosphere implements JSONPopulator {
    private String visibility;

    public String getVisibility() {
        return visibility;
    }


    @Override
    public void populate(JSONObject data) {
        visibility = data.optString("visibility");
    }
}