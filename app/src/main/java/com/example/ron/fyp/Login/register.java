package com.example.ron.fyp.Login;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.dd.processbutton.ProcessButton;
import com.example.ron.fyp.Database.JSONParser;
import com.example.ron.fyp.MainActivity;
import com.example.ron.fyp.R;
import com.wrapp.floatlabelededittext.FloatLabeledEditText;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class register extends AppCompatActivity
        implements gender_dialog.NoticeDialogListener {
    private FormEditText username_ET, password_ET, passwordre_ET, name_ET, dob_ET, email_ET, gender_ET, height_ET, weight_ET, heighta_ET, heightb_ET;
    private TextView doc_ET;
    private ProcessButton submit_BTN;
    private Spinner weight_SP, height_SP;
    private FloatLabeledEditText height_LB;
    Toolbar toolbar;
    private int weight = 0;
    private int height = 0;
    private JSONObject json;
    private Calendar calendar;
    private int mYear, mMonth, mDay, TodayYear, TodayMonth, TodayDay;
    private android.app.DatePickerDialog datePickerDialog;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initialization();
        actionbar();
        hideKeyboard(findViewById(R.id.registerParentLayout));
        username_ET.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        /**         Toolbar onClick   NavigationOnClickListener   **/
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
        heighta_ET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 1) {
                    heightb_ET.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(heightb_ET, InputMethodManager.SHOW_IMPLICIT);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        /**                        weight onItemselected                       **/
        height_SP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0:
                        height_ET.setVisibility(View.VISIBLE);
                        heighta_ET.setVisibility(View.INVISIBLE);
                        heightb_ET.setVisibility(View.INVISIBLE);
                        doc_ET.setVisibility(View.INVISIBLE);
                        heighta_ET.setText("");
                        heightb_ET.setText("");
                        weight_ET.setNextFocusDownId(R.id.height_ET);
                        break;
                    case 1:
                        height_ET.setVisibility(View.INVISIBLE);
                        heighta_ET.setVisibility(View.VISIBLE);
                        heightb_ET.setVisibility(View.VISIBLE);
                        doc_ET.setVisibility(View.VISIBLE);
                        height_ET.setText("");
                        heighta_ET.setNextFocusDownId(R.id.heightb_ET);
                        weight_ET.setNextFocusDownId(R.id.heighta_ET);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        /**                       on Submit listener               **/
        submit_BTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkvaild();
            }
        });
        gender_ET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gender_dialog GD = new gender_dialog();
                GD.show(getFragmentManager(), "TAG");
            }
        });
        heightb_ET.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    checkvaild();
                    return true;
                } else {
                    return false;
                }
            }
        });
        height_ET.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    checkvaild();
                    return true;
                } else {
                    return false;
                }
            }
        });
        dob_ET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(0);
                datePickerDialog.updateDate(mYear, mMonth, mDay);
            }
        });
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void Messagepass(String gender) {
        gender_ET.setText(gender);
    }

    public boolean checkIntegrity() {
        boolean Valid = true;
        int heighta = 0;
        int heightb = 0;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = formatter.parse(dob_ET.getText().toString());
            Date date1 = formatter.parse(setDateFormat(TodayYear, TodayMonth, TodayDay));
            if (date1.before(date) || date1.getYear() - date.getYear() >= 100) {
                Valid = false;
                dob_ET.setError("InValid Date");
                YoYo.with(Techniques.Shake).playOn(dob_ET);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (!heighta_ET.getText().toString().isEmpty()) {
            heighta = Integer.parseInt(heighta_ET.getText().toString());
        }
        if (!heightb_ET.getText().toString().isEmpty()) {
            heightb = Integer.parseInt(heightb_ET.getText().toString());
        }
        if (!weight_ET.getText().toString().isEmpty()) {
            weight = Integer.parseInt(weight_ET.getText().toString());
        }
        if (!height_ET.getText().toString().isEmpty()) {
            height = Integer.parseInt(height_ET.getText().toString());
        }
        if (weight_SP.getSelectedItem().toString().equals("lb")) {
            weight = (int) (weight * 0.45359237);
        }
        if (height_SP.getSelectedItem().toString().equals("cm")) {

        } else {
            height = heighta * 100;
            height += heightb;
        }
        if (!passwordre_ET.getText().toString().equals(password_ET.getText().toString())) {
            passwordre_ET.setError("Confirm your password");
            YoYo.with(Techniques.Shake).playOn(passwordre_ET);
            Valid = false;
        }
        if (weight > 600 || weight < 20) {
            weight_ET.setError("InValid weight");
            YoYo.with(Techniques.Shake).playOn(weight_ET);
            Valid = false;
        }
        if (height > 300 || height < 50) {
            YoYo.with(Techniques.Shake).playOn(height_ET);
            height_ET.setError("InValid height");
            YoYo.with(Techniques.Shake).playOn(heighta_ET);
            YoYo.with(Techniques.Shake).playOn(heightb_ET);
            heightb_ET.setError("InValid height");
            height_ET.setError("InValid height");
            Valid = false;
        }
        return Valid;
    }

    public void checkvaild() {
        if (isNetworkConnected(getApplicationContext())) {
            FormEditText[] allFields = {username_ET, password_ET, email_ET, passwordre_ET, name_ET, dob_ET, gender_ET, height_ET, weight_ET};
            boolean allValid = true;
            /**                 Check Valid             **/
            if (checkIntegrity()) {
                for (FormEditText field : allFields) {
                    allValid = field.testValidity() && allValid;
                }
            } else {
                allValid = false;
            }
            for (int testvalid = 0; testvalid < 9; testvalid++) {
                if (!allFields[testvalid].testValidity()) {
                    YoYo.with(Techniques.Shake).playOn(allFields[testvalid]);
                }

            }

            submit_BTN.setProgress(50);
            if (allValid) {
                /**                   php            **/
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                JSONParser jsonParser = new JSONParser();
                params.add(new BasicNameValuePair("username", username_ET.getText().toString()));
                params.add(new BasicNameValuePair("password", password_ET.getText().toString()));
                params.add(new BasicNameValuePair("name", name_ET.getText().toString()));
                params.add(new BasicNameValuePair("gender", gender_ET.getText().toString()));
                params.add(new BasicNameValuePair("dob", dob_ET.getText().toString()));
                params.add(new BasicNameValuePair("email", email_ET.getText().toString()));
                params.add(new BasicNameValuePair("weight", String.valueOf(weight)));
                params.add(new BasicNameValuePair("height", String.valueOf(height)));
                json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/register.php", "GET", params);
                Log.v("RegisterJSON", String.valueOf(json));
                try {
                    if(json.getString("success").equals("1")){
                        finish();
                        Toast.makeText(this,"Register success",Toast.LENGTH_SHORT).show();
                        submit_BTN.setProgress(100);
                    }else{
                        if(json.getString("success").equals("2")){
                            username_ET.setError("Duplicate username");
                            YoYo.with(Techniques.Shake).playOn(username_ET);
                            submit_BTN.setProgress(0);
                        }else{
                            email_ET.setError("Duplicate email");
                            YoYo.with(Techniques.Shake).playOn(email_ET);
                            submit_BTN.setProgress(0);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /**                   php            **/

            } else {
                submit_BTN.setProgress(0);
            }
        } else {
            submit_BTN.setProgress(-1);
            submit_BTN.setText("Error");
            Toast.makeText(getApplicationContext(), "Network Disconnect", Toast.LENGTH_SHORT).show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    submit_BTN.setProgress(0);
                }
            }, 1500);
        }
    }

    public void initialization() {
        username_ET = (FormEditText) findViewById(R.id.username_ET);
        password_ET = (FormEditText) findViewById(R.id.password_ET);
        passwordre_ET = (FormEditText) findViewById(R.id.passwordre_ET);
        name_ET = (FormEditText) findViewById(R.id.name_ET);
        email_ET = (FormEditText) findViewById(R.id.email_ET);
        dob_ET = (FormEditText) findViewById(R.id.DOB_ET);
        height_ET = (FormEditText) findViewById(R.id.height_ET);
        weight_ET = (FormEditText) findViewById(R.id.weight_ET);
        gender_ET = (FormEditText) findViewById(R.id.gender_ET);
        toolbar = (Toolbar) findViewById(R.id.create_register_toolbar);
        submit_BTN = (ProcessButton) findViewById(R.id.submit_BTN);
        calendar = Calendar.getInstance();
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);
        TodayYear = mYear;
        TodayMonth = mMonth;
        TodayDay = mDay;
        weight_SP = (Spinner) findViewById(R.id.weight_SP);
        height_SP = (Spinner) findViewById(R.id.height_SP);
        heighta_ET = (FormEditText) findViewById(R.id.heighta_ET);
        heightb_ET = (FormEditText) findViewById(R.id.heightb_ET);
        doc_ET = (TextView) findViewById(R.id.doc_TXT);
        height_LB = (FloatLabeledEditText) findViewById(R.id.view4);

    }

    public void actionbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(R.drawable.close);
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        datePickerDialog = new android.app.DatePickerDialog(this, new android.app.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                mYear = year;
                mMonth = month;
                mDay = day;
                dob_ET.setText(setDateFormat(year, month, day));
                Log.v("Date of birth:::::::::", setDateFormat(year, month, day));
            }
        }, mYear, mMonth, mDay);
        return datePickerDialog;
    }

    private String setDateFormat(int year, int monthOfYear, int dayOfMonth) {
        return String.valueOf(year) + "-"
                + String.valueOf(monthOfYear + 1) + "-"
                + String.valueOf(dayOfMonth);
    }

    public boolean isNetworkConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable();
            }
        }
        return false;
    }
}