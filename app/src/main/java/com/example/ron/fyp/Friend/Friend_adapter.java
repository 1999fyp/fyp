package com.example.ron.fyp.Friend;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ron.fyp.Database.JSONParser;
import com.example.ron.fyp.Profile.ImageLoadTask;
import com.example.ron.fyp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by User on 23/4/2016.
 */
public class Friend_adapter extends BaseAdapter implements View.OnClickListener {
    private ArrayList<String> friend_id = new ArrayList<>();
    private ArrayList<String> full_name = new ArrayList<>();
    private ArrayList<String> icon = new ArrayList<>();
    private ArrayList<String> status = new ArrayList<>();
    private TextView friend_name1;
    private int position;
    private Button status_BTN, status_BTN1, status_BTN2;
    private SharedPreferences prefs = null;
    private CircleImageView friend_icon1;
    Context c;

    Friend_adapter(ArrayList<String> user_id, ArrayList<String> full_name, ArrayList<String> icon, ArrayList<String> status, Context c) {
        this.friend_id = user_id;
        this.full_name = full_name;
        this.icon = icon;
        this.status = status;
        this.friend_id = user_id;
        this.c = c;
    }

    @Override
    public int getCount() {
        return friend_id.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = LayoutInflater.from(c).inflate(R.layout.friend_item, parent, false);
        this.position = position;

        friend_name1 = (TextView) v.findViewById(R.id.friend_name);
        friend_icon1 = (CircleImageView) v.findViewById(R.id.friend_icon);
        status_BTN = (Button) v.findViewById(R.id.status_BTN);
        status_BTN1 = (Button) v.findViewById(R.id.status_BTN1);
        status_BTN2 = (Button) v.findViewById(R.id.status_BTN2);
        status_BTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(status_BTN.getText()=="Requested")
                {
                    Toast.makeText(c," Delete requested success", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(c," Delete success", Toast.LENGTH_SHORT).show();
                }
                new ControlFriend(1,friend_id.get(position)).execute();

            }
        });
        status_BTN1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ControlFriend(2,friend_id.get(position)).execute();
                Toast.makeText(c,"Accept Success", Toast.LENGTH_SHORT).show();
            }
        });
        status_BTN2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ControlFriend(1,friend_id.get(position)).execute();
                Toast.makeText(c,"Reject Success", Toast.LENGTH_SHORT).show();
            }
        });
        friend_name1.setText(full_name.get(position));
        switch (status.get(position)) {
            case "0":
                status_BTN.setVisibility(View.INVISIBLE);
                status_BTN1.setVisibility(View.VISIBLE);
                status_BTN.setEnabled(true);
                status_BTN2.setVisibility(View.VISIBLE);

                break;
            case "1":
                status_BTN.setVisibility(View.VISIBLE);
                status_BTN.setText("Requested");
                status_BTN.setBackgroundColor(Color.GRAY);
                status_BTN1.setVisibility(View.INVISIBLE);
                status_BTN2.setVisibility(View.INVISIBLE);
                break;
            case "2":
                status_BTN.setVisibility(View.VISIBLE);
                status_BTN.setText("Unfriend");
                status_BTN.setEnabled(true);
                status_BTN.setBackgroundColor(Color.RED);
                status_BTN1.setVisibility(View.INVISIBLE);
                status_BTN2.setVisibility(View.INVISIBLE);
                break;
        }
        new ImageLoadTask("http://raymondchan1179.dlinkddns.com/fyp/" + icon.get(position), friend_icon1).execute();
        return v;
    }

    @Override
    public void onClick(View v) {
        Log.d("abccccc", v.toString());

    }
    class ControlFriend extends AsyncTask<Void, Void, Void> {

        private JSONObject json = null;
        private int user_ID;
        private int method;
        private String friend_id_str;
        ControlFriend(int method,String friend_name){
            this.method = method;
            this.friend_id_str = friend_name;
        }
        @Override
        protected Void doInBackground(Void... mparams) {
            prefs = PreferenceManager.getDefaultSharedPreferences(c);
            user_ID = prefs.getInt("userID", user_ID);
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            JSONParser jsonParser = new JSONParser();
            params.add(new BasicNameValuePair("user_id", String.valueOf(user_ID)));
            params.add(new BasicNameValuePair("friend_id", String.valueOf(friend_id_str)));
            params.add(new BasicNameValuePair("method", String.valueOf(method)));
            Log.d("tetetetet", String.valueOf(user_ID)+ String.valueOf(friend_id)+String.valueOf(method));
            json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/friend.php", "GET", params);

            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new Friend_adapter(friend_id, full_name, icon, status, c);
        }
    }
}
