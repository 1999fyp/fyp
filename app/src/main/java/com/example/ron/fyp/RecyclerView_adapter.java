package com.example.ron.fyp;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RecyclerView_adapter extends RecyclerView.Adapter<RecyclerView_adapter.ViewHolder>{

    private final FragmentActivity mActivity;
    //String[] title = {"Detail Information", "Friends list", "Activity Log","Log Out"};
    String[] title = {"Detail Information","Activity Log","Log Out"};
    OnItemClickListener mItemClickListener;
    public RecyclerView_adapter(FragmentActivity mActivity) {
        this.mActivity = mActivity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent , int viewType) {
        final LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        final View sView = mInflater.inflate(R.layout.tab3_list, parent, false);
        return new ViewHolder(sView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder , int position) {
        holder.title.setText(title[position]);
        switch (position){
            case 0:holder.profire_IMG.setImageResource(R.drawable.profile);break;
            case 1:holder.profire_IMG.setImageResource(R.drawable.friend);break;
            case 2:holder.profire_IMG.setImageResource(R.drawable.log);break;
            case 3:holder.profire_IMG.setImageResource(R.drawable.logout);break;
        }
    }

    @Override
    public int getItemCount() {
        return title.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnTouchListener {

        TextView title;
        ImageView profire_IMG;

        public ViewHolder(View view) {
            super(view);
            title = (TextView)view.findViewById(R.id.list_title);
            profire_IMG= (ImageView)view.findViewById(R.id.profile_IMG);
            view.setOnClickListener(this);
            view.setOnTouchListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, getPosition());
            }
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if(event.getAction()==MotionEvent.ACTION_DOWN){
                switch (getPosition()) {
                    case 0:
                        profire_IMG.setImageResource(R.drawable.profile_onclick);
                        break;
                    case 1:
                        profire_IMG.setImageResource(R.drawable.friend_onclick);
                        break;
                    case 2:
                        profire_IMG.setImageResource(R.drawable.log_onclick);
                        break;
                    case 3:
                        profire_IMG.setImageResource(R.drawable.logout_onclick);
                        break;
                }
            }
            if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
                switch (getPosition()) {
                    case 0:
                        profire_IMG.setImageResource(R.drawable.profile);
                        break;
                    case 1:
                        profire_IMG.setImageResource(R.drawable.friend);
                        break;
                    case 2:
                        profire_IMG.setImageResource(R.drawable.log);
                        break;
                    case 3:
                        profire_IMG.setImageResource(R.drawable.logout);
                        break;
                }
            }

            return false;
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view , int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

}