package com.example.ron.fyp.Weather;

import org.json.JSONObject;

/**
 * Created by User on 20/3/2016.
 */
public class Units implements JSONPopulator {
    private String temperature;

    public String getTemperature() {
        return temperature;
    }

    @Override
    public void populate(JSONObject data) {
        temperature = data.optString("temperature");
    }
}
