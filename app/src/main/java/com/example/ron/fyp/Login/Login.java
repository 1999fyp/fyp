package com.example.ron.fyp.Login;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.andreabaccega.widget.FormEditText;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.dd.processbutton.iml.ActionProcessButton;
import com.example.ron.fyp.Database.JSONParser;
import com.example.ron.fyp.MainActivity;
import com.example.ron.fyp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import android.content.SharedPreferences;

public class Login extends AppCompatActivity implements View.OnClickListener {
    private String username, password;
    private FormEditText f1, f2;
    private ActionProcessButton login_BTN, register_BTN;
    private String full_name = "";
    private int user_id = 0;
    private JSONObject json;
    //private static JSONObject json;

private TextView Logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Typeface type = Typeface.createFromAsset(getAssets(),"fonts/lobster.otf");
        Logo=(TextView) findViewById(R.id.LogoText);
        Logo.setTypeface(type);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        f1 = (FormEditText) findViewById(R.id.username_ET);
        f2 = (FormEditText) findViewById(R.id.password_ET);
        login_BTN = (ActionProcessButton) findViewById(R.id.submit_BTN);
        register_BTN = (ActionProcessButton) findViewById(R.id.register_BTN);


        //finish password and login
        f2.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    onClick(findViewById(R.id.submit_BTN));
                    return true;
                } else {
                    return false;
                }
            }
        });

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        if (v == findViewById(R.id.submit_BTN)) {
            login_BTN.setProgress(50);
            username = f1.getText().toString();
            password = f2.getText().toString();
            FormEditText[] allFields = {f1, f2};
            boolean allValid = true;
            for (FormEditText field : allFields) {
                allValid = field.testValidity() && allValid;
            }
            if (!f1.testValidity())
                YoYo.with(Techniques.Shake).playOn(f1);


            if (!f2.testValidity())
                YoYo.with(Techniques.Shake).playOn(f2);


            if (allValid) {
                if (isNetworkConnected(getApplicationContext())) {
                    /**   Start MyAsyncTask   **/
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    JSONParser jsonParser = new JSONParser();
                    params.add(new BasicNameValuePair("username", username));
                    params.add(new BasicNameValuePair("password", password));
                    json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/login.php", "GET", params);
                    // json success tag
                    if (f1.length() < 6) {
                        f1.setError("Username must more than 6 letter ");
                        YoYo.with(Techniques.Shake).playOn(f1);
                        login_BTN.setProgress(0);
                    } else {
                        if (f2.length() < 6) {
                            f2.setError("Password must more than 6 letter ");
                            YoYo.with(Techniques.Shake).playOn(f2);
                            login_BTN.setProgress(0);
                        } else {
                            MyAsyncTask myAsyncTask = new MyAsyncTask();
                            myAsyncTask.execute(100);
                        }
                    }

                } else {
                    login_BTN.setProgress(-1);
                    login_BTN.setText("Error");
                    Toast.makeText(getApplicationContext(), "Network Disconnect", Toast.LENGTH_SHORT).show();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            login_BTN.setProgress(0);
                        }
                    }, 1500);
                }

            } else {
                // EditText are going to appear with an exclamation mark and an explicative message.
                Handler handler = new Handler();
                login_BTN.setProgress(-1);
                handler.postDelayed(new Runnable() {
                    public void run() {
                        login_BTN.setProgress(0);
                    }
                }, 1500);
            }


        } else {
            if (isNetworkConnected(getApplicationContext())) {
                Intent intent = new Intent(this, register.class);
                startActivity(intent);
            } else {
                register_BTN.setProgress(-1);
                register_BTN.setText("Error");
                Toast.makeText(getApplicationContext(), "Network Disconnect", Toast.LENGTH_SHORT).show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        register_BTN.setProgress(0);
                    }
                }, 1500);
            }

        }

    }

    /**
     * Check Network
     **/
    public boolean isNetworkConnected(Context context) {
        if (context != null) {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            if (mNetworkInfo != null) {
                return mNetworkInfo.isAvailable();
            }
        }
        return false;
    }

    @Override
    protected void onResume() {
        f1.setText("raymond");
        f2.setText("123456");
        super.onResume();
    }

    @Override
    protected void onStop() {
        username = "";
        password = "";
        f1.setText("");
        f2.setText("");
        login_BTN.setProgress(0);
        super.onStop();
    }

    /**
     * AsyncTask
     **/
    class MyAsyncTask extends AsyncTask<Integer, Void, Void> {
        /**
         * doInBackground ,  onProgressUpdate ,onPostExecute
         **/
        @Override
        protected void onPreExecute() {
            Log.d("AStest", "onPreExecute");
            /**   1.Execute  **/
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Integer... params) {
            Log.d("AStest", "doInBackground");
            return null;
        }


        @Override
        protected void onProgressUpdate(Void... values) {
            /**   after publishProgress  **/
            super.onProgressUpdate();
        }

        @Override
        protected void onPostExecute(Void s) {
            Log.d("AStest", "onPostExecute");
            /**  after  doInBackground**/
            try {
                if (json.getString("success").equals("1")) {
                    full_name = json.getString("full_name");
                    user_id = json.getInt("user_id");

                    Toast.makeText(getApplicationContext(), "Welcome " + full_name, Toast.LENGTH_SHORT).show();
                    //public abstract SharedPreferences.Editor putString ("userID", username);
                    //public abstract SharedPreferences.Editor putInt ("status", 1);

                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putInt("userID", user_id);
                    editor.putBoolean("status", true);
                    editor.commit();


                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("username", username);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    login_BTN.setProgress(100);
                } else {
                    login_BTN.setProgress(-1);
                    login_BTN.setProgress(0);
                    Toast.makeText(getApplicationContext(), "Wrong Username or Password", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            super.onPostExecute(s);
        }

    }
}

