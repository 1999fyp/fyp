package com.example.ron.fyp.Profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.dd.processbutton.ProcessButton;
import com.example.ron.fyp.R;
import com.isseiaoki.simplecropview.CropImageView;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class CropImage extends Activity {
    Bitmap bmp = null;
    private static final int CROP_RESULTCODE = 9998;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_image);

        String filename = getIntent().getStringExtra("image");
        try {
            FileInputStream is = this.openFileInput(filename);
            bmp = BitmapFactory.decodeStream(is);
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        final CropImageView cropImageView = (CropImageView) findViewById(R.id.cropImageView);
       // final ImageView croppedImageView = (ImageView) findViewById(R.id.croppedImageView);
        cropImageView.setHandleSizeInDp(8);
        cropImageView.setHandleShowMode(CropImageView.ShowMode.SHOW_ALWAYS);
        cropImageView.setGuideShowMode(CropImageView.ShowMode.SHOW_ON_TOUCH);
        cropImageView.setMinFrameSizeInDp(100);

        // Set image for cropping
        cropImageView.setImageBitmap(bmp);

        ProcessButton cropButton = (ProcessButton) findViewById(R.id.crop_button);
        ProcessButton cancel = (ProcessButton) findViewById(R.id.Cancel_BTN);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        cropButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bitmap bitmap = cropImageView.getCroppedBitmap();
                try {
                    //Write file
                    String filename = "bitmap.png";
                    FileOutputStream stream = getApplicationContext().openFileOutput(filename, Context.MODE_PRIVATE);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    if (bitmap != null && !bitmap.isRecycled()) {
                        stream.close();
                        bitmap.recycle();
                        bitmap = null;
                    }
                    //Cleanup

                    //Pop intent
                    Intent in1 = new Intent(getApplicationContext(), CropImage.class);
                    in1.putExtra("image", filename);
                    setResult(CROP_RESULTCODE,in1);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // Get cropped image, and show result.
             //   croppedImageView.setImageBitmap(cropImageView.getCroppedBitmap());
            }
        });
    }

}
