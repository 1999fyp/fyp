package com.example.ron.fyp.Weather;

/**
 * Created by User on 20/3/2016.
 */
public interface WeatherServiceCallback {
    void serviceSuccess(Channel channel);
    void serviceFailure(Exception exception);
}
