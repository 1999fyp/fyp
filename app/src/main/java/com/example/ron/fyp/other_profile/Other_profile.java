package com.example.ron.fyp.other_profile;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ron.fyp.Database.JSONParser;
import com.example.ron.fyp.Event_Detail;
import com.example.ron.fyp.Profile.ImageLoadTask;
import com.example.ron.fyp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.blurry.Blurry;

public class Other_profile extends AppCompatActivity {
    String user_ID, icon, full_name;
    ImageView profile_userimg_background;
    Toolbar otherprofile_toolbar;
    TextView otherprofile_fullname;

    CircleImageView otherprofile_icon;
    private SharedPreferences prefs = null;
    JSONParser jParser = new JSONParser();
    ArrayList<HashMap<String, String>> List;
    ArrayList<HashMap<String, String>> List1;

    JSONArray hosted_JSONArray = null;
    JSONArray joined_JSONArray = null;

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_HOST = "host";
    private static final String TAG_JOIN = "joined";

    private String FEED_URL = "http://raymondchan1179.dlinkddns.com/fyp/pastActivity.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_profile);

        Bundle bundle = getIntent().getExtras();
        user_ID = bundle.getString("user_id");

        otherprofile_toolbar = (Toolbar) findViewById(R.id.otherprofile_toolbar);

        setSupportActionBar(otherprofile_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        otherprofile_toolbar.setNavigationIcon(R.drawable.back);
        otherprofile_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        otherprofile_fullname = (TextView) findViewById(R.id.otherprofile_fullname);
        profile_userimg_background = (ImageView) findViewById(R.id.profile_userimg_background);
        otherprofile_icon = (CircleImageView) findViewById(R.id.otherprofile_icon);
        new ControlFriend(4,"user_ID");
        new GetOtherAskTask().execute();
    }

    private void setData() {
        new ImageLoadTask("http://raymondchan1179.dlinkddns.com/fyp/" + icon, profile_userimg_background).execute();
        new ImageLoadTask("http://raymondchan1179.dlinkddns.com/fyp/" + icon, otherprofile_icon).execute();
        otherprofile_fullname.setText(full_name);
        showListView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_other_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.other_add:
                new ControlFriend(2, user_ID).execute();
                Toast.makeText(getApplicationContext(), "Accept Success", Toast.LENGTH_SHORT).show();
                item.setIcon(R.drawable.tick);
                break;
            default:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setBlur() {
        profile_userimg_background.postDelayed(new Runnable() {
            @Override
            public void run() {
                Blurry.with(getApplicationContext())
                        .radius(10)
                        .sampling(8)
                        .async()
                        .capture(profile_userimg_background)
                        .into(profile_userimg_background);

            }
        }, 1000);
    }

    class GetOtherAskTask extends AsyncTask<Void, Void, Void> {
        private JSONObject json = null;

        public GetOtherAskTask() {
        }

        @Override
        protected Void doInBackground(Void... mparams) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            JSONParser jsonParser = new JSONParser();

            params.add(new BasicNameValuePair("user_ID", String.valueOf(user_ID)));
            json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/profile.php", "GET", params);
            try {
                icon = json.getString("icon");
                full_name = json.getString("full_name");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            setData();
            setBlur();
        }
    }

    private void showListView() {
        ListView lv = (ListView) findViewById(R.id.hosted_listview);
        ListView lv2 = (ListView) findViewById(R.id.joined_listview);
        TextView tv = (TextView) findViewById(R.id.hosted_tv);
        TextView tv2 = (TextView) findViewById(R.id.joined_tv);

        List = new ArrayList<HashMap<String, String>>();
        List1 = new ArrayList<HashMap<String, String>>();

        final List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("user_id", user_ID));
        JSONObject json = jParser.makeHttpRequest(FEED_URL, "GET", params);
        Log.i("JSON Response: ", json.toString());

        try {
            int success = json.getInt(TAG_SUCCESS);

            if (success == 1) {
                hosted_JSONArray = json.getJSONArray(TAG_HOST);
                joined_JSONArray = json.getJSONArray(TAG_JOIN);
                Log.v("joined_JSONArray", String.valueOf(joined_JSONArray));

                for (int i = 0; i < hosted_JSONArray.length(); i++) {
                    JSONObject c = hosted_JSONArray.getJSONObject(i);

                    // Storing each json item in variable
                    String event_id = c.getString("event_id");
                    String event_name = c.getString("event_name");
                    String type_id = c.getString("type_id");

                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    map.put("event_id", event_id);
                    map.put("event_name", event_name);
                    map.put("type_id", type_id);

                    // adding HashList to ArrayList
                    List.add(map);
                }

                for (int i = 0; i < joined_JSONArray.length(); i++) {
                    JSONObject c = joined_JSONArray.getJSONObject(i);

                    // Storing each json item in variable
                    String event_id = c.getString("event_id");
                    String event_name = c.getString("event_name");
                    String type_id = c.getString("type_id");

                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    map.put("event_id", event_id);
                    map.put("event_name", event_name);
                    map.put("type_id", type_id);

                    // adding HashList to ArrayList
                    List1.add(map);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (hosted_JSONArray.length() > 0)
            tv.setVisibility(View.VISIBLE);
        if (joined_JSONArray.length() > 0)
            tv2.setVisibility(View.VISIBLE);

        SimpleAdapter adapter = new SimpleAdapter(
                this, List,
                R.layout.other_profile_list_item, new String[]{"event_name"},
                new int[]{R.id.other_profile_event_name});
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // When clicked, show a toast with the TextView text
                HashMap<String, String> map = (HashMap<String, String>) parent.getItemAtPosition(position);
                String value = map.get("event_id");

                Intent intent = new Intent(getApplicationContext(), Event_Detail.class);
                Bundle bundle = new Bundle();
                bundle.putString("eventID", value);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        SimpleAdapter adapter2 = new SimpleAdapter(
                this, List1,
                R.layout.other_profile_list_item, new String[]{"event_name"},
                new int[]{R.id.other_profile_event_name});
        lv2.setAdapter(adapter2);
        lv2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // When clicked, show a toast with the TextView text
                HashMap<String, String> map = (HashMap<String, String>) parent.getItemAtPosition(position);
                String value = map.get("event_id");

                Intent intent = new Intent(getApplicationContext(), Event_Detail.class);
                Bundle bundle = new Bundle();
                bundle.putString("eventID", value);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    class ControlFriend extends AsyncTask<Void, Void, Void> {

        private JSONObject json = null;
        private int user_ID;
        private int method;
        private String friend_id_str,status;

        ControlFriend(int method, String friend_name) {
            this.method = method;
            this.friend_id_str = friend_name;
        }

        @Override
        protected Void doInBackground(Void... mparams) {
            prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            user_ID = prefs.getInt("userID", user_ID);
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            JSONParser jsonParser = new JSONParser();
            params.add(new BasicNameValuePair("user_id", String.valueOf(user_ID)));
            params.add(new BasicNameValuePair("friend_id", String.valueOf(friend_id_str)));
            params.add(new BasicNameValuePair("method", String.valueOf(method)));
            json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/friend.php", "GET", params);
            try {
                status =json.getString("status");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(status != "2"){
                getSupportActionBar().setIcon(R.drawable.tick);
            }
            if(status == "1"){

            }
        }
    }
}
