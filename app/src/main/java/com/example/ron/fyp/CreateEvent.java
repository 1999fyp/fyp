package com.example.ron.fyp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.ron.fyp.Database.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class CreateEvent extends AppCompatActivity {

    private Spinner eventTargetSpinner;
    private Spinner eventTypeSpinner;
    private ArrayAdapter<String> targetList;
    private ArrayAdapter<String> eventList;

    private Calendar calendar;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private String hourString, minuteString, ampmString, privacy;
    private android.app.DatePickerDialog startDatePickerDialog;
    private android.app.DatePickerDialog endDatePickerDialog;
    private android.app.TimePickerDialog startTimePickerDialog;
    private android.app.TimePickerDialog endTimePickerDialog;

    private com.rengwuxian.materialedittext.MaterialEditText startDateEditText, endDateEditText, startTimeEditText, endTimeEditText;
    Toolbar toolbar;
    private Button submitButton;
    private JSONObject json;
    private com.rengwuxian.materialedittext.MaterialEditText eventName, eventStartDate, eventEndDate,
            eventStartTime, eventEndTime, eventCapacity, eventDescription, eventStartingPoint, eventEndingPoint, eventCredit;
    private int eventTypeID, eventHostID, defValue;
    private double start_lat, start_long, end_lat, end_long;
    private Spinner eventType, eventTarget;
    private LinearLayout capacityLayoutVisibility;
    private TextView eventTimeErrorMessage;
    int num = 0;
    boolean validTime = true, validCapacity = false, validName, validDescription, validLocation, validDate = true;
    boolean valid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_event);
        initialization();
        hideKeyboard(findViewById(R.id.create_view));
        eventName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        calendar = Calendar.getInstance();
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);
        mHour = calendar.get(Calendar.HOUR_OF_DAY);
        mMinute = calendar.get(Calendar.MINUTE);
        Button addBtn = (Button) findViewById(R.id.create_event_add_peopleBtn);
        Button dropBtn = (Button) findViewById(R.id.create_event_drop_peopleBtn);
        final com.rengwuxian.materialedittext.MaterialEditText editText =
                (com.rengwuxian.materialedittext.MaterialEditText) findViewById(R.id.create_event_maximum_participant);
        addBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!editText.getText().toString().isEmpty()){
                    num = Integer.valueOf(editText.getText().toString());
                    num++;
                    editText.setText(String.valueOf(num));
                }
                else {
                    num++;
                    editText.setText(String.valueOf(num));
                }
            }
        });
        dropBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!editText.getText().toString().isEmpty()){
                    num = Integer.valueOf(editText.getText().toString());
                    if (num <= 1) {
                        editText.setText(String.valueOf(num));
                    }
                    else{
                        num--;
                        editText.setText(String.valueOf(num));
                    }
                }
            }
        });

        eventTargetSpinner = (Spinner) findViewById(R.id.event_target_spinner);
        String[] targetType = {"Public Event (For everyone)", "Private Event (For friends only)", "Individual only"};
        toolbar = (Toolbar) findViewById(R.id.create_toolbar);
        targetList = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, targetType);
        targetList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        eventTargetSpinner.setAdapter(targetList);

        eventTypeSpinner = (Spinner) findViewById(R.id.event_type_spinner);
        String[] eventType = {"Running", "Cycling", "Hiking"};
        eventList = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, eventType);
        eventList.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        eventTypeSpinner.setAdapter(eventList);

        startDateEditText = (com.rengwuxian.materialedittext.MaterialEditText) findViewById(R.id.create_event_startDateSet);
        startDateEditText.setText(setDateFormat(mYear, mMonth, mDay));
        startDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(0);
                startDatePickerDialog.updateDate(mYear, mMonth, mDay);
            }
        });

        endDateEditText = (com.rengwuxian.materialedittext.MaterialEditText) findViewById(R.id.create_event_endDateSet);
        endDateEditText.setText(setDateFormat(mYear, mMonth, mDay));
        endDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(3);
                endDatePickerDialog.updateDate(mYear, mMonth, mDay);
            }
        });

        startTimeEditText = (com.rengwuxian.materialedittext.MaterialEditText) findViewById(R.id.create_event_start_time);
        startTimeEditText.setText(timeFormatting());
        startTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(1);
                //datePickerDialog.updateDate(mYear, mMonth, mDay);
                startTimePickerDialog.updateTime(mHour, mMinute);
            }
        });

        endTimeEditText = (com.rengwuxian.materialedittext.MaterialEditText) findViewById(R.id.create_event_end_time);
        endTimeEditText.setText(timeFormatting());
        endTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(2);
                endTimePickerDialog.updateTime(mHour, mMinute);
            }
        });

        checkRunTimeValidity();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationIcon(R.drawable.close);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onBackPressed();
            }
        });
        submitButton = (Button) findViewById(R.id.createEventSubmitBtn);
        submitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                submitEventForm();
            }
        });

        eventTargetSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                capacityLayoutVisibility = (LinearLayout) findViewById(R.id.event_capacity_layout);
                if (position == 2) {
                    capacityLayoutVisibility.setVisibility(View.GONE);
                    eventCapacity.setText("1");
                }
                else if (position == 0 || position == 1)
                    capacityLayoutVisibility.setVisibility(View.VISIBLE);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        eventTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ImageView typeIcon = (ImageView)findViewById(R.id.create_event_type);
                if (position == 1)
                    typeIcon.setImageResource(R.drawable.create_event_cycling);
                else if (position == 2)
                    typeIcon.setImageResource(R.drawable.create_event_hiking);
                else
                    typeIcon.setImageResource(R.drawable.create_event_type);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void initialization() {
        eventName = (com.rengwuxian.materialedittext.MaterialEditText) findViewById(R.id.event_name);
        eventType = (Spinner) findViewById(R.id.event_type_spinner);
        eventTarget = (Spinner) findViewById(R.id.event_target_spinner);
        eventDescription = (com.rengwuxian.materialedittext.MaterialEditText) findViewById(R.id.description);
        eventStartDate = (com.rengwuxian.materialedittext.MaterialEditText) findViewById(R.id.create_event_startDateSet);
        eventEndDate = (com.rengwuxian.materialedittext.MaterialEditText) findViewById(R.id.create_event_endDateSet);
        eventStartTime = (com.rengwuxian.materialedittext.MaterialEditText) findViewById(R.id.create_event_start_time);
        eventEndTime = (com.rengwuxian.materialedittext.MaterialEditText) findViewById(R.id.create_event_end_time);
        eventCapacity = (com.rengwuxian.materialedittext.MaterialEditText) findViewById(R.id.create_event_maximum_participant);
        eventStartingPoint = (com.rengwuxian.materialedittext.MaterialEditText) findViewById(R.id.create_event_startLocation);
        eventEndingPoint = (com.rengwuxian.materialedittext.MaterialEditText) findViewById(R.id.create_event_endLocation);
        eventCredit = (com.rengwuxian.materialedittext.MaterialEditText) findViewById(R.id.create_event_credit);
        eventTimeErrorMessage = (TextView) findViewById(R.id.eventTimeErrorMessage);
    }

    public String timeFormatting() {
        String completeTimeString;
        if (calendar.get(Calendar.HOUR_OF_DAY) < 10)
            hourString = "0" + calendar.get(Calendar.HOUR_OF_DAY);
        else if (calendar.get(Calendar.HOUR_OF_DAY) > 12 && (calendar.get(Calendar.HOUR_OF_DAY) - 12) >= 10 )
            hourString = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY) - 12);
        else if (calendar.get(Calendar.HOUR_OF_DAY) > 12 && (calendar.get(Calendar.HOUR_OF_DAY) - 12) < 10)
            hourString = "0" + String.valueOf(calendar.get(Calendar.HOUR_OF_DAY) - 12);
        else
            hourString = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
        if (calendar.get(Calendar.MINUTE) < 10)
            minuteString = "0" + calendar.get(Calendar.MINUTE);
        else
            minuteString = String.valueOf(calendar.get(Calendar.MINUTE));
        if (calendar.get(Calendar.HOUR_OF_DAY) < 12)
            ampmString = "AM";
        else
            ampmString = "PM";
        completeTimeString = hourString + ":" + minuteString + " " + ampmString;
        return completeTimeString;
    }

    public void checkRunTimeValidity() {
        TextWatcher checkEventName = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (eventName.getText().toString().isEmpty()) {
                    eventName.setError("Event name cannot be empty");
                } else if (!eventName.getText().toString().isEmpty() && eventName.getError() == "Event name cannot be empty") {
                    eventName.setError(null);
                    validName = true;
                } else
                    validName = true;
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (eventName.getText().toString().isEmpty()) {
                    eventName.setError("Event name cannot be empty");
                } else if (!eventName.getText().toString().isEmpty() && eventName.getError() == "Event name cannot be empty") {
                    eventName.setError(null);
                    validName = true;
                } else
                    validName = true;
            }
        };
        TextWatcher checkEventDescription = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (eventDescription.getText().toString().isEmpty()) {
                    eventDescription.setError("Description cannot be empty");
                } else if (!eventDescription.getText().toString().isEmpty() && eventDescription.getError() == "Description cannot be empty") {
                    eventDescription.setError(null);
                    validDescription = true;
                } else
                    validDescription = true;
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (eventDescription.getText().toString().isEmpty()) {
                    eventDescription.setError("Description cannot be empty");
                } else if (!eventDescription.getText().toString().isEmpty() && eventDescription.getError() == "Description cannot be empty") {
                    eventDescription.setError(null);
                    validDescription = true;
                } else
                    validDescription = true;
            }
        };
        TextWatcher checkEventCapacity = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (eventTarget.getSelectedItem().toString().equals("Individual only") && eventCapacity.getText().toString().isEmpty()) {
                    validCapacity = true;
                } else {
                    if (eventCapacity.getText().toString().isEmpty()) {
                        eventCapacity.setError("Maximum participants cannot be empty");
                        validCapacity = false;
                    } else if (!eventCapacity.getText().toString().isEmpty() && eventCapacity.getError() == "Maximum participants cannot be empty") {
                        eventCapacity.setError(null);
                        validCapacity = true;
                    }
                }
                if (!eventCapacity.getText().toString().isEmpty()) {
                    if (Integer.valueOf(eventCapacity.getText().toString()) > 10000) {
                        eventCapacity.setError("Too many participants");
                        validCapacity = false;
                    } else if (Integer.valueOf(eventCapacity.getText().toString()) == 0) {
                        eventCapacity.setError("Maximum Participant must be at least 1");
                        validCapacity = false;
                    }
                    else{
                        eventCapacity.setError(null);
                        validCapacity = true;
                    }
                }
                else{
                    eventCapacity.setError(null);
                    validCapacity = true;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (eventTarget.getSelectedItem().toString().equals("Individual only") && eventCapacity.getText().toString().isEmpty()) {
                    validCapacity = true;
                } else {
                    if (eventCapacity.getText().toString().isEmpty()) {
                        eventCapacity.setError("Maximum participants cannot be empty");
                        validCapacity = false;
                    } else if (!eventCapacity.getText().toString().isEmpty() && eventCapacity.getError() == "Maximum participants cannot be empty") {
                        eventCapacity.setError(null);
                        validCapacity = true;
                    }
                }
                if (!eventCapacity.getText().toString().isEmpty()) {
                    if (Integer.valueOf(eventCapacity.getText().toString()) > 10000) {
                        eventCapacity.setError("Too many participants");
                        validCapacity = false;
                    } else if (Integer.valueOf(eventCapacity.getText().toString()) == 0) {
                        eventCapacity.setError("Maximum Participant must be at least 1");
                        validCapacity = false;
                    }
                    else{
                        eventCapacity.setError(null);
                        validCapacity = true;
                    }
                }
                else{
                    eventCapacity.setError(null);
                    validCapacity = true;
                }
            }
        };

        TextWatcher checkEventTime = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                valid = true;
                String checkStartTimeAMPM = eventStartTime.getText().toString();
                String checkEndTimeAMPM = eventEndTime.getText().toString();
                // If event start and end on same day
                if (eventStartDate.getText().toString().equals(eventEndDate.getText().toString())) {

                    // Check start PM, end AM (e.g. start 5:00PM, end 5:00AM)
                    if (checkStartTimeAMPM.substring(6).equals("PM") &&
                            checkEndTimeAMPM.substring(6).equals("AM")) {
                        eventStartTime.setTextColor(getResources().getColor(R.color.RealRed));
                        eventEndTime.setTextColor(getResources().getColor(R.color.RealRed));
                        eventTimeErrorMessage.setVisibility(View.VISIBLE);
                        validTime = false;
                    }

                    // Check start hour > end hour, same AM PM (e.g. start 5:00PM, end 4:00PM)
                    else if (Integer.parseInt(checkStartTimeAMPM.substring(0, 2)) >
                            Integer.parseInt(checkEndTimeAMPM.substring(0, 2)) &&
                            checkEndTimeAMPM.substring(6).equals(checkStartTimeAMPM.substring(6))) {
                        // Check Start hour = 12:00 PM, if yes, this will be valid
                        // (e.g. start 12:00PM, end 7:00PM). AM PM is checked in the if above
                        if (checkStartTimeAMPM.substring(0,2).equals("12") &&
                                checkStartTimeAMPM.substring(6).equals("PM")){
                            eventStartTime.setTextColor(getResources().getColor(R.color.black));
                            eventEndTime.setTextColor(getResources().getColor(R.color.black));
                            eventTimeErrorMessage.setVisibility(View.GONE);
                            valid = true;
                        }
                        else{
                            eventStartTime.setTextColor(getResources().getColor(R.color.RealRed));
                            eventEndTime.setTextColor(getResources().getColor(R.color.RealRed));
                            eventTimeErrorMessage.setVisibility(View.VISIBLE);
                            validTime = false;
                        }
                    }

                    // Check Start hour > end hour = 12:00PM, same AM PM (e.g. start 2:00PM, end 12:00PM)
                    else if (checkStartTimeAMPM.substring(6).equals("PM") &&
                            !checkStartTimeAMPM.substring(0,2).equals("12") &&
                            checkEndTimeAMPM.substring(6).equals("PM") &&
                            checkEndTimeAMPM.substring(0,2).equals("12")){
                        eventStartTime.setTextColor(getResources().getColor(R.color.RealRed));
                        eventEndTime.setTextColor(getResources().getColor(R.color.RealRed));
                        eventTimeErrorMessage.setVisibility(View.VISIBLE);
                        validTime = false;
                    }

                    // Check same hour, same AM PM, wrong minutes (e.g. start 5:30PM, end 5:00PM)
                    else if (checkStartTimeAMPM.substring(6).equals(checkEndTimeAMPM.substring(6)) &&
                            checkStartTimeAMPM.substring(0, 2).equals(checkEndTimeAMPM.substring(0, 2))) {
                        int checkStartMinutes, checkEndMinutes;
                        checkStartMinutes = Integer.parseInt(checkStartTimeAMPM.substring(3, 5));
                        checkEndMinutes = Integer.parseInt(checkEndTimeAMPM.substring(3, 5));
                        if (checkStartMinutes > checkEndMinutes) {
                            eventTimeErrorMessage.setText("Invalid start and end time on the same day");
                            eventStartTime.setTextColor(getResources().getColor(R.color.RealRed));
                            eventEndTime.setTextColor(getResources().getColor(R.color.RealRed));
                            eventTimeErrorMessage.setVisibility(View.VISIBLE);
                            validTime = false;
                        } else {
                            eventStartTime.setTextColor(getResources().getColor(R.color.black));
                            eventEndTime.setTextColor(getResources().getColor(R.color.black));
                            eventTimeErrorMessage.setVisibility(View.GONE);
                            validTime = true;
                        }
                    }

                    // Valid input on the same day
                    else {
                        eventStartTime.setTextColor(getResources().getColor(R.color.black));
                        eventEndTime.setTextColor(getResources().getColor(R.color.black));
                        eventTimeErrorMessage.setVisibility(View.GONE);
                        validTime = true;
                    }
                }
                else {
                    eventStartTime.setTextColor(getResources().getColor(R.color.black));
                    eventEndTime.setTextColor(getResources().getColor(R.color.black));
                    eventTimeErrorMessage.setVisibility(View.GONE);
                    validTime = true;
                }
            };
        };

        TextWatcher checkLocationIsSet = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (eventStartingPoint.getText().toString().isEmpty() || eventEndingPoint.getText().toString().isEmpty()) {
                    if (eventStartingPoint.getText().toString().isEmpty()) {
                        eventStartingPoint.setError("Start location not set");
                    } else
                        eventEndingPoint.setError("End location not set");
                } else {
                    eventStartingPoint.setError(null);
                    eventEndingPoint.setError(null);
                    eventStartingPoint.setErrorColor(R.color.baseline);
                    eventEndingPoint.setErrorColor(R.color.baseline);
                    validLocation = true;
                }
            }
            ;
        };

        TextWatcher checkEventDate = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // Substring Reference
                // Log.v ("YEAR", eventEndDate.getText().toString().substring(0,4));
                // Log.v ("MONTH", eventEndDate.getText().toString().substring(5,7));
                // Log.v ("DAY", eventEndDate.getText().toString().substring(8,10));
                // Check Start Year > End Year (e.g. Start 2018, end 2017)
                if (Integer.valueOf(eventStartDate.getText().toString().substring(0,4)) >
                        Integer.valueOf(eventEndDate.getText().toString().substring(0,4))){
                    Log.v ("First","First");
                    eventEndDate.setError("End date is earlier than start date");
                    validDate = false;
                }
                // Check Same Year but Start Month > End Month (e.g. Start 2016-05, end 2016-04)
                else if (eventEndDate.getText().toString().substring(0,4).equals(eventStartDate.getText().toString().substring(0,4)) &&
                        (Integer.valueOf(eventStartDate.getText().toString().substring(5,7)) >
                        Integer.valueOf(eventEndDate.getText().toString().substring(5,7)))){
                    Log.v ("Second","Second");
                    eventEndDate.setError("End date is earlier than start date");
                    validDate = false;
                }
                // Check Same Year Same Month but Start Day > End Day (e.g. Start 2016-05-05, end 2016-05-03)
                else if (eventEndDate.getText().toString().substring(0,4).equals(eventStartDate.getText().toString().substring(0,4)) &&
                        (eventStartDate.getText().toString().substring(5,7).equals(eventEndDate.getText().toString().substring(5,7))) &&
                        Integer.valueOf(eventStartDate.getText().toString().substring(8,10)) >
                                Integer.valueOf(eventEndDate.getText().toString().substring(8,10))){
                    Log.v ("Third","Third");
                    eventEndDate.setError("End date is earlier than start date");
                    validDate = false;
                }
                else{
                    eventEndDate.setError(null);
                    validDate = true;
                }
            }
        };
        eventName.addTextChangedListener(checkEventName);
        eventDescription.addTextChangedListener(checkEventDescription);
        eventCapacity.addTextChangedListener(checkEventCapacity);
        eventStartTime.addTextChangedListener(checkEventTime);
        eventEndTime.addTextChangedListener(checkEventTime);
        eventStartingPoint.addTextChangedListener(checkLocationIsSet);
        eventEndingPoint.addTextChangedListener(checkLocationIsSet);
        eventStartDate.addTextChangedListener(checkEventDate);
        eventEndDate.addTextChangedListener(checkEventDate);
        eventStartDate.addTextChangedListener(checkEventTime);
        eventEndDate.addTextChangedListener(checkEventTime);
    }

    public void checkValidity() {
        if (!validTime || !validCapacity || !validDescription ||
                !validName || !validLocation || !validDate) {
            valid = false;
        } else
            valid = true;
    }

    public void submitEventForm() {
        checkValidity();
        Log.v("Time", String.valueOf(validTime));
        Log.v("Capacity", String.valueOf(validCapacity));
        Log.v("Description", String.valueOf(validDescription));
        Log.v("Name", String.valueOf(validName));
        Log.v("Location", String.valueOf(validLocation));
        if (valid) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            defValue = 0000;
            eventHostID = prefs.getInt("userID", defValue);
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            JSONParser jsonParser = new JSONParser();
            params.add(new BasicNameValuePair("user_id", String.valueOf(eventHostID)));
            params.add(new BasicNameValuePair("event_name", eventName.getText().toString()));
            if (eventType.getSelectedItem().toString() == "Running")
                eventTypeID = 1;
            else if (eventType.getSelectedItem().toString() == "Hiking")
                eventTypeID = 2;
            else if (eventType.getSelectedItem().toString() == "Cycling")
                eventTypeID = 3;
            params.add(new BasicNameValuePair("type_id", String.valueOf(eventTypeID)));
            params.add(new BasicNameValuePair("capacity", eventCapacity.getText().toString()));
            if (eventCredit.toString().isEmpty())
                eventCredit.setText("0");
            params.add(new BasicNameValuePair("credit", eventCredit.getText().toString()));
            params.add(new BasicNameValuePair("description", eventDescription.getText().toString()));
            params.add(new BasicNameValuePair("start_date", eventStartDate.getText().toString()));
            params.add(new BasicNameValuePair("end_date", eventEndDate.getText().toString()));
            params.add(new BasicNameValuePair("start_time", eventStartTime.getText().toString()));
            params.add(new BasicNameValuePair("end_time", eventEndTime.getText().toString()));
            params.add(new BasicNameValuePair("start_location", eventStartingPoint.getText().toString()));
            params.add(new BasicNameValuePair("end_location", eventEndingPoint.getText().toString()));
            params.add(new BasicNameValuePair("start_lat", String.valueOf(start_lat)));
            params.add(new BasicNameValuePair("start_long", String.valueOf(start_long)));
            params.add(new BasicNameValuePair("end_lat", String.valueOf(end_lat)));
            params.add(new BasicNameValuePair("end_long", String.valueOf(end_long)));
            if (eventTargetSpinner.getSelectedItem().toString().equals("Public Event (For everyone)")){
                privacy = "public";
            }
            else if (eventTargetSpinner.getSelectedItem().toString().equals("Private Event (For friends only)")){
                privacy = "private";
            }
            else if (eventTargetSpinner.getSelectedItem().toString().equals("Individual only")){
                privacy = "individual";
            }
            Log.v ("privacy", privacy);
            params.add(new BasicNameValuePair("privacy", privacy));
            json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/createEvent.php", "GET", params);
            AlertDialog.Builder SuccessfulCreateEventDialog = new AlertDialog.Builder(this);
            SuccessfulCreateEventDialog.setTitle("Event created successfully");
            SuccessfulCreateEventDialog.setMessage("Return to homepage in 3 seconds.");
            SuccessfulCreateEventDialog.setCancelable(true);
            SuccessfulCreateEventDialog.setNegativeButton("OK", null);
            SuccessfulCreateEventDialog.setIcon(R.drawable.create_event_success);
            AlertDialog SuccessfulCreateEventAlert = SuccessfulCreateEventDialog.create();
            SuccessfulCreateEventAlert.show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 3000);
        } else {
            if (eventName.getText().toString().isEmpty()) {
                eventName.setError("Event name cannot be empty");
            }
            if (eventDescription.getText().toString().isEmpty()) {
                eventDescription.setError("Description cannot be empty");
            }
            if (eventStartingPoint.getText().toString().isEmpty()) {
                eventStartingPoint.setError("Start location not set");
            }
            if (eventEndingPoint.getText().toString().isEmpty()) {
                eventEndingPoint.setError("End location not set");
            }
            if (eventCapacity.getText().toString().isEmpty()) {
                eventCapacity.setError("Maximum participants cannot be empty");
            }
            AlertDialog.Builder failedCreateEventDialog = new AlertDialog.Builder(this);
            failedCreateEventDialog.setTitle("Failed to create event");
            failedCreateEventDialog.setMessage("Please check for the error messages.");
            failedCreateEventDialog.setCancelable(true);
            failedCreateEventDialog.setNegativeButton("OK", null);
            failedCreateEventDialog.setIcon(R.drawable.create_event_fail);
            AlertDialog failedCreateEventAlert = failedCreateEventDialog.create();
            failedCreateEventAlert.show();
        }
    }


    public void onClickToStartingPoint(View v) {

        Fragment fragment = null;
        fragment = new Map_Fragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable("TAG", 0);
        fragment.setArguments(bundle);

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.create_view, fragment).addToBackStack("Frag1").commit();
        }
    }

    public void onClickToEndPoint(View v) {
        Fragment fragment = null;
        fragment = new Map_Fragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable("TAG", 1);
        fragment.setArguments(bundle);

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.create_view, fragment).addToBackStack("Frag1").commit();
        }
    }

    public void setStartLatLong(double a, double b) {
        this.start_lat = a;
        this.start_long = b;
    }

    public void setEndLatLong(double a, double b) {
        this.end_lat = a;
        this.end_long = b;
    }

    public void setStartPointLocation(String str) {
        eventStartingPoint.setText(str);
    }

    public void setEndPointLocation(String str) {
        eventEndingPoint.setText(str);
    }

    @Override
    protected Dialog onCreateDialog(int id) {

        if (id == 0) {
            startDatePickerDialog = new android.app.DatePickerDialog(this, new android.app.DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int day) {
                    mYear = year;
                    mMonth = month;
                    mDay = day;
                    startDateEditText.setText(setDateFormat(year, month, day));
                    endDateEditText.setText(setDateFormat(year, month, day));
                }

            }, mYear, mMonth, mDay);
            startDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            return startDatePickerDialog;
        } else if (id == 1) {
            startTimePickerDialog = new android.app.TimePickerDialog(this, new android.app.TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    if (hourOfDay < 10)
                        hourString = "0" + hourOfDay;
                    else if (hourOfDay > 12 && (Integer.valueOf(hourOfDay) - 12 >= 10))
                        hourString = String.valueOf(hourOfDay - 12);
                    else if (hourOfDay > 12 && (Integer.valueOf(hourOfDay) - 12 < 10))
                        hourString = "0" + String.valueOf(hourOfDay - 12);
                    else
                        hourString = String.valueOf(hourOfDay);
                    if (minute < 10)
                        minuteString = "0" + minute;
                    else
                        minuteString = String.valueOf(minute);

                    if (hourOfDay < 12)
                        ampmString = "AM";
                    else
                        ampmString = "PM";
                    mHour = hourOfDay;
                    mMinute = minute;
                    startTimeEditText.setText(hourString + ":" + minuteString + " " + ampmString);
                }
            }, mHour, mMinute, false);
            return startTimePickerDialog;
        } else if (id == 2) {
            endTimePickerDialog = new android.app.TimePickerDialog(this, new android.app.TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    if (hourOfDay < 10)
                        hourString = "0" + hourOfDay;
                    else if (hourOfDay > 12 && (Integer.valueOf(hourOfDay) - 12 >= 10))
                        hourString = String.valueOf(hourOfDay - 12);
                    else if (hourOfDay > 12 && (Integer.valueOf(hourOfDay) - 12 < 10))
                        hourString = "0" + String.valueOf(hourOfDay - 12);
                    else
                        hourString = String.valueOf(hourOfDay);
                    if (minute < 10)
                        minuteString = "0" + minute;
                    else
                        minuteString = String.valueOf(minute);

                    if (hourOfDay < 12)
                        ampmString = "AM";
                    else
                        ampmString = "PM";
                    mHour = hourOfDay;
                    mMinute = minute;
                    endTimeEditText.setText(hourString + ":" + minuteString + " " + ampmString);
                }
            }, mHour, mMinute, false);
            return endTimePickerDialog;
        } else if (id == 3) {
            endDatePickerDialog = new android.app.DatePickerDialog(this, new android.app.DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int day) {
                    mYear = year;
                    mMonth = month;
                    mDay = day;
                    endDateEditText.setText(setDateFormat(year, month, day));
                    Log.v("Date:::::::::::", setDateFormat(year, month, day));
                }

            }, mYear, mMonth, mDay);
            endDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            return endDatePickerDialog;
        }
        return null;
    }

    private String setDateFormat(int year, int monthOfYear, int dayOfMonth) {
        //e.g. 2015-08-08
        if (monthOfYear < 9 && dayOfMonth < 10){
            return String.valueOf(year) + "-0"
                    + String.valueOf(monthOfYear + 1) + "-0"
                    + String.valueOf(dayOfMonth);
        }
        //e.g. 2015-08-31
        else if (monthOfYear < 9 && dayOfMonth >= 10) {
            return String.valueOf(year) + "-0"
                    + String.valueOf(monthOfYear + 1) + "-"
                    + String.valueOf(dayOfMonth);
        }
        //e.g. 2015-10-05
        else if (monthOfYear > 8 && dayOfMonth < 10){
            return String.valueOf(year) + "-"
                    + String.valueOf(monthOfYear + 1) + "-0"
                    + String.valueOf(dayOfMonth);
        }
        //e.g. 2015-12-31
        else {
            return String.valueOf(year) + "-"
                    + String.valueOf(monthOfYear + 1) + "-"
                    + String.valueOf(dayOfMonth);
        }
    }

    private String setTimeFormat(int hour, int minute) {
        if (hour > 12)
            return String.valueOf(hour - 12) + ":" + String.valueOf(minute) + " PM";
        return String.valueOf(hour) + ":" + String.valueOf(minute) + " AM";
    }

    private String realTimeFormat(int hour, int minute) {
        return String.valueOf(hour) + ":" + String.valueOf(minute);
    }
    public void onBackPressed() {
        AlertDialog.Builder backDialog = new AlertDialog.Builder(this);
        backDialog.setTitle("Are you sure to go back?");
        backDialog.setMessage("If you choose to proceed, the data you input will be lost.");
        backDialog.setCancelable(true);
        backDialog.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });
        backDialog.setNegativeButton("Stay", null);
        backDialog.setIcon(R.drawable.create_event_fail);
        AlertDialog backAlert = backDialog.create();
        backAlert.show();
    }
}