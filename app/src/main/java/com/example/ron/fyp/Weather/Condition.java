package com.example.ron.fyp.Weather;

import org.json.JSONObject;

/**
 * Created by User on 20/3/2016.
 */
public class Condition implements JSONPopulator {
    private int code;
    private int temperature;
    private String description;


    public int getTemperature() {
        return temperature;
    }

    public String getDescription() {
        return description;
    }

    public int getCode() {
        return code;
    }



    @Override
    public void populate(JSONObject data) {
        code = data.optInt("code");
        temperature = data.optInt("temp");
        description = data.optString("text");

    }
}
