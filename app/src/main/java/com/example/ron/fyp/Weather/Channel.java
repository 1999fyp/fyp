package com.example.ron.fyp.Weather;

import org.json.JSONObject;

/**
 * Created by User on 20/3/2016.
 */
public class Channel implements JSONPopulator {
    private item myitem;
    private Units units;
    private Wind wind;
    private Myatmosphere atm;

    public Wind getWind() {
        return wind;
    }

    public Units getUnits() {
        return units;
    }

    public item getMyitem() {
        return myitem;
    }

    public Myatmosphere getAtm() {
        return atm;
    }

    @Override
    public void populate(JSONObject data) {
        units = new Units();
        units.populate(data.optJSONObject("units"));
        myitem = new item();
        myitem.populate(data.optJSONObject("item"));
        wind = new Wind();
        wind.populate(data.optJSONObject("wind"));
        atm = new Myatmosphere();
        atm.populate(data.optJSONObject("Atmosphere"));
    }
}
