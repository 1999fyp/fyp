package com.example.ron.fyp.Friend;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.example.ron.fyp.Database.JSONParser;
import com.example.ron.fyp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Friends_list extends AppCompatActivity {
    private ArrayList<String> user_id = new ArrayList<>();
    private ArrayList<String> full_name = new ArrayList<>();
    private ArrayList<String> icon = new ArrayList<>();
    private ArrayList<String> status = new ArrayList<>();
    private ArrayList<String> friend_id = new ArrayList<>();
    private ListView friend_listview1;
    private Toolbar toolbar;

    ScheduledFuture<?> scheduledFuture;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_list);
        friend_listview1 = (ListView) findViewById(R.id.friend_listview1);
        toolbar = (Toolbar) findViewById(R.id.friend_toolbar);
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setTitle("Friend");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scheduledFuture.cancel(true);
                finish();
            }
        });

        // Very 9 Start here
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        new Getfriend().execute();
                    }
                });
            }
        };
        scheduledFuture = scheduler.scheduleAtFixedRate(r, 1L, 1L, TimeUnit.SECONDS);
        // Very 9 End here

        //new Getfriend().execute();
    }

    class Getfriend extends AsyncTask<Void, Void, Void> {
        private SharedPreferences prefs = null;
        private JSONObject json = null;
        private int user_ID;

        @Override
        protected Void doInBackground(Void... mparams) {
            prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            user_ID = prefs.getInt("userID", user_ID);
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            JSONParser jsonParser = new JSONParser();
            params.add(new BasicNameValuePair("user_id", String.valueOf(user_ID)));
            params.add(new BasicNameValuePair("method", String.valueOf(3)));
            params.add(new BasicNameValuePair("friend_id", String.valueOf(0)));
            json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/friend.php", "GET", params);
            Log.v("zzzzzz", String.valueOf(json));
            try {
                JSONArray result = null;
                result = json.getJSONArray("results");
                user_id.clear();
                full_name.clear();
                icon.clear();
                status.clear();
                if (result != null)
                    for (int i = 0; i < result.length(); i++) {
                        JSONObject c = result.getJSONObject(i);
                        user_id.add(c.getString("user_id"));
                        full_name.add(c.getString("full_name"));
                        icon.add(c.getString("icon"));
                        status.add(c.getString("status"));
                    }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            friend_listview1.setAdapter( new Friend_adapter(user_id, full_name, icon, status,getApplicationContext()));
        }
    }

    @Override
    public void onBackPressed() {
        scheduledFuture.cancel(true);
        super.onBackPressed();
    }
}
