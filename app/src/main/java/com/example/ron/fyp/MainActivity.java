package com.example.ron.fyp;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.StrictMode;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.ron.fyp.SlidingTab.SlidingTabLayout;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    Toolbar toolbar;
    ViewPager pager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence Titles[] = {"Home", "Gallery", "User"};
    int NumberOfTabs = 3;

    SearchView searchView;
    MenuItem searchMenuItem;
    FrameLayout ll;

    Boolean is_searching = false;

    long lastPress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //For disabling SearchView when not in used
        //setupUI(this.findViewById(R.id.global_view));

        // Creating The Toolbar and setting it as the Toolbar for the activity
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter = new ViewPagerAdapter(getSupportFragmentManager(), Titles, NumberOfTabs, getApplicationContext());

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        // Assigning the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);

        //Set Default Tab Position
        pager.setCurrentItem(0);
        pager.setOffscreenPageLimit(4);

        //Set application locale to English
        Locale locale = new Locale("en");
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.search);
        searchView = (SearchView) searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);
        searchView.setIconifiedByDefault(false);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        searchView.setMaxWidth(width - (int) (width * 0.1));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                return true;
            case R.id.create:
                Intent myIntent = new Intent(this, CreateEvent.class);
                startActivity(myIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //For disabling SearchView when not in used
    public void hideSoftKeyboard(Activity activity) {
        //InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        try {
            searchView.clearFocus();
        } catch (Exception ex) {
            Log.v("Something", "Wrong");
        }
    }

    public void setupUI(View view) {

        if (!(view instanceof SearchView)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(MainActivity.this);
                    return false;
                }

            });
        }

        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public void setBottomToolbarState(boolean bool) {
        Toolbar tb = (Toolbar) findViewById(R.id.oh_my_toolbar);
        if (bool == true) {
            YoYo.with(Techniques.FadeInUp).duration(300).playOn(tb);
            tb.setVisibility(View.VISIBLE);
        } else {
            YoYo.with(Techniques.FadeInDown).duration(300).playOn(tb);
            tb.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;

        ll = (FrameLayout) findViewById(R.id.search_fragment);
        ll.setMinimumHeight(height);
        ll.setVisibility(View.VISIBLE);

        Fragment fragment = null;
        fragment = new SearchResult();

        Bundle bundle = new Bundle();
        bundle.putSerializable("TAG", query);
        fragment.setArguments(bundle);

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.search_fragment, fragment).addToBackStack("Frag1").commit();
        }

        is_searching = true;

        searchView.setFocusable(false);
        searchView.clearFocus();
        setBottomToolbarState(false);

        return false;
    }

    @Override
    public void onBackPressed() {
        Log.v("searchView.getQuery()", String.valueOf(searchView.getQuery()));
        if (!is_searching) {
            Log.v("searchView","if");
            super.onBackPressed();
        } else {
            Log.v("searchView","else");
            getFragmentManager().popBackStack();
            is_searching = false;
            searchView.setQuery("",false);
            searchView.setFocusable(false);
            searchView.clearFocus();
            ll.setVisibility(View.GONE);
            if (pager.getCurrentItem()==0)
                setBottomToolbarState(true);
        }
    }
}