package com.example.ron.fyp.GridView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.media.Image;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ron.fyp.CreateEvent;
import com.example.ron.fyp.Database.JSONParser;
import com.example.ron.fyp.R;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DetailsActivity extends AppCompatActivity {
    Toolbar toolbar;
    private static final int ANIM_DURATION = 600;
    private TextView descriptionTextView;
    private ImageView imageView;

    private int mLeftDelta;
    private int mTopDelta;
    private float mWidthScale;
    private float mHeightScale;

    private FrameLayout frameLayout;
    private ColorDrawable colorDrawable;

    private int thumbnailTop;
    private int thumbnailLeft;
    private int thumbnailWidth;
    private int thumbnailHeight;
    String img_id;
    int defValue, userID;
    String user_id;

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_RESULTS = "results";
    private static final String TAG_USERID = "user_id";
    private static final String TAG_USERNAME = "username";
    private static final String TAG_COMMENT = "comment";
    private static final String TAG_TIMEDIFF = "time_diff";
    ArrayList<HashMap<String, String>> commentAdapter;
    ListView commentList;

    JSONParser jParser = new JSONParser();
    JSONArray results = null;

    public void actionbar() {
        toolbar = (Toolbar) findViewById(R.id.image_detail_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Image Details");
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.image_detail_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.image_delete:
                if (user_id.equals(String.valueOf(userID)))
                    new AlertDialog.Builder(this)
                            .setTitle("Confirm delete?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Delete photo
                                    deletePhoto();
                                    Toast.makeText(getApplication(), "Deleted.", Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .show();
                else
                    Toast.makeText(getApplication(), "Not owner of photo.", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.image_report:
                new AlertDialog.Builder(this)
                        .setTitle("Confirm report?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Report photo
                                reportPhoto();
                                Toast.makeText(getApplication(), "Reported.", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Setting details screen layout
        setContentView(R.layout.activity_details_view);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        actionbar();

        // this function, should put after setContentView
        //one more question how to change to the name of this toolbar you mean add a title? yes now the title is FYP
        //ActionBar actionBar = getSupportActionBar();
        //actionBar.hide();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        defValue = 0000;
        userID = prefs.getInt("userID", defValue);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;
        int width = size.x;
        ImageView imageview = (ImageView) findViewById(R.id.grid_item_image);
        imageview.requestLayout();
        imageview.getLayoutParams().height = height;
        imageview.getLayoutParams().width = width;

        //retrieves the thumbnail data
        Bundle bundle = getIntent().getExtras();
        thumbnailTop = bundle.getInt("top");
        thumbnailLeft = bundle.getInt("left");
        thumbnailWidth = bundle.getInt("width");
        thumbnailHeight = bundle.getInt("height");

        String description = bundle.getString("description");
        String image = bundle.getString("image");
        img_id = bundle.getString("img_id");
        user_id = bundle.getString("user_id");
        Log.v("photo_user_id",user_id);
        Log.v("current user id", String.valueOf(userID));
        showListView();

        Button sendBtn = (Button) findViewById(R.id.comment_btn);
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertComment();
            }
        });

        //initialize and set the image description
        descriptionTextView = (TextView) findViewById(R.id.description);
        descriptionTextView.setText(Html.fromHtml(description));

        //Set image url
        imageView = (ImageView) findViewById(R.id.grid_item_image);
        Picasso.with(this).load(image).into(imageView);

        // Only run the animation if we're coming from the parent activity, not if
        // we're recreated automatically by the window manager (e.g., device rotation)
        if (savedInstanceState == null) {
            ViewTreeObserver observer = imageView.getViewTreeObserver();
            observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

                @Override
                public boolean onPreDraw() {
                    imageView.getViewTreeObserver().removeOnPreDrawListener(this);

                    // Figure out where the thumbnail and full size versions are, relative
                    // to the screen and each other
                    int[] screenLocation = new int[2];
                    imageView.getLocationOnScreen(screenLocation);
                    mLeftDelta = thumbnailLeft - screenLocation[0];
                    mTopDelta = thumbnailTop - screenLocation[1];

                    // Scale factors to make the large version the same size as the thumbnail
                    mWidthScale = (float) thumbnailWidth / imageView.getWidth();
                    mHeightScale = (float) thumbnailHeight / imageView.getHeight();

                    //enterAnimation();

                    return true;
                }
            });
        }
    }
    public void insertComment() {
        com.andreabaccega.widget.FormEditText comment = (com.andreabaccega.widget.FormEditText) findViewById(R.id.comment_context);
        if (!comment.getText().toString().isEmpty()) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            JSONParser jsonParser = new JSONParser();
            params.add(new BasicNameValuePair("user_id", String.valueOf(userID)));
            params.add(new BasicNameValuePair("img_id", img_id));
            params.add(new BasicNameValuePair("comment", comment.getText().toString()));
            JSONObject json;
            json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/comment_insert.php", "GET", params);
            showListView();
            Toast.makeText(getApplication(), "Your comment has been posted.", Toast.LENGTH_LONG).show();
            comment.setText("");
            comment.clearFocus();
        }
        else {
            comment.setHint("Please enter comment before pressing send.");
            comment.setHintTextColor(getResources().getColor(R.color.RealRed));
        }
    }

    public void showListView() {
        commentList = (ListView)findViewById(R.id.comment_list);
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("img_id", img_id));
        JSONObject json = jParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/comment_view.php", "GET", params);

        Log.v("Json object of comment", String.valueOf(json));

        try {
            int success = json.getInt(TAG_SUCCESS);
            Log.v("success", String.valueOf(success));
            if (success == 1) {
                results = json.getJSONArray(TAG_RESULTS);
                commentAdapter = new ArrayList<HashMap<String, String>>();

                // looping through All Products
                for (int i = 0; i < results.length(); i++) {
                    JSONObject c = results.getJSONObject(i);

                    // Storing each json item in variable
                    String user_id = c.getString(TAG_USERID);
                    String username = c.getString(TAG_USERNAME);
                    String comment = c.getString(TAG_COMMENT);
                    String time_difference = c.getString(TAG_TIMEDIFF);

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(TAG_USERID, user_id);
                    map.put(TAG_USERNAME, username);
                    map.put(TAG_COMMENT, comment);
                    map.put(TAG_TIMEDIFF, time_difference);
                    Log.v ("map",String.valueOf(map));
                    commentAdapter.add(map);
                    SimpleAdapter simpleAdapter = new SimpleAdapter(this, commentAdapter,
                            R.layout.comment_list_item, new String[] { TAG_USERNAME, TAG_COMMENT },
                            new int[] { R.id.comment_username_view, R.id.comment_context_view });
                    commentList.setAdapter(simpleAdapter);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    /**
     * The enter animation scales the picture in from its previous thumbnail
     * size/location.
     */
    public void enterAnimation() {

        // Set starting values for properties we're going to animate. These
        // values scale and position the full size version down to the thumbnail
        // size/location, from which we'll animate it back up
        imageView.setPivotX(0);
        imageView.setPivotY(0);
        imageView.setScaleX(mWidthScale);
        imageView.setScaleY(mHeightScale);
        imageView.setTranslationX(mLeftDelta);
        imageView.setTranslationY(mTopDelta);

        // interpolator where the rate of change starts out quickly and then decelerates.
        TimeInterpolator sDecelerator = new DecelerateInterpolator();

        // Animate scale and translation to go from thumbnail to full size
        imageView.animate().setDuration(ANIM_DURATION).scaleX(1).scaleY(1).
                translationX(0).translationY(0).setInterpolator(sDecelerator);

        // Fade in the black background
        ObjectAnimator bgAnim = ObjectAnimator.ofInt(colorDrawable, "alpha", 0, 255);
        bgAnim.setDuration(ANIM_DURATION);
        bgAnim.start();

    }

    /**
     * The exit animation is basically a reverse of the enter animation.
     * This Animate image back to thumbnail size/location as relieved from bundle.
     *
     * @param endAction This action gets run after the animation completes (this is
     *                  when we actually switch activities)
     */
    public void exitAnimation(final Runnable endAction) {

        TimeInterpolator sInterpolator = new AccelerateInterpolator();
        imageView.animate().setDuration(ANIM_DURATION).scaleX(mWidthScale).scaleY(mHeightScale).
                translationX(mLeftDelta).translationY(mTopDelta)
                .setInterpolator(sInterpolator).withEndAction(endAction);

        // Fade out background
        ObjectAnimator bgAnim = ObjectAnimator.ofInt(colorDrawable, "alpha", 0);
        bgAnim.setDuration(ANIM_DURATION);
        bgAnim.start();
    }

    @Override
    public void onBackPressed() {
        finish();
/*        exitAnimation(new Runnable() {
            public void run() {
                finish();
            }
        });*/
    }

    private void deletePhoto() {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        JSONParser jsonParser = new JSONParser();
        params.add(new BasicNameValuePair("img_id", img_id));
        JSONObject json;
        json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/delete_photo.php", "GET", params);
    }

    private void reportPhoto() {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        JSONParser jsonParser = new JSONParser();
        params.add(new BasicNameValuePair("user_id", user_id));
        params.add(new BasicNameValuePair("img_id", img_id));
        JSONObject json;
        json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/report.php", "GET", params);
    }

}
