package com.example.ron.fyp.Profile;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ron.fyp.Database.JSONParser;
import com.example.ron.fyp.R;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class profile_content_fragment1 extends Fragment {
    private PieChart profile_piechart;
    private View rootView;
    private int user_ID;
    JSONObject json;
    private SharedPreferences prefs = null;
    private TextView hiking_total,running_total,cycling_total;
    protected String[] mParties = new String[] {
            "Running","Hiking","Hiking"
    };
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.activity_profile_content_fragment1, container, false);
        getjson();
        setChart();
        return rootView;
    }

    private void setChart() {
        profile_piechart = (PieChart) rootView.findViewById(R.id.profile_piechart);
        profile_piechart.setUsePercentValues(true);
        profile_piechart.setDescription("");
        profile_piechart.setDrawCenterText(true);
        profile_piechart.setCenterText("Total 200 KM");
        profile_piechart.setExtraOffsets(5, 10, 5, 5);
        setData(7,8,9,22);
        profile_piechart.setDragDecelerationFrictionCoef(0.95f);
        profile_piechart.setDrawHoleEnabled(true);
        profile_piechart.setHoleColor(Color.WHITE);

        profile_piechart.setTransparentCircleColor(Color.WHITE);
        profile_piechart.setTransparentCircleAlpha(110);

        profile_piechart.setHoleRadius(58f);
        profile_piechart.setTransparentCircleRadius(61f);

        profile_piechart.setDrawCenterText(true);

        profile_piechart.setRotationAngle(0);
        // enable rotation of the chart by touch
        profile_piechart.setRotationEnabled(true);
        profile_piechart.setHighlightPerTapEnabled(true);
        profile_piechart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        Legend l = profile_piechart.getLegend();
        l.setEnabled(false);


    }
    private void setData(int run, int cycling, int hiking, int total) {


        ArrayList<Entry> yVals1 = new ArrayList<Entry>();

        // IMPORTANT: In a PieChart, no values (Entry) should have the same
        // xIndex (even if from different DataSets), since no values can be
        // drawn above each other.

        yVals1.add(new Entry((float) run, 0));
        yVals1.add(new Entry((float) cycling, 1));
        yVals1.add(new Entry((float) hiking, 2));



        ArrayList<String> xVals = new ArrayList<String>();

        for (int i = 0; i < 3; i++)
            xVals.add(mParties[i % mParties.length]);

        PieDataSet dataSet = new PieDataSet(yVals1, "Election Results");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);
        profile_piechart.setData(data);

        // undo all highlights
        profile_piechart.highlightValues(null);

        profile_piechart.invalidate();
    }

    public void getjson() {
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        user_ID = prefs.getInt("userID", user_ID);
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        JSONParser jsonParser = new JSONParser();
        prefs.getInt("userID", user_ID);
        params.add(new BasicNameValuePair("user_ID", String.valueOf(user_ID)));
        json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/profile.php", "GET", params);
       
    }
}
