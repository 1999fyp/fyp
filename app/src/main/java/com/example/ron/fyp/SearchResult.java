package com.example.ron.fyp;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ron.fyp.Database.JSONParser;
import com.example.ron.fyp.other_profile.Other_profile;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SearchResult extends Fragment {

    String query;

    JSONParser jParser = new JSONParser();
    ArrayList<HashMap<String, String>> searchList;
    ArrayList<HashMap<String, String>> searchList1;

    // products JSONArray
    JSONArray user_JSONArray = null;
    JSONArray event_JSONArray = null;

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_USER = "user";
    private static final String TAG_EVENT = "event";
    private static final String TAG_UID = "user_id";
    private static final String TAG_FULLNAME = "full_name";

    private String FEED_URL = "http://raymondchan1179.dlinkddns.com/fyp/search_user.php";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.search_list, container, false);

        //Retrieve TAG
        Bundle args = getArguments();
        query = (String) args.getSerializable("TAG");
        Log.v("query",query);

        ListView lv= (ListView)v.findViewById(R.id.listview);
        ListView lv2= (ListView)v.findViewById(R.id.listview2);
        TextView tv = (TextView)v.findViewById(R.id.seperate_user);
        TextView tv2 = (TextView)v.findViewById(R.id.seperate_event);

        searchList = new ArrayList<HashMap<String, String>>();
        searchList1 = new ArrayList<HashMap<String, String>>();

        final List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("query", query));
        JSONObject json = jParser.makeHttpRequest(FEED_URL, "GET", params);
        Log.i("JSON Response: ", json.toString());

        try {
            int success = json.getInt(TAG_SUCCESS);

            if (success == 1) {
                user_JSONArray = json.getJSONArray(TAG_USER);
                event_JSONArray = json.getJSONArray(TAG_EVENT);

                for (int i = 0; i < user_JSONArray.length(); i++) {
                    JSONObject c = user_JSONArray.getJSONObject(i);

                    // Storing each json item in variable
                    String uid = c.getString(TAG_UID);
                    String fullname = c.getString(TAG_FULLNAME);

                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    map.put(TAG_UID, uid);
                    map.put(TAG_FULLNAME, fullname);

                    // adding HashList to ArrayList
                    searchList.add(map);
                }

                for (int i = 0; i < event_JSONArray.length(); i++) {
                    JSONObject c = event_JSONArray.getJSONObject(i);

                    // Storing each json item in variable
                    String event_id = c.getString("event_id");
                    String event_name = c.getString("event_name");

                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    map.put(TAG_UID, event_id);
                    map.put(TAG_FULLNAME, event_name);

                    // adding HashList to ArrayList
                    searchList1.add(map);
                }

                if (user_JSONArray.length()>0)
                    tv.setVisibility(View.VISIBLE);
                if (event_JSONArray.length()>0)
                    tv2.setVisibility(View.VISIBLE);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        SimpleAdapter adapter = new SimpleAdapter(
                getActivity(), searchList,
                R.layout.search_list_item, new String[] { TAG_FULLNAME },
                new int[] { R.id.fullname_tv });
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // When clicked, show a toast with the TextView text
                HashMap<String,String> map =(HashMap<String,String>)parent.getItemAtPosition(position);
                String value = map.get(TAG_UID);

                Intent intent = new Intent(getActivity().getApplicationContext(), Other_profile.class);
                Bundle bundle = new Bundle();
                bundle.putString("user_id",value);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        SimpleAdapter adapter2 = new SimpleAdapter(
                getActivity(), searchList1,
                R.layout.search_list_item, new String[] { TAG_FULLNAME },
                new int[] { R.id.fullname_tv });
        lv2.setAdapter(adapter2);
        lv2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // When clicked, show a toast with the TextView text
                HashMap<String,String> map =(HashMap<String,String>)parent.getItemAtPosition(position);
                String value = map.get(TAG_UID);

                Intent intent = new Intent(getActivity(), Event_Detail.class);
                Bundle bundle = new Bundle();
                bundle.putString("eventID", value);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        return v;
    }
}