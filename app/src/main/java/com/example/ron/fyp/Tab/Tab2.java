package com.example.ron.fyp.Tab;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.ron.fyp.Database.JSONParser;
import com.example.ron.fyp.DividerItemDecoration;
import com.example.ron.fyp.Event_Detail;
import com.example.ron.fyp.R;
import com.example.ron.fyp.RecyclerView_adapter;
import com.example.ron.fyp.Tab2_recyclerview;
import com.kennyc.bottomsheet.BottomSheet;
import com.kennyc.bottomsheet.BottomSheetListener;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.ptr.PtrClassicFrameLayout;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

public class Tab2 extends Fragment implements View.OnClickListener, BottomSheetListener {

    TextView one, two, three;
    String privacy, type_name, sorting;
    private SharedPreferences prefs = null;
    private int user_ID, defValue;

    JSONParser jParser = new JSONParser();
    private String url = "http://raymondchan1179.dlinkddns.com/fyp/sorting.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_RESULTS = "results";

    private static final String TAG_TYPEID = "type_id";
    private static final String TAG_EVENTID = "event_id";
    private static final String TAG_EVENTNAME = "event_name";

    private static final String TAG_STARTTIME = "start_time";
    private static final String TAG_STARTDATE = "start_date";
    private static final String TAG_STARTLOCATION = "start_location";

    private static final String TAG_CAPACITY = "capacity";
    private static final String TAG_JOINED = "joined";

    private static final String TAG_FULLNAME = "full_name";
    private static final String TAG_IMG = "IMG";

    // products JSONArray
    JSONArray results = null;

    private PtrFrameLayout mPtrFrame;

    FragmentActivity mActivity;
    Tab2_recyclerview tab2_adapter;
    RecyclerView mRecyclerView;
    ArrayList<String> myListEventID = new ArrayList<String>();
    ArrayList<String> myListEventname = new ArrayList<String>();
    ArrayList<String> myListHost = new ArrayList<String>();
    ArrayList<String> myListSTARTDATE = new ArrayList<String>();
    ArrayList<String> myListSTARTTIME = new ArrayList<String>();
    ArrayList<String> myListSTARTLOCATION = new ArrayList<String>();
    ArrayList<String> myListJoined = new ArrayList<String>();
    ArrayList<String> myListCAPACITY = new ArrayList<String>();
    ArrayList<Integer> myListTYPE= new ArrayList<Integer>();
    ImageView joinBtn;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.tab_2, container, false);

        one = (TextView) getActivity().findViewById(R.id.title1);
        two = (TextView) getActivity().findViewById(R.id.title2);
        three = (TextView) getActivity().findViewById(R.id.title3);
        getActivity().findViewById(R.id.title1).setOnClickListener(this);
        getActivity().findViewById(R.id.title2).setOnClickListener(this);
        getActivity().findViewById(R.id.title3).setOnClickListener(this);

        mRecyclerView = (RecyclerView) v.findViewById(R.id.tab2_recycler);

        privacy = "Public";
        type_name = "All";
        sorting = "Latest";

        //Pull to refresh
        mPtrFrame = (PtrClassicFrameLayout) v.findViewById(R.id.tab2_ptr_frame);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                frame.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showListview();
                        mPtrFrame.refreshComplete();
                    }
                }, 1800);
            }

            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
            }
        });
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title1:
                new BottomSheet.Builder(getContext())
                        .setSheet(R.menu.visibility)
                        .setListener(this)
                        .show();
                break;
            case R.id.title2:
                new BottomSheet.Builder(getContext())
                        .setSheet(R.menu.grid_sports)
                        .grid()
                        .setTitle("Sports")
                        .setListener(this)
                        .show();
                break;
            case R.id.title3:
                new BottomSheet.Builder(getContext())
                        .setSheet(R.menu.sort)
                        .setListener(this)
                        .show();
                break;
        }
    }
    @Override
    public void onSheetShown() {
        Log.v("Sheet", "onSheetShown");
    }
    @Override
    public void onSheetItemSelected(MenuItem item) {

        Toast.makeText(getActivity().getApplicationContext(), item.getTitle() + " Clicked", Toast.LENGTH_SHORT).show();

        String temp_title;
        temp_title = String.valueOf(item.getTitle());

        if ((temp_title.equals("Public")) || (temp_title.equals("Private")) || (temp_title.equals("Individual"))) {
            privacy = temp_title;
            one.setText(temp_title);
        }
        else if ((temp_title.equals("Running")) || (temp_title.equals("Cycling")) || (temp_title.equals("Hiking")) || (temp_title.equals("All"))) {
            type_name = temp_title;
            two.setText(temp_title);
        }
        else if ((temp_title.equals("Latest")) || (temp_title.equals("Hottest")) || (temp_title.equals("Favourite"))) {
            sorting = temp_title;
            three.setText(temp_title);
        }
        showListview();
    }

    @Override
    public void onSheetDismissed(int which) {
        Log.v("TAG", "onSheetDismissed " + which);
        switch (which) {
            case BottomSheet.BUTTON_POSITIVE:
                Toast.makeText(getActivity().getApplicationContext(), "Positive Button Clicked", Toast.LENGTH_SHORT).show();
                break;
            case BottomSheet.BUTTON_NEGATIVE:
                Toast.makeText(getActivity().getApplicationContext(), "Negative Button Clicked", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void showListview() {
        Log.v("Making Connection", "......");
        prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        defValue = 0000;
        user_ID = prefs.getInt("userID", defValue);

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("privacy", privacy));
        params.add(new BasicNameValuePair("type_name", type_name));
        params.add(new BasicNameValuePair("sorting", sorting));
        params.add(new BasicNameValuePair("user_ID", String.valueOf(user_ID)));
        JSONObject json = jParser.makeHttpRequest(url, "GET", params);

        Log.v("JSON", String.valueOf(json));

        try {
            int success = json.getInt(TAG_SUCCESS);
            if (success == 1) {
                results = json.getJSONArray(TAG_RESULTS);

                myListEventID.clear();
                myListEventname.clear();
                myListHost.clear();
                myListSTARTDATE.clear();
                myListSTARTTIME.clear();
                myListSTARTLOCATION.clear();
                myListJoined.clear();
                myListCAPACITY.clear();
                myListTYPE.clear();

                // looping through All Products
                for (int i = 0; i < results.length(); i++) {
                    JSONObject c = results.getJSONObject(i);
                    // Storing each json item in variable
                    String event_id = c.getString(TAG_EVENTID);
                    String event_name = c.getString(TAG_EVENTNAME);
                    String host_name = c.getString(TAG_FULLNAME);
                    String start_date = c.getString(TAG_STARTDATE);
                    String start_time = c.getString(TAG_STARTTIME);
                    String start_location = c.getString(TAG_STARTLOCATION);
                    //String joined = c.getString(TAG_JOINED);
                    Integer joined = Integer.valueOf(c.getString(TAG_JOINED));
                    String capacity = c.getString(TAG_CAPACITY);
                    Integer type_id = Integer.valueOf(c.getString(TAG_TYPEID));

                    myListEventID.add(event_id);
                    myListEventname.add(event_name);
                    myListHost.add(host_name);
                    myListSTARTDATE.add(start_date);
                    myListSTARTTIME.add(start_time);
                    myListSTARTLOCATION.add(start_location);
                    myListJoined.add(String.valueOf(joined));
                    myListCAPACITY.add(capacity);
                    myListTYPE.add(type_id);
                }
            }
            else
            {
                // Clear list if no result is found
                myListEventID.clear();
                myListEventname.clear();
                myListHost.clear();
                myListSTARTDATE.clear();
                myListSTARTTIME.clear();
                myListSTARTLOCATION.clear();
                myListJoined.clear();
                myListCAPACITY.clear();
                myListTYPE.clear();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        joinBtn = (ImageView) getActivity().findViewById(R.id.joinBtn);
        tab2_adapter = new Tab2_recyclerview(mActivity, myListEventname, myListHost, myListSTARTDATE, myListSTARTTIME,
                myListSTARTLOCATION, myListJoined, myListCAPACITY, myListTYPE,myListEventID,joinBtn);
        mRecyclerView.setAdapter(tab2_adapter);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        tab2_adapter.SetOnItemClickListener(new Tab2_recyclerview.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {

                String eventID = tab2_adapter.geteventid(position);

                Intent intent = new Intent(getActivity(), Event_Detail.class);
                Bundle bundle = new Bundle();
                bundle.putString("eventID", eventID);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onResume() {
        showListview();
        super.onResume();
    }
}