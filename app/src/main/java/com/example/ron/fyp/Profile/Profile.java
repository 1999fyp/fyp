package com.example.ron.fyp.Profile;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.LoginFilter;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.ecloud.pulltozoomview.PullToZoomScrollViewEx;
import com.example.ron.fyp.Database.JSONParser;
import com.example.ron.fyp.R;

import com.example.ron.fyp.RecyclerView_adapter;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.blurry.Blurry;

public class Profile extends AppCompatActivity implements RecyclerViewExpandableItemManager.OnGroupCollapseListener,
        RecyclerViewExpandableItemManager.OnGroupExpandListener {

    private PullToZoomScrollViewEx scrollView;
    private View headView, zoomView, contentView;
    private ImageView profile_userprofileimg;
    private TextView profile_access_level, profile_username;
    private ProgressBar profile_ProgressBar;
    private CircleImageView profile_icon;
    private Toolbar profile_toolbar, profile_toolbar_onscroll;
    private JSONObject json;
    private int user_ID;
    private SharedPreferences prefs = null;
    private RecyclerView Profile_information_recycler;
    private boolean position5 = false;
    private boolean position6 = false;
    private String facebook_id, username, password,
            access_level, credits, DOB, email, gender,
            icon, full_name,
            totalkm_running, totalkm_hiking, totalkm_cycling;
    private Parcelable eimSavedState;
    private static final String SAVED_STATE_EXPANDABLE_ITEM_MANAGER = "RecyclerViewExpandableItemManager";
    private RecyclerViewExpandableItemManager mRecyclerViewExpandableItemManager;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getjson();
        eimSavedState = (savedInstanceState != null) ? savedInstanceState.getParcelable(SAVED_STATE_EXPANDABLE_ITEM_MANAGER) : null;
        setScrollView();
    }


    private void setImage() {
        new ImageLoadTask("http://raymondchan1179.dlinkddns.com/fyp/" + icon, profile_userprofileimg).execute();
        new ImageLoadTask("http://raymondchan1179.dlinkddns.com/fyp/" + icon, profile_icon).execute();
    }


    private void getjson() {
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        user_ID = prefs.getInt("userID", user_ID);
        facebook_id = prefs.getString("facebook_id", facebook_id);
        username = prefs.getString("username", username);
        password = prefs.getString("password", password);
        access_level = prefs.getString("access_level", access_level);
        credits = prefs.getString("credits", credits);
        DOB = prefs.getString("DOB", DOB);
        email = prefs.getString("email", email);
        gender = prefs.getString("gender", gender);
        icon = prefs.getString("icon", icon);
        full_name = prefs.getString("full_name", full_name);
        /**     weight = prefs.getString("weight", weight);
         height = prefs.getString("height", height);**/
        totalkm_running = prefs.getString("totalkm_running", totalkm_running);
        totalkm_hiking = prefs.getString("totalkm_hiking", totalkm_hiking);
        totalkm_cycling = prefs.getString("totalkm_cycling", totalkm_cycling);
    }

    private void setScrollView() {
        scrollView = (PullToZoomScrollViewEx) findViewById(R.id.scroll_view);
        headView = LayoutInflater.from(this).inflate(R.layout.profile_headview, null, false);
        zoomView = LayoutInflater.from(this).inflate(R.layout.profile_zoom, null, false);
        contentView = LayoutInflater.from(this).inflate(R.layout.profile_content, null, false);
        scrollView.setHeaderView(headView);
        scrollView.setZoomView(zoomView);
        scrollView.setScrollContentView(contentView);
        profile_ProgressBar = (ProgressBar) headView.findViewById(R.id.profile_ProgressBar);
        profile_toolbar = (Toolbar) headView.findViewById(R.id.profile_toolbar);
        profile_toolbar_onscroll = (Toolbar) findViewById(R.id.profile_toolbar_onscroll);
        Profile_information_recycler = (RecyclerView) contentView.findViewById(R.id.profile_information_recycler);


        profile_access_level = (TextView) headView.findViewById(R.id.profile_access_level);
        profile_username = (TextView) headView.findViewById(R.id.personal_information);
        profile_icon = (CircleImageView) headView.findViewById(R.id.profile_icon);
        profile_userprofileimg = (ImageView) zoomView.findViewById(R.id.profile_userimg_background);
        DisplayMetrics localDisplayMetrics = new DisplayMetrics();
        scrollView.setParallax(true);

        profile_toolbar.post(new Runnable() {
            @Override
            public void run() {
                setSupportActionBar(profile_toolbar);
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            }
        });
        getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
        int mScreenHeight = localDisplayMetrics.heightPixels;
        int mScreenWidth = localDisplayMetrics.widthPixels;
        LinearLayout.LayoutParams localObject = new LinearLayout.LayoutParams(mScreenWidth, (int) (10F * (mScreenWidth / 16.0F)));
        scrollView.setHeaderLayoutParams(localObject);
        setRecycler();
        setImage();
        setToolbar();
        setOnScroll();
        setBlur();
        sethead();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit_info:
                new AlertDialog.Builder(this).setTitle("Edit Information").setMessage("Coming Soon").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
                break;
            default:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setRecycler() {
        RecyclerViewExpandableItemManager expMgr = new RecyclerViewExpandableItemManager(null);

        Profile_information_recycler.setHasFixedSize(true);
        Profile_information_recycler.setLayoutManager(new LinearLayoutManager(this));
        Profile_information_recycler.setItemAnimator(new DefaultItemAnimator());
        //  Profile_information_recycler.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL_LIST));
        mRecyclerViewExpandableItemManager = new RecyclerViewExpandableItemManager(eimSavedState);
        mRecyclerViewExpandableItemManager.setOnGroupExpandListener(this);
        mRecyclerViewExpandableItemManager.setOnGroupCollapseListener(this);
        MyAdapter myAdapter = new MyAdapter(getApplicationContext(), mRecyclerViewExpandableItemManager);
        myAdapter.SetOnItemClickListener(new MyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                if (position == 4) {
                    Log.d("4", Profile_information_recycler.getHeight() + "");
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) Profile_information_recycler.getLayoutParams();
                    if (!position5) {
                        params.height = Profile_information_recycler.getHeight() + 1300;
                        position5 = !position5;
                    } else {
                        params.height = Profile_information_recycler.getHeight() - 1300;
                        position5 = !position5;
                    }

                    Profile_information_recycler.setLayoutParams(params);

                } else {
                    if (position == 5) {
                        Log.d("5", Profile_information_recycler.getHeight() + "");
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) Profile_information_recycler.getLayoutParams();
                        if (!position6) {
                            params.height = Profile_information_recycler.getHeight() + 1300;
                            position6 = !position6;
                        } else {
                            params.height = Profile_information_recycler.getHeight() - 1300;
                            position6 = !position6;
                        }
                        Profile_information_recycler.setLayoutParams(params);

                    }
                }

            }
        });
        Profile_information_recycler.setAdapter(expMgr.createWrappedAdapter(myAdapter));
        Profile_information_recycler.setNestedScrollingEnabled(false);


        expMgr.attachRecyclerView(Profile_information_recycler);

    }

    private void setOnScroll() {

        setSupportActionBar(profile_toolbar_onscroll);
        scrollView.getPullRootView().getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if (scrollView.getPullRootView().getScrollY() >= 400 && profile_toolbar_onscroll.getVisibility() != View.VISIBLE) {
                    YoYo.with(Techniques.FadeInUp).duration(300).playOn(profile_toolbar_onscroll);
                    profile_toolbar_onscroll.setVisibility(View.VISIBLE);
                    setSupportActionBar(profile_toolbar_onscroll);
                    getSupportActionBar().setDisplayShowTitleEnabled(false);
                    Log.d("show", "up");
                }

                if (scrollView.getPullRootView().getScrollY() < 400 && profile_toolbar_onscroll.getVisibility() == View.VISIBLE) {
                    YoYo.with(Techniques.FadeInDown).duration(300).playOn(profile_toolbar_onscroll);
                    profile_toolbar_onscroll.setVisibility(View.INVISIBLE);
                    Log.d("show", "down");
                }
            }
        });
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.getPullRootView().fullScroll(ScrollView.FOCUS_UP);
            }
        });
    }

    private void sethead() {
        profile_username.setText(full_name);
        profile_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Image_detail.class);
                startActivity(intent);
            }
        });
        //  profile_access_level.setText(access_level);
    }

    private void setToolbar() {
        profile_toolbar.setNavigationIcon(R.drawable.back);
        profile_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        profile_toolbar_onscroll.setNavigationIcon(R.drawable.back);
        profile_toolbar_onscroll.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        setSupportActionBar(profile_toolbar);

    }

    private void setBlur() {
        profile_userprofileimg.postDelayed(new Runnable() {
            @Override
            public void run() {
                Blurry.with(getApplicationContext())
                        .radius(10)
                        .sampling(8)
                        .async()
                        .capture(profile_userprofileimg)
                        .into(profile_userprofileimg);
                profile_ProgressBar.setVisibility(View.GONE);
                profile_userprofileimg.setVisibility(View.VISIBLE);
                profile_icon.setVisibility(View.VISIBLE);
                //   profile_access_level.setVisibility(View.VISIBLE);
                profile_username.setVisibility(View.VISIBLE);

            }
        }, 1000);
    }

    @Override
    public void onGroupCollapse(int groupPosition, boolean fromUser) {

    }

    @Override
    public void onGroupExpand(int groupPosition, boolean fromUser) {

    }

    //  Set Chart Data


    /**
     * test area
     **/
    static abstract class MyBaseItem {
        public final long id;
        public final String text;

        public MyBaseItem(long id, String text) {
            this.id = id;
            this.text = text;
        }
    }

    static class MyGroupItem extends MyBaseItem {
        public final List<MyChildItem> children;

        public MyGroupItem(long id, String text) {
            super(id, text);
            children = new ArrayList<>();
        }
    }

    static class MyChildItem extends MyBaseItem {
        public MyChildItem(long id, String text) {
            super(id, text);
        }
    }

    static abstract class MyBaseViewHolder extends AbstractExpandableItemViewHolder {
        View v;
        ImageView profile_information_icon, profile_next, height_icon, favorite_IMG;
        TextView profile_title, personal_information, height_content, height_title;
        RelativeLayout profile_list_layout;

        public MyBaseViewHolder(View itemView) {
            super(itemView);
            profile_list_layout = (RelativeLayout) itemView.findViewById(R.id.profile_list_layout);
            profile_information_icon = (ImageView) itemView.findViewById(R.id.profile_information_icon);
            profile_title = (TextView) itemView.findViewById(R.id.profile_title);
            personal_information = (TextView) itemView.findViewById(R.id.personal_information);
            profile_next = (ImageView) itemView.findViewById(R.id.profile_next);
            height_icon = (ImageView) itemView.findViewById(R.id.height_icon);
            height_content = (TextView) itemView.findViewById(R.id.height_content);
            height_title = (TextView) itemView.findViewById(R.id.height_title);
            favorite_IMG = (ImageView) itemView.findViewById(R.id.favorite_IMG);
            this.v = itemView;

        }
    }

    static class MyGroupViewHolder extends MyBaseViewHolder {
        public MyGroupViewHolder(View itemView) {
            super(itemView);
        }
    }


    static class MyChildViewHolder extends MyBaseViewHolder {
        public MyChildViewHolder(View itemView) {
            super(itemView);
        }
    }

    /**
     * test area2
     **/
    static class MyAdapter extends AbstractExpandableItemAdapter<MyGroupViewHolder, MyChildViewHolder> implements View.OnClickListener, View.OnLongClickListener {
        List<MyGroupItem> mItems;
        int groupPosition;
        private LineChart chart;
        String Full_name, Gender, DOB, Email, Weight, Height, Credits, totalkm_running, totalkm_hiking, totalkm_cycling;
        private int totalkm_running_num, totalkm_hiking_num, totalkm_cycling_num;
        private OnItemClickListener mItemClickListener;
        private Context mcontext;
        private PieChart mpiechart;
        private boolean expand;
        private ViewGroup parent;
        private RecyclerViewExpandableItemManager mExpandableItemManager;
        private RelativeLayout f1_relativeLayout;
        private TextView height_title, height_content, totalkm_running_TXT, totalkm_hiking_TXT, totalkm_cycling_TXT;
        private ImageView height_icon, favorite_IMG;
        private ProgressBar profile_progress;
        private ArrayList<String> date = new ArrayList<String>();
        private ArrayList<String> health_height = new ArrayList<String>();
        private ArrayList<String> health_weight = new ArrayList<String>();
        protected String[] mParties = new String[]{
                "Running", "Cycling", "Hiking"
        };

        public MyAdapter(Context context, RecyclerViewExpandableItemManager expandableItemManager) {
            Log.d("teststep", "Myadapter");
            setHasStableIds(true); // this is required for expandable feature.
            this.mcontext = context;
            mItems = new ArrayList<>();
            for (int i = 0; i < 20; i++) {
                MyGroupItem group = new MyGroupItem(i, "GROUP " + i);
                for (int j = 0; j < 5; j++) {
                    group.children.add(new MyChildItem(i, "child " + i));
                }
                this.mExpandableItemManager = expandableItemManager;
                mItems.add(group);
            }
        }

        @Override
        public int getGroupCount() {
            Log.d("teststep", "getGroupCount");
            return 7;
        }

        @Override
        public int getChildCount(int groupPosition) {
            Log.d("teststep", "getChildCount");
            return 1;
        }

        @Override
        public long getGroupId(int groupPosition) {
            Log.d("teststep", "getGroupId");
            // This method need to return unique value within all group items.
            return mItems.get(groupPosition).id;

        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            Log.d("teststep", "getChildId");
            // This method need to return unique value within the group.
            return mItems.get(groupPosition).children.get(childPosition).id;

        }

        @Override
        public MyGroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
            Log.d("teststep", "onCreateGroupViewHolder");

            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.personal_information_list, parent, false);
            return new MyGroupViewHolder(v);
        }

        @Override
        public MyChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
            Log.d("teststep", "onCreateChildViewHolder");
            new GetHeathTask().execute();
            this.parent = parent;
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_profile_content_fragment1, parent, false);
            mpiechart = (PieChart) v.findViewById(R.id.profile_piechart);
            f1_relativeLayout = (RelativeLayout) v.findViewById(R.id.f1_blur);
            totalkm_running_TXT = (TextView) v.findViewById(R.id.running_total);
            totalkm_cycling_TXT = (TextView) v.findViewById(R.id.cycling_total);
            totalkm_hiking_TXT = (TextView) v.findViewById(R.id.hiking_total);
            profile_progress = (ProgressBar) v.findViewById(R.id.profile_progress);
            favorite_IMG = (ImageView)v.findViewById(R.id.favorite_IMG);
            chart = (LineChart) v.findViewById(R.id.linechart);
            return new MyChildViewHolder(v);
        }

        @Override
        public void onBindGroupViewHolder(MyGroupViewHolder holder, int groupPosition, int viewType) {
            Log.d("teststep", "onBindGroupViewHolder");
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mcontext);
            holder.v.setOnClickListener(this);
            holder.v.setOnLongClickListener(this);
            this.height_title = holder.height_title;
            this.height_content = holder.height_content;
            this.height_icon = holder.height_icon;
            this.favorite_IMG = holder.favorite_IMG;

            if (!expand) {
                holder.profile_list_layout.setBackgroundColor(Color.parseColor("#00ABFF"));
                holder.profile_next.setImageResource(R.drawable.down);
            } else {
                holder.profile_list_layout.setBackgroundColor(Color.parseColor("#A1E0FF"));
                holder.profile_next.setImageResource(R.drawable.up);
            }
            switch (groupPosition) {
                case 0:
                    holder.profile_information_icon.setImageResource(R.drawable.username);
                    holder.profile_next.setVisibility(View.INVISIBLE);
                    holder.profile_title.setText("Full Name");
                    holder.personal_information.setText(prefs.getString("full_name", Full_name));
                    break;
                case 1:
                    holder.profile_information_icon.setImageResource(R.drawable.gender);
                    holder.profile_next.setVisibility(View.INVISIBLE);
                    holder.profile_title.setText("Gender");
                    holder.personal_information.setText(prefs.getString("gender", Gender));
                    break;
                case 2:
                    holder.profile_information_icon.setImageResource(R.drawable.dob);
                    holder.profile_next.setVisibility(View.INVISIBLE);
                    holder.profile_title.setText("Date of Birth");
                    holder.personal_information.setText(prefs.getString("DOB", DOB));
                    break;
                case 3:
                    holder.profile_information_icon.setImageResource(R.drawable.email);
                    holder.profile_next.setVisibility(View.INVISIBLE);
                    holder.profile_title.setText("Email");
                    holder.personal_information.setText(prefs.getString("email", Email));
                    break;
                case 4:
                    holder.profile_information_icon.setImageResource(R.drawable.weight);
                    holder.profile_next.setVisibility(View.VISIBLE);
                    holder.profile_title.setText("Weight");
                    holder.height_content.setVisibility(View.VISIBLE);
                    holder.height_title.setVisibility(View.VISIBLE);
                    holder.height_icon.setVisibility(View.VISIBLE);
                    holder.personal_information.setText(prefs.getString("weight", Weight));
                    holder.height_content.setText(prefs.getString("height", Height));
                    break;
                case 5:
                    holder.profile_information_icon.setImageResource(R.drawable.all_shit);
                    holder.profile_next.setVisibility(View.VISIBLE);
                    holder.height_content.setVisibility(View.INVISIBLE);
                    holder.height_title.setVisibility(View.INVISIBLE);
                    holder.height_icon.setVisibility(View.INVISIBLE);

                    holder.profile_title.setText("Total exercise time");
                    if (prefs.getString("totalkm_running", totalkm_running) == null)
                        totalkm_running_num = 0;
                    else
                        totalkm_running_num = Integer.parseInt(prefs.getString("totalkm_running", totalkm_running));
                    if (prefs.getString("totalkm_hiking", totalkm_hiking) == null)
                        totalkm_hiking_num = 0;
                    else
                        totalkm_hiking_num = Integer.parseInt(prefs.getString("totalkm_hiking", totalkm_hiking));
                    if (prefs.getString("totalkm_cycling", totalkm_cycling) == null)
                        totalkm_cycling_num = 0;
                    else
                        totalkm_cycling_num = Integer.parseInt(prefs.getString("totalkm_cycling", totalkm_cycling));

                    holder.personal_information.setText(totalkm_running_num + totalkm_hiking_num + totalkm_cycling_num + "");


                    break;
                case 6:
                    holder.profile_information_icon.setImageResource(R.drawable.event_detail_credit);
                    holder.profile_next.setVisibility(View.INVISIBLE);
                    holder.profile_title.setText("Credits");
                    holder.personal_information.setText(prefs.getString("credits", Credits));
                    break;

            }


        }

        @Override
        public void onBindChildViewHolder(MyChildViewHolder holder, int groupPosition, int childPosition, int viewType) {
            Log.d("teststep", "onBindChildViewHolder");

            if (groupPosition == 4) {
                chart.setVisibility(View.VISIBLE);
                f1_relativeLayout.setVisibility(View.INVISIBLE);
                profile_progress.setVisibility(View.INVISIBLE);
            } else {
                height_content.setVisibility(View.INVISIBLE);
                height_icon.setVisibility(View.INVISIBLE);
                height_title.setVisibility(View.INVISIBLE);

                if (groupPosition == 5) {
                    totalkm_running_TXT.setText(totalkm_running_num + " km");
                    totalkm_hiking_TXT.setText(totalkm_hiking_num + " km");
                    totalkm_cycling_TXT.setText(totalkm_cycling_num + " km");
                    chart.setVisibility(View.INVISIBLE);
                    f1_relativeLayout.setVisibility(View.VISIBLE);
                    profile_progress.setVisibility(View.INVISIBLE);
                }
            }


        }

        class GetHeathTask extends AsyncTask<Void, Void, Void> {
            private SharedPreferences prefs = null;
            private JSONObject json = null;
            private int user_ID;

            public GetHeathTask() {
            }

            @Override
            protected Void doInBackground(Void... mparams) {
                prefs = PreferenceManager.getDefaultSharedPreferences(mcontext);
                user_ID = prefs.getInt("userID", user_ID);
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                JSONParser jsonParser = new JSONParser();
                prefs.getInt("userID", user_ID);
                params.add(new BasicNameValuePair("user_id", String.valueOf(user_ID)));
                json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/gethealth.php", "GET", params);
                try {
                    JSONArray result = null;
                    result = json.getJSONArray("results");
                    if (result != null)
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject c = result.getJSONObject(i);
                            date.add(c.getString("update_date"));
                            health_height.add(c.getString("height"));
                            health_weight.add(c.getString("weight"));
                        }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onProgressUpdate(Void... values) {
                super.onProgressUpdate(values);
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                setLineChart();
                setPieChart();
            }
        }

        private void setPieData(int run, int cycling, int hiking, int total) {


            ArrayList<Entry> yVals1 = new ArrayList<Entry>();

            // IMPORTANT: In a PieChart, no values (Entry) should have the same
            // xIndex (even if from different DataSets), since no values can be
            // drawn above each other.
            if (run != 0)
                yVals1.add(new Entry((float) run, 0));
            if (cycling != 0)
                yVals1.add(new Entry((float) cycling, 1));
            if (hiking != 0)
                yVals1.add(new Entry((float) hiking, 2));
            switch (comparesport(run, cycling, hiking)) {
                case "run":
                    favorite_IMG.setImageResource(R.drawable.running);
                    favorite_IMG.setColorFilter(Color.parseColor("#FF3636"));
                    break;
                case "hiking":
                    favorite_IMG.setImageResource(R.drawable.hiking);
                    favorite_IMG.setColorFilter(Color.parseColor("#50B7FF"));
                    break;
                case "cycling":
                    favorite_IMG.setImageResource(R.drawable.cycling);
                    favorite_IMG.setColorFilter(Color.parseColor("#FF2DD2"));
                    break;
                default:
                    break;
            }
            ArrayList<String> xVals = new ArrayList<String>();

            for (int i = 0; i < 3; i++)
                xVals.add(mParties[i % mParties.length]);
            PieDataSet dataSet = new PieDataSet(yVals1, "Election Results");
            dataSet.setSliceSpace(3f);
            dataSet.setSelectionShift(5f);
            ArrayList<Integer> colors = new ArrayList<Integer>();
            colors.add(Color.parseColor("#FF3636"));
            colors.add(Color.parseColor("#50B7FF"));
            colors.add(Color.parseColor("#FF2DD2"));
            dataSet.setColors(colors);
            PieData data = new PieData(xVals, dataSet);
            data.setValueFormatter(new PercentFormatter());
            data.setValueTextSize(11f);
            data.setValueTextColor(Color.WHITE);
            mpiechart.setData(data);
            // undo all highlights
            mpiechart.highlightValues(null);
            mpiechart.invalidate();
        }

        private String comparesport(int run, int cycling, int hiking) {
            if (run > cycling) {
                if (run > hiking)
                    return "run";
                else
                    return "hiking";
            } else {
                if (cycling > hiking)
                    return "cycling";
                else
                    return "hiking";

            }
        }

        public void setPieChart() {
            mpiechart.setUsePercentValues(true);
            mpiechart.setDescription("");
            mpiechart.setDrawCenterText(true);
            mpiechart.setCenterText("Total \n" + (totalkm_cycling_num + totalkm_running_num + totalkm_hiking_num) + " KM");
            mpiechart.setExtraOffsets(5, 10, 5, 5);
            setPieData(totalkm_running_num, totalkm_cycling_num, totalkm_hiking_num, (totalkm_cycling_num + totalkm_running_num + totalkm_hiking_num));
            mpiechart.setDragDecelerationFrictionCoef(0.95f);
            mpiechart.setDrawHoleEnabled(true);
            mpiechart.setHoleColor(Color.WHITE);

            mpiechart.setTransparentCircleColor(Color.WHITE);
            mpiechart.setTransparentCircleAlpha(110);

            mpiechart.setHoleRadius(58f);
            mpiechart.setTransparentCircleRadius(61f);

            mpiechart.setDrawCenterText(true);

            mpiechart.setRotationAngle(0);
            // enable rotation of the chart by touch
            mpiechart.setRotationEnabled(true);
            mpiechart.setHighlightPerTapEnabled(true);
            mpiechart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
            Legend l = mpiechart.getLegend();
            l.setEnabled(false);


        }

        @Override
        public boolean onCheckCanExpandOrCollapseGroup(MyGroupViewHolder holder, int groupPosition, int x, int y, boolean expand) {
            Log.d("teststep", "onCheckCanExpandOrCollapseGroup");
            this.groupPosition = groupPosition;
            this.expand = expand;
            if (groupPosition != 4 && groupPosition != 5)
                return false;
            return true;
        }

        public void setLineChart() {
            chart.setTouchEnabled(true);
            chart.setDragEnabled(true);
            chart.setScaleEnabled(true);
            chart.setScaleXEnabled(true);
            chart.setScaleYEnabled(true);
            chart.setPinchZoom(true);

            chart.setDoubleTapToZoomEnabled(false);
            chart.setDescription("");
            chart.setNoDataTextDescription("No data exist");

            XAxis xAxis = chart.getXAxis();
            xAxis.setEnabled(true);
            xAxis.setDrawLabels(true);
            xAxis.setDrawAxisLine(true);
            xAxis.setDrawGridLines(false);


            YAxis yAxisl = chart.getAxisLeft();
            YAxis yAxisr = chart.getAxisRight();


            yAxisr.setEnabled(false);

            yAxisl.setEnabled(true);
            yAxisl.setTextSize(15);
            yAxisl.setDrawLabels(true);
            yAxisl.setDrawAxisLine(true);
            yAxisl.setDrawGridLines(true);
            xAxis.setTextColor(Color.BLACK);
            xAxis.setTextSize(10);
            xAxis.setGridLineWidth(1);
            xAxis.setDrawAxisLine(false);
            // xAxis.setAxisLineColor(Color.BLUE);
            xAxis.setAxisLineWidth(1);
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

            LimitLine limitLine = new LimitLine(140f, "Critical Blood Pressure");
            limitLine.setLineColor(Color.RED);
            limitLine.setLineWidth(4f);
            limitLine.setTextColor(Color.BLACK);
            limitLine.setTextSize(12f);

            yAxisl.setTextColor(Color.BLACK);
            yAxisl.setTextSize(15);
            yAxisl.setGridLineWidth(1);
            yAxisl.setDrawAxisLine(false);
            //  yAxisl.setAxisLineColor(Color.BLUE);
            yAxisl.setAxisLineWidth(1);
            yAxisl.setTextColor(Color.BLACK);

            //    xAxis.addLimitLine(limitLine);
            LineData data = new LineData(getXAxisValues(), getDataSet());
            chart.setDrawGridBackground(false);
            chart.setData(data);
            chart.animateX(200);
            //     chart.fitScreen();
            chart.invalidate();

        }

        private ArrayList<ILineDataSet> getDataSet() {
            ArrayList<ILineDataSet> dataSets = null;

            // VS1
            ArrayList<Entry> valueSet1 = new ArrayList<>();
            Entry v1e1;

            for (int a = 0; a < (health_weight.size() - 1); a++) {
                v1e1 = new Entry(Float.parseFloat(health_weight.get(a)), a);
                valueSet1.add(v1e1);
            }
            ArrayList<Entry> valueSet2 = new ArrayList<>();
            Entry v2e1;
            for (int a = 0; a < (health_weight.size() - 1); a++) {
                v2e1 = new Entry(Float.parseFloat(health_height.get(a)), a);
                valueSet2.add(v2e1);
            }

            LineDataSet lineDataSet1 = new LineDataSet(valueSet1, "Weight");
            lineDataSet1.setColor(Color.rgb(199, 21, 133));
            lineDataSet1.setLineWidth(2.5f);
            lineDataSet1.setCircleColor(Color.rgb(240, 238, 70));
            lineDataSet1.setCircleRadius(5f);
            lineDataSet1.setFillColor(Color.rgb(199, 21, 133));
            lineDataSet1.setDrawCubic(true);
            lineDataSet1.setValueTextSize(10f);
            lineDataSet1.setValueTextColor(Color.BLACK);
            lineDataSet1.setAxisDependency(YAxis.AxisDependency.LEFT);

            LineDataSet lineDataSet2 = new LineDataSet(valueSet2, "Height");
            lineDataSet2.setColor(Color.rgb(102, 205, 170));
            lineDataSet2.setLineWidth(2.5f);
            lineDataSet2.setCircleColor(Color.rgb(240, 238, 70));
            lineDataSet2.setCircleRadius(5f);
            lineDataSet2.setFillColor(Color.rgb(102, 205, 170));
            lineDataSet2.setDrawCubic(true);
            lineDataSet2.setValueTextSize(10f);
            lineDataSet2.setValueTextColor(Color.BLACK);
            lineDataSet2.setAxisDependency(YAxis.AxisDependency.LEFT);
            dataSets = new ArrayList<>();
            dataSets.add(lineDataSet1);
            dataSets.add(lineDataSet2);
            return dataSets;

        }

        private ArrayList<String> getXAxisValues() {
            ArrayList<String> xAxis = new ArrayList<>();
            for (int a = 0; a < (date.size() - 1); a++) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date date1 = formatter.parse(date.get(a));
                    Log.d("abc", date1 + "");
                    SimpleDateFormat print = new SimpleDateFormat("dd/MM");
                    Log.d("abc11", print.format(date1));
                    xAxis.add(print.format(date1) + "");
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            return xAxis;
        }

        @Override
        public void onClick(View v) {
            Log.d("groupPosition", groupPosition + "");
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(v, groupPosition);
            }

        }

        public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
            this.mItemClickListener = mItemClickListener;
        }

        @Override
        public boolean onLongClick(View v) {
            Toast.makeText(mcontext, "thththis thx", Toast.LENGTH_LONG).show();
            return true;
        }

        public interface OnItemClickListener {
            void onItemClick(View view, int position);
        }
    }


}