package com.example.ron.fyp.Login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.ron.fyp.Login.Login;
import com.example.ron.fyp.MainActivity;
import com.example.ron.fyp.R;

public class CheckLoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.check_login);
        Checkstatus();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Checkstatus();
    }


    public void Checkstatus(){
        boolean defValue =false;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Log.d("Check",prefs.getBoolean("status",defValue)+"");
        if (prefs.getBoolean("status",defValue)) {
            Intent intent = new Intent(this, MainActivity.class);
            Log.v("TOMAIN","true");
            startActivity(intent);
        }
        if (!prefs.getBoolean("status",defValue)){
            Intent intent = new Intent(this, Login.class);
            Log.v("TOLOGIN","false");
            startActivity(intent);
        }
    }
}
