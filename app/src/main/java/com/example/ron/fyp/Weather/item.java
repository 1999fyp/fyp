package com.example.ron.fyp.Weather;

import org.json.JSONObject;

/**
 * Created by User on 20/3/2016.
 */
public class item implements JSONPopulator {
    private Condition condition;

    public Condition getCondition() {
        return condition;
    }

    @Override
    public void populate(JSONObject data) {
        condition = new Condition();
        condition.populate(data.optJSONObject("condition"));
    }
}
