package com.example.ron.fyp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ron.fyp.Database.JSONParser;
import com.example.ron.fyp.other_profile.Other_profile;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class Event_Detail extends AppCompatActivity {

    String event_id;
    MapView mapView;
    GoogleMap map;
    int defValue, userID, capacityUpdate;
    TextView tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10,listview_TXT;
    Toolbar toolbar;
    String getEventID, getEventName, getHostName, getCredit, getStartDate, getEndDate, getUserId,
            getStartTime, getEndTime, getStartLocation, getEndLocation, getJoined, getCapacity,
            getStartLat, getStartLong, getEndLat, getEndLong, getTypeName, getDescription, getTypeID;
    String year, month, day, hour, minutes, currentAMPM, currentDate, currentTime, eventHour, eventMinutes
            , eventAMPM ;
    Boolean checkIsJoined = false, checkIsFavourite = false;
    ImageButton detailFavouriteIcon;
    JSONObject json;
    SimpleDateFormat simpleDateFormat;
    JSONParser jParser = new JSONParser();
    ListView joined_listview;
    private String url = "http://raymondchan1179.dlinkddns.com/fyp/detailPage.php";
    LinearLayout event_host_name;
    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_RESULTS = "results";
    private static final String TAG_JOINED = "is_joined";
    private static final String TAG_FULL = "is_full";

    // products JSONArray
    JSONArray results = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        actionbar();
        showDetail();
        final info.hoang8f.widget.FButton joinBtn = (info.hoang8f.widget.FButton) findViewById(R.id.joinBtn);
        final info.hoang8f.widget.FButton quitBtn = (info.hoang8f.widget.FButton) findViewById(R.id.quitBtn);
        checkShouldStart();


        joined_listview = (ListView) findViewById(R.id.joined_listview);
        detailFavouriteIcon = (ImageButton) findViewById(R.id.detailFavouriteBtn);
        event_host_name = (LinearLayout) findViewById(R.id.event_host_name);
        listview_TXT = (TextView) findViewById(R.id.listview_TXT);
        checkFavourite();
        setonClickHostName();
        MapSetup(savedInstanceState);
        detailFavouriteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkIsFavourite) {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    defValue = 0000;
                    userID = prefs.getInt("userID", defValue);
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    JSONParser jsonParser = new JSONParser();
                    params.add(new BasicNameValuePair("user_id", String.valueOf(userID)));
                    params.add(new BasicNameValuePair("event_id", getEventID));
                    params.add(new BasicNameValuePair("method", "1"));
                    json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/favourite.php", "GET", params);
                    try {
                        if (json.getString("success").equals("1")) {
                            detailFavouriteIcon.setImageResource(R.drawable.favourited);
                            Toast.makeText(getApplicationContext(), "Favourite", Toast.LENGTH_LONG).show();
                            checkIsFavourite = true;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    defValue = 0000;
                    userID = prefs.getInt("userID", defValue);
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    JSONParser jsonParser = new JSONParser();
                    params.add(new BasicNameValuePair("user_id", String.valueOf(userID)));
                    params.add(new BasicNameValuePair("event_id", getEventID));
                    json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/unfavourite.php", "GET", params);
                    try {
                        if (json.getString("success").equals("2")) {
                            detailFavouriteIcon.setImageResource(R.drawable.favourite_null);
                            Toast.makeText(getApplicationContext(), "Unfavourite", Toast.LENGTH_LONG).show();
                            checkIsFavourite = false;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });


        joinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<NameValuePair> params = new ArrayList<NameValuePair>();
                JSONParser jsonParser = new JSONParser();
                params.add(new BasicNameValuePair("user_id", String.valueOf(userID)));
                params.add(new BasicNameValuePair("event_id", getEventID));
                json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/joined_activity.php", "GET", params);
                Toast.makeText(getApplicationContext(), "Joined successfully", Toast.LENGTH_LONG).show();
                capacityUpdate++;
                tv4.setText(capacityUpdate + " / " + getCapacity);
                if (capacityUpdate == Integer.valueOf(getCapacity)) {
                    joinBtn.setEnabled(false);
                    joinBtn.setShadowEnabled(false);
                    joinBtn.setButtonColor(getResources().getColor(R.color.grey));
                    joinBtn.setText(" Full ");
                    quitBtn.setEnabled(true);
                    quitBtn.setCornerRadius(8);
                    quitBtn.setShadowEnabled(true);
                    quitBtn.setShadowHeight(15);
                    quitBtn.setButtonColor(getResources().getColor(R.color.tabsScrollColor));
                    quitBtn.setText(" Quit event ");
                } else {
                    joinBtn.setEnabled(false);
                    joinBtn.setShadowEnabled(false);
                    joinBtn.setButtonColor(getResources().getColor(R.color.grey));
                    joinBtn.setText(" Successfully Joined ");
                    quitBtn.setEnabled(true);
                    quitBtn.setCornerRadius(8);
                    quitBtn.setShadowEnabled(true);
                    quitBtn.setShadowHeight(15);
                    quitBtn.setButtonColor(getResources().getColor(R.color.tabsScrollColor));
                    quitBtn.setText(" Quit event ");
                    checkShouldStart();
                    new GetJoined().execute();
                }
            }
        });

        quitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<NameValuePair> params = new ArrayList<NameValuePair>();
                JSONParser jsonParser = new JSONParser();
                params.add(new BasicNameValuePair("user_id", String.valueOf(userID)));
                params.add(new BasicNameValuePair("event_id", getEventID));
                json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/quit_activity.php", "GET", params);
                Toast.makeText(getApplicationContext(), "Quited successfully", Toast.LENGTH_LONG).show();
                quitBtn.setEnabled(false);
                quitBtn.setShadowEnabled(false);
                quitBtn.setButtonColor(getResources().getColor(R.color.grey));
                quitBtn.setText(" Successfully Quited ");
                joinBtn.setEnabled(true);
                joinBtn.setCornerRadius(8);
                joinBtn.setShadowEnabled(true);
                joinBtn.setShadowHeight(10);
                joinBtn.setButtonColor(getResources().getColor(R.color.tabsScrollColor));
                joinBtn.setText(" Join event ");
                capacityUpdate--;
                tv4.setText(capacityUpdate + " / " + getCapacity);
                new GetJoined().execute();
            }
        });
        new GetJoined().execute();
    }

    public void checkShouldStart(){
        info.hoang8f.widget.FButton startBtn = (info.hoang8f.widget.FButton) findViewById(R.id.eventStartBtn);
        info.hoang8f.widget.FButton joinBtn = (info.hoang8f.widget.FButton) findViewById(R.id.joinBtn);
        info.hoang8f.widget.FButton quitBtn = (info.hoang8f.widget.FButton) findViewById(R.id.quitBtn);
        Calendar calendar = Calendar.getInstance();
        year = String.valueOf(calendar.get(Calendar.YEAR));
        month = AddZero(calendar.get(Calendar.MONTH)+1);
        day = AddZero(calendar.get(Calendar.DATE));
        hour = AddZero(calendar.get(Calendar.HOUR_OF_DAY));
        minutes = AddZero(calendar.get(Calendar.MINUTE));
        simpleDateFormat = new SimpleDateFormat("a");
        currentDate = year + "-" + month + "-" + day;
        currentTime = hour + ":" + minutes + " ";

        Log.v ("Current EventStartDate", String.valueOf(getStartDate));
        Log.v ("Current date:", currentDate);
        Log.v ("Current time:", currentTime);
        Log.v ("Check Current is Joined", String.valueOf(checkIsJoined));

        eventHour = getStartTime.substring(0,2);
        eventMinutes = getStartTime.substring(3,5);
        eventAMPM = getStartTime.substring(6,8);

        if (checkIsJoined) {
            Log.v ("CurrentC", "CurrentC");
            if (getStartDate.equals(currentDate)) {
                if (eventAMPM.equals("PM")){
                    eventHour = String.valueOf(Integer.parseInt(eventHour) + 12);
                }
                Log.v ("ACurrent", hour);
                Log.v ("BCurrent", eventHour);
                if (hour.equals(eventHour)){
                    if (Integer.valueOf(eventMinutes) - Integer.valueOf(minutes) <= 15){
                        joinBtn.setVisibility(View.GONE);
                        quitBtn.setVisibility(View.GONE);
                        startBtn.setVisibility(View.VISIBLE);
                        startBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent toLogging = new Intent (getApplication(), Logging.class);
                                Bundle eventIdBundle = new Bundle();
                                Bundle typeIdBundle = new Bundle();
                                eventIdBundle.putString("eventID", getEventID);
                                typeIdBundle.putString("typeID", getTypeID);
                                toLogging.putExtras(eventIdBundle);
                                toLogging.putExtras(typeIdBundle);
                                startActivity(toLogging);
                            }
                        });
                    }
                }
                else if (Integer.valueOf(eventHour) - Integer.valueOf(hour) == 1){
                    if (Integer.valueOf(eventMinutes) <= 14){
                        eventMinutes = eventMinutes + 60 ;
                    }
                    if (Integer.valueOf(eventMinutes) - Integer.valueOf(minutes) <= 15){
                        joinBtn.setVisibility(View.GONE);
                        quitBtn.setVisibility(View.GONE);
                        startBtn.setVisibility(View.VISIBLE);
                        startBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent toLogging = new Intent (getApplication(), Logging.class);
                                Bundle eventIdBundle = new Bundle();
                                Bundle typeIdBundle = new Bundle();
                                eventIdBundle.putString("eventID", getEventID);
                                typeIdBundle.putString("typeID", getTypeID);
                                toLogging.putExtras(eventIdBundle);
                                toLogging.putExtras(typeIdBundle);
                                startActivity(toLogging);
                            }
                        });
                    }
                }
            }
        }
        else if (!checkIsJoined){
            if (getStartDate.equals(currentDate)){
                if (hour.equals(eventHour)){
                    if (Integer.valueOf(eventMinutes) - Integer.valueOf(minutes) <= 15){
                        joinBtn.setVisibility(View.GONE);
                        quitBtn.setVisibility(View.GONE);
                        startBtn.setVisibility(View.VISIBLE);
                        startBtn.setText("Event already started.");
                        startBtn.setButtonColor(R.color.RealRed);
                    }
                }
                else if (Integer.valueOf(eventHour) - Integer.valueOf(hour) == 1){
                    if (Integer.valueOf(eventMinutes) <= 14){
                        eventMinutes = eventMinutes + 60 ;
                    }
                    if (Integer.valueOf(eventMinutes) - Integer.valueOf(minutes) <= 15){
                        joinBtn.setVisibility(View.GONE);
                        quitBtn.setVisibility(View.GONE);
                        startBtn.setVisibility(View.VISIBLE);
                        startBtn.setText("Event already started.");
                        startBtn.setButtonColor(R.color.RealRed);
                    }
                }
            }
        }
    }

    private String AddZero(int temp) {
        String temp_addZero;
        temp_addZero = (temp > 10) ? String.valueOf(temp) : "0" + String.valueOf(temp);
        return temp_addZero;
    }

    private void setonClickHostName() {
        event_host_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Other_profile.class);
                Bundle bundle = new Bundle();
                bundle.putString("user_id", getUserId);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    public void actionbar() {
        toolbar = (Toolbar) findViewById(R.id.log_list_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationIcon(R.drawable.back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void checkFavourite() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        defValue = 0000;
        userID = prefs.getInt("userID", defValue);
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        JSONParser jsonParser = new JSONParser();
        params.add(new BasicNameValuePair("user_id", String.valueOf(userID)));
        params.add(new BasicNameValuePair("event_id", getEventID));
        params.add(new BasicNameValuePair("method", "0"));
        json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/favourite.php", "GET", params);
        try {
            if (json.getString("success").equals("4")) {
                detailFavouriteIcon.setImageResource(R.drawable.favourited);
                checkIsFavourite = true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void showDetail() {
        Log.v("Making Connection", "......");
        //get eventID
        Intent intent = getIntent();
        Bundle args = intent.getExtras();
        info.hoang8f.widget.FButton joinBtn = (info.hoang8f.widget.FButton) findViewById(R.id.joinBtn);
        info.hoang8f.widget.FButton quitBtn = (info.hoang8f.widget.FButton) findViewById(R.id.quitBtn);

        getEventID = args.getString("eventID");

        //put eventID to php
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        defValue = 0000;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        userID = prefs.getInt("userID", defValue);
        params.add(new BasicNameValuePair("event_id", getEventID));
        params.add(new BasicNameValuePair("user_id", String.valueOf(userID)));
        Log.v("check userID", String.valueOf(userID));
        Log.v("check eventID", getEventID);

        JSONObject json = jParser.makeHttpRequest(url, "GET", params);
        Log.v("JSON", String.valueOf(json));

        try {
            int isJoined = json.getInt(TAG_JOINED);
            Log.v("check joined status", String.valueOf(isJoined));
            if (isJoined == 1) {
                joinBtn.setEnabled(false);
                joinBtn.setText(" Joined already ");
                joinBtn.setShadowEnabled(false);
                joinBtn.setButtonColor(getResources().getColor(R.color.grey));
                quitBtn.setEnabled(true);
                checkIsJoined = true;
            } else {
                quitBtn.setEnabled(false);
                quitBtn.setShadowEnabled(false);
                quitBtn.setButtonColor(getResources().getColor(R.color.grey));
            }

            int isFull = json.getInt(TAG_FULL);
            Log.v("check full status", String.valueOf(isFull));
            if (isFull == 1) {
                joinBtn.setEnabled(false);
                joinBtn.setShadowEnabled(false);
                joinBtn.setButtonColor(getResources().getColor(R.color.grey));
                joinBtn.setText(" Full ");
                if (checkIsJoined == true) {
                    quitBtn.setEnabled(true);
                } else {
                    quitBtn.setEnabled(false);
                    quitBtn.setShadowEnabled(false);
                    quitBtn.setButtonColor(getResources().getColor(R.color.grey));
                }
            }

            int success = json.getInt(TAG_SUCCESS);
            if (success == 1) {
                results = json.getJSONArray(TAG_RESULTS);

                for (int i = 0; i < results.length(); i++) {
                    JSONObject c = results.getJSONObject(i);
                    // Storing each json item in variable
                    //event
                    getEventName = c.getString("event_name");
                    getJoined = c.getString("joined");
                    getCapacity = c.getString("capacity");
                    getCredit = c.getString("credit");
                    getDescription = c.getString("description");
                    getHostName = c.getString("full_name");

                    //type
                    getTypeName = c.getString("type_name");
                    getTypeID = c.getString("type_id");

                    //date & time
                    getStartDate = c.getString("start_date");
                    getEndDate = c.getString("end_date");
                    getStartTime = c.getString("start_time");
                    getEndTime = c.getString("end_time");
                    getUserId = c.getString("user_id");
                    //location
                    getStartLocation = c.getString("start_location");
                    getEndLocation = c.getString("end_location");
                    getStartLat = c.getString("start_lat");
                    getEndLat = c.getString("end_lat");
                    getStartLong = c.getString("start_long");
                    getEndLong = c.getString("end_long");
                }

                // Set Text here
                tv1 = (TextView) findViewById(R.id.displayEventName);
                tv1.setText(getEventName);

                tv2 = (TextView) findViewById(R.id.displayType);
                ImageView tv2icon = (ImageView) findViewById(R.id.event_type);
                if (getTypeName.equals("Running")) {
                    tv2icon.setImageResource(R.drawable.small_running_icon);
                    tv2.setText(getTypeName);
                } else if (getTypeName.equals("Hiking")) {
                    tv2icon.setImageResource(R.drawable.small_hiking_icon);
                    tv2.setText(getTypeName);
                } else if (getTypeName.equals("Cycling")) {
                    tv2icon.setImageResource(R.drawable.small_cycling_icon);
                    tv2.setText(getTypeName);
                }

                tv3 = (TextView) findViewById(R.id.displayHostName);
                tv3.setText(getHostName);

                tv4 = (TextView) findViewById(R.id.displayParticipant);
                tv4.setText(getJoined + " / " + getCapacity);
                capacityUpdate = Integer.valueOf(getJoined);

                tv5 = (TextView) findViewById(R.id.displayCreditRequirement);
                tv5.setText(getCredit + " credit(s)");

                tv6 = (TextView) findViewById(R.id.displayEventDate);
                if (getStartDate.equals(getEndDate)) {
                    tv6.setText(getStartDate);
                } else {
                    tv6.setText(getStartDate + "   TO    " + getEndDate);
                }

                tv7 = (TextView) findViewById(R.id.displayEventTime);
                tv7.setText(getStartTime.substring(0, 8) + "    TO    " + getEndTime.substring(0, 8));

                tv8 = (TextView) findViewById(R.id.displayStartLocation);
                tv8.setText(getStartLocation);

                tv9 = (TextView) findViewById(R.id.displayEndLocation);
                tv9.setText(getEndLocation);

                tv10 = (TextView) findViewById(R.id.displayDescription);
                tv10.setText(getDescription);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void MapSetup(Bundle savedInstanceState) {
        //Map setup
        mapView = (MapView) findViewById(R.id.mapViewEventDetail);
        mapView.onCreate(savedInstanceState);

        // Gets to GoogleMap from the MapView and does initialization stuff
        map = mapView.getMap();
        //map.getUiSettings().setMyLocationButtonEnabled(false);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.setIndoorEnabled(false);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        } else {
            // Show rationale and request permission.
        }

        MapsInitializer.initialize(this);

        // Updates the location and zoom of the MapView
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(getStartLat), Double.parseDouble
                (getStartLong)), 13);
        map.animateCamera(cameraUpdate);

        MarkerOptions startMarker = new MarkerOptions();
        MarkerOptions finishMarker = new MarkerOptions();

        startMarker.position(new LatLng(Double.parseDouble(getStartLat), Double.parseDouble(getStartLong)));
        startMarker.title("START");
        startMarker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

        finishMarker.position(new LatLng(Double.parseDouble(getEndLat), Double.parseDouble(getEndLong)));
        finishMarker.title("FINISH");
        finishMarker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

        map.clear();
        map.addMarker(startMarker).showInfoWindow();
        map.addMarker(finishMarker).showInfoWindow();
        //map.animateCamera(CameraUpdateFactory.newLatLng(latLng));

        //Log.v("Marker latitude:", String.valueOf(latLng.latitude));
        //Log.v("Marker longitude:", String.valueOf(latLng.longitude));
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    class GetJoined extends AsyncTask<Void, Void, Void> {
        private JSONObject json = null;
        private ArrayList<String> user_id = new ArrayList<>();
        private ArrayList<String> full_name = new ArrayList<>();
        private ArrayList<String> icon = new ArrayList<>();

        @Override
        protected Void doInBackground(Void... mparams) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            JSONParser jsonParser = new JSONParser();
            params.add(new BasicNameValuePair("event_id", String.valueOf(getEventID)));
            json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/activity_join.php", "GET", params);
            try {
                JSONArray result = null;
                result = json.getJSONArray("results");
                if (result != null)
                    for (int i = 0; i < result.length(); i++) {
                        JSONObject c = result.getJSONObject(i);
                        full_name.add(c.getString("full_name"));
                        user_id.add(c.getString("user_id"));
                        icon.add(c.getString("icon"));
                    }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            joined_listview.setAdapter(new Event_Detail_adapter(user_id, full_name, icon, getApplicationContext()));
            
        }
    }
}