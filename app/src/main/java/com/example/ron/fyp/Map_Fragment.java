package com.example.ron.fyp;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class Map_Fragment extends Fragment {

    Button back;
    MapView mapView;
    GoogleMap map;
    Integer TAG;
    LatLng markerOptionPosition;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.set_location, container, false);

        //Retrieve TAG
        Bundle args = getArguments();
        TAG = (Integer) args.getSerializable("TAG");

        //Back Button
        back = (Button) v.findViewById(R.id.back);
        back.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (markerOptionPosition != null) {
                    latlngLinkage();
                    getLocationFromLatLng();
                }
                getFragmentManager().popBackStack();
            }
        });

        //Map setup
        mapView = (MapView) v.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);

        // Gets to GoogleMap from the MapView and does initialization stuff
        map = mapView.getMap();
        map.getUiSettings().setZoomControlsEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.setIndoorEnabled(false);
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        } else {
            // Show rationale and request permission.
        }

        MapsInitializer.initialize(getActivity());

        // Updates the location and zoom of the MapView
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(22.299777, 114.1748966), 12);
        map.animateCamera(cameraUpdate);

        Toast.makeText(getActivity(), "Click to locate the marker", Toast.LENGTH_LONG).show();
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng latLng) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                if (TAG == 0) {
                    markerOptions.title("START");
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                } else {
                    markerOptions.title("FINISH");
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                }
                map.clear();
                //map.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                map.addMarker(markerOptions).showInfoWindow();

                markerOptionPosition = latLng;
                //Log.v("Location:", getLocationFromLatLng());
                Log.v("Marker latitude:", String.valueOf(markerOptionPosition.latitude));
                Log.v("Marker longitude:", String.valueOf(markerOptionPosition.longitude));
            }
        });
        return v;
    }

    public String getLocationFromLatLng() {
        String locationDetail = "unknown";

        if (Geocoder.isPresent()) {
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            List<Address> addresses = null;
            Log.v("Geocoder", "present");

            try {
                addresses = geocoder.getFromLocation(markerOptionPosition.latitude, markerOptionPosition.longitude, 1);
                Log.v("address", String.valueOf(addresses));
            } catch (IOException ex) {
                Log.v("Error fetching address", "");
                ex.printStackTrace();
                ex.getMessage();
            }

            if (addresses != null && addresses.size() > 0) {

                if (addresses.size() >= 1 && addresses.get(0).getAddressLine(1) != null) {

                    String line1 = addresses.get(0).getAddressLine(1);
                    Log.v("Line1", line1);
                    if (addresses.get(0).getThoroughfare() != null) {
                        locationDetail = addresses.get(0).getThoroughfare();
                        String landmark = addresses.get(0).getFeatureName();
                        Log.v ("Landmark", landmark);
                        Log.v("Thoroughfare", locationDetail);
                    } else locationDetail = line1;
                } else
                    Log.v("Line1", "does not exist.");
            } else
                Log.v("address", "null");
        } else
            Log.v("Geocoder", "missed");

        CreateEvent myActivity = (CreateEvent) getActivity();
        if (TAG == 0) {
            myActivity.setStartPointLocation(locationDetail);
            return locationDetail;
        }
        else {
            myActivity.setEndPointLocation(locationDetail);
            return locationDetail;
        }
    }


    public void latlngLinkage() {
        CreateEvent myActivity = (CreateEvent) getActivity();
        if (TAG == 0) {
            myActivity.setStartLatLong(markerOptionPosition.latitude, markerOptionPosition.longitude);
            Log.v("Start location passed", String.valueOf(markerOptionPosition));
        } else {
            myActivity.setEndLatLong(markerOptionPosition.latitude, markerOptionPosition.longitude);
            Log.v("End location passed", String.valueOf(markerOptionPosition));
        }
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}

