package com.example.ron.fyp;


import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.DateFormatSymbols;
import java.util.ArrayList;

public class ViewLog extends AppCompatActivity{

    String log_id, event_id, user_id, total_distance, avg_pace, total_calories, total_time, type_of_sports, start_date, end_date, start_time, end_time, coordinates;
    TextView datetime_tv, km_tv, time_tv, minkm_tv, calories_tv;
    private ArrayList<LatLng> routeList = new ArrayList<LatLng>();

    MapView mapView;
    GoogleMap map;
    private Polyline line;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_log);

        Bundle bundle = getIntent().getExtras();
        log_id = bundle.getString("log_id");
        event_id = bundle.getString("event_id");
        user_id = bundle.getString("user_id");
        total_distance = bundle.getString("total_distance");
        avg_pace = bundle.getString("avg_pace");
        total_calories = bundle.getString("total_calories");
        total_time = bundle.getString("total_time");
        type_of_sports = bundle.getString("type_of_sports");
        start_date = bundle.getString("start_date");
        end_date = bundle.getString("end_date");
        start_time = bundle.getString("start_time");
        end_time = bundle.getString("end_time");
        coordinates = bundle.getString("coordinates");

        datetime_tv = (TextView)findViewById(R.id.loglog_datetime);
        km_tv = (TextView)findViewById(R.id.loglog_km);
        time_tv = (TextView)findViewById(R.id.loglog_time);
        minkm_tv = (TextView)findViewById(R.id.loglog_minkm);
        calories_tv = (TextView)findViewById(R.id.loglog_calories);

        datetime_tv.setText(setDate(start_date) + " - " + setTime(start_time));
        km_tv.setText(total_distance);
        time_tv.setText(setTotalTime(total_time));
        minkm_tv.setText(avg_pace);
        calories_tv.setText(total_calories);
        extractCoordinates();
        MapSetup(savedInstanceState);
    }

    private String setDate(String start_date) {
        int year, month, day;
        year = Integer.parseInt(start_date.substring(0,4));
        month = Integer.parseInt(start_date.substring(5,7));
        day = Integer.parseInt(start_date.substring(8,10));
        return getMonth(month) + " " + day + ", " + year;
    }

    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }

    private String setTime(String start_time) {
        int hour, minute;
        hour = Integer.parseInt(start_time.substring(0,2));
        minute = Integer.parseInt(start_time.substring(3,5));

        if (hour>12)
            return hour-12 + ":" + minute + " PM";
        else
            return Integer.parseInt(start_time.substring(1,2)) + ":" + minute + " AM";
    }

    private String setTotalTime(String total_time) {
        int totalSecs = Integer.parseInt(total_time);
        int hours, minutes, seconds;
        hours = totalSecs / 3600;
        minutes = (totalSecs % 3600) / 60;
        seconds = totalSecs % 60;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    private void extractCoordinates() {
        Double temp_lat, temp_lng;

        String a = "(";
        String b = ",";
        String c = ")";

        for (int i = -1; (i = coordinates.indexOf(a, i + 1)) != -1; ) {
            temp_lat = Double.valueOf(coordinates.substring(i+1,coordinates.indexOf(b,i)));
            temp_lng = Double.valueOf(coordinates.substring(coordinates.indexOf(b,i)+1,coordinates.indexOf(c,i)));
            LatLng temp = new LatLng(temp_lat, temp_lng);
            routeList.add(temp);
        }
    }

    // Initialize Map
    private void MapSetup(Bundle savedInstanceState) {
        Log.v("MapSetup", "start");

        //Map setup
        mapView = (MapView) findViewById(R.id.view_log_map_view);
        mapView.onCreate(savedInstanceState);

        // Gets to GoogleMap from the MapView and does initialization stuff
        map = mapView.getMap();
        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.setIndoorEnabled(false);

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        } else {
            // Show rationale and request permission.
        }

        MapsInitializer.initialize(this);

        LatLng point = routeList.get(0);

        // Updates the location and zoom of the MapView
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(point.latitude, point.longitude), 19);
        map.animateCamera(cameraUpdate);
        Log.v("MapSetup", "finish");

        drawMap();
    }

    private void drawMap() {

        PolylineOptions options = new PolylineOptions().width(10).color(Color.BLUE).geodesic(true);
        for (int i = 0; i < routeList.size(); i++) {
            LatLng point = routeList.get(i);
            options.add(point);
        }
        line = map.addPolyline(options);
    }

    @Override
    protected void onStart() {
        Log.v("onStart", "onStart");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.v("onResume", "onResume");
        mapView.onResume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        Log.v("onPause", "onPause");
        mapView.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.v("onStop", "onStop");
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
