package com.example.ron.fyp.Tab;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ron.fyp.Activity_Log.Activity_Log_List;
import com.example.ron.fyp.Database.JSONParser;
import com.example.ron.fyp.Friend.Friends_list;
import com.example.ron.fyp.Profile.Profile;
import com.example.ron.fyp.R;
import com.example.ron.fyp.RecyclerView_adapter;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class Tab3 extends Fragment {
    FragmentActivity mActivity;
    RecyclerView mRecyclerView;
    FloatingActionButton floatingActionButton;
    CircleImageView user_img;
    RecyclerView_adapter adapter;
    TextView name_TXT, access_TXT, credits_TXT, img_TXT;
    int user_ID = 0;
    JSONObject json;
    SharedPreferences prefs = null;
    private String facebook_id, username, password,
            access_level, credits, DOB, email, gender,
            icon, language, full_name, weight, height,
            totalkm_running, totalkm_hiking, totalkm_cycling;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.tab_3, container, false);
        prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        user_ID = prefs.getInt("userID", user_ID);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        getjson();
        //mRecyclerView.setNestedScrollingEnabled(false);
        adapter = new RecyclerView_adapter(mActivity);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        adapter.SetOnItemClickListener(new RecyclerView_adapter.OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                switch (position) {
                    case 0:
                        // profile
                        Intent intent = new Intent(getActivity(), Profile.class);
                        startActivity(intent);
                        break;
                    case 1:{
                        // activity log
                        Intent intent1 = new Intent(getActivity(), Activity_Log_List.class);
                        startActivity(intent1);
                        break;
                    }
                    case 2:{
                        //logout
                       SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putBoolean("status", false);
                        setallnull();
                        editor.commit();
                        getActivity().finish();
                        break;
                    }
                    case 3: {
                        // friend
                        Intent intent2 = new Intent(getActivity(),Friends_list.class );
                        startActivity(intent2);
                        break;
                    }

                }
            }
        });

        return rootView;
    }


    public void setallnull() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("facebook_id", null);
        editor.putString("username", null);
        editor.putString("password", null);
        editor.putString("access_level", null);
        editor.putString("credits", null);
        editor.putString("DOB", null);
        editor.putString("email", null);
        editor.putString("gender", null);
        editor.putString("icon", null);
        editor.putString("language", null);
        editor.putString("full_name", null);
        editor.putString("weight", null);
        editor.putString("height", null);
        editor.putString("totalkm_running", null);
        editor.putString("totalkm_hiking", null);
        editor.putString("totalkm_hiking", null);
        editor.commit();
    }

    @Override
    public void onResume() {

        super.onResume();
    }

    private void getjson() {
        myGetinfo_Task g = new myGetinfo_Task(getActivity());
        g.execute();
    }

    public class myGetinfo_Task extends AsyncTask<Void, Void, Void> {
        private SharedPreferences prefs = null;
        private JSONObject json = null;
        private Activity mactivity;
        private int user_ID;
        public myGetinfo_Task(Activity mactivity) {
            this.mactivity = mactivity;
        }

        @Override
        protected Void doInBackground(Void... mparams) {

            prefs = PreferenceManager.getDefaultSharedPreferences(mactivity);
            user_ID = prefs.getInt("userID", user_ID);
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            JSONParser jsonParser = new JSONParser();
            prefs.getInt("userID", user_ID);
            params.add(new BasicNameValuePair("user_ID", String.valueOf(user_ID)));
            params.add(new BasicNameValuePair("method", "0"));
            params.add(new BasicNameValuePair("height", "0"));
            params.add(new BasicNameValuePair("weight", "0"));
            json = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/profile.php", "GET", params);
         //   JSONObject json_2 = jsonParser.makeHttpRequest("http://raymondchan1179.dlinkddns.com/fyp/gethealth.php", "GET", params);
            try {
                facebook_id = json.getString("facebook_id");
                username = json.getString("username");
                password = json.getString("password");
                access_level = json.getString("access_level");
                credits = json.getString("credits");
                DOB = json.getString("DOB");
                email = json.getString("email");
                gender = json.getString("gender");
                icon = json.getString("icon");
                language = json.getString("language");
                full_name = json.getString("full_name");
                weight = json.getString("weight");
                height = json.getString("height");
                totalkm_running = json.getString("totalkm_running");
                totalkm_hiking = json.getString("totalkm_hiking");
                totalkm_cycling = json.getString("totalkm_cycling");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("ask", facebook_id + username + password +
                    access_level + credits + DOB + email + gender +
                    icon + language + full_name + weight + height +
                    totalkm_running + totalkm_hiking + totalkm_cycling);

            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("facebook_id", facebook_id);
            editor.putString("username", username);
            editor.putString("password", password);
            editor.putString("access_level", access_level);
            editor.putString("credits", credits);
            editor.putString("DOB", DOB);
            editor.putString("email", email);
            editor.putString("gender", gender);
            editor.putString("icon", icon);
            editor.putString("language", language);
            editor.putString("full_name", full_name);
            editor.putString("weight", weight);
            editor.putString("height", height);
            editor.putString("totalkm_running", totalkm_running);
            editor.putString("totalkm_hiking", totalkm_hiking);
            editor.putString("totalkm_hiking", totalkm_hiking);
            editor.commit();
            super.onPostExecute(aVoid);
        }
    }
}

